import { createSelector } from 'reselect';

/**
 * Direct selector to the page state domain
 */
const selectPageDomain = (state) => state.get('page');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Page
 */

const makeSelectPage = () => createSelector(
  selectPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectPage;
export {
  selectPageDomain,
};
