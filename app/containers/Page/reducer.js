/*
 *
 * Page reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  CHANGE_THEME,
} from './constants';

const initialState = fromJS({
  theme: 'admin',
});

function pageReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_THEME:
      return state.set('theme', action.value);
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default pageReducer;
