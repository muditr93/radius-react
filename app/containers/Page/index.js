/**
 *
 * Page
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import TopBar from 'containers/TopBar';
import SideBar from 'containers/SideBar';
import NotificationSidebar from 'containers/NotificationSidebar';

import makeSelectPage from './selectors';
import reducer from './reducer';
import saga from './saga';

import themes from '../../themes';

export class Page extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { page, hideBars, children } = this.props;
    return hideBars ? (
      <MuiThemeProvider muiTheme={themes[page.theme]}>
        {children}
      </MuiThemeProvider>
    ) : (
      <MuiThemeProvider muiTheme={themes[page.theme]}>
        <div>
          <TopBar />
          <SideBar />
          <NotificationSidebar />
          { children }
        </div>
      </MuiThemeProvider>
    );
  }
}

// for STATIC type check
Page.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  page: PropTypes.any,
  children: PropTypes.node.isRequired,
  hideBars: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  page: makeSelectPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'page', reducer });
const withSaga = injectSaga({ key: 'page', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Page);
