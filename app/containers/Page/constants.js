/*
 *
 * Page constants
 *
 */

export const DEFAULT_ACTION = 'app/Page/DEFAULT_ACTION';
export const CHANGE_THEME = 'app/Page/CHANGE_THEME';
