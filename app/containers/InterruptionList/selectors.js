import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the interruptionList state domain
 */

const selectInterruptionListDomain = state =>
  state.get("interruptionList", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by InterruptionList
 */

const makeSelectInterruptionList = () =>
  createSelector(selectInterruptionListDomain, substate => substate.toJS());

export default makeSelectInterruptionList;
export { selectInterruptionListDomain };
