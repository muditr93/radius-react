import { fromJS } from 'immutable';
import interruptionListReducer from '../reducer';

describe('interruptionListReducer', () => {
  it('returns the initial state', () => {
    expect(interruptionListReducer(undefined, {})).toEqual(fromJS({}));
  });
});
