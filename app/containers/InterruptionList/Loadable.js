/**
 *
 * Asynchronously loads the component for InterruptionList
 *
 */

import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
