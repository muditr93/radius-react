/**
 *
 * InterruptionList
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectInterruptionList from "./selectors";
import reducer from "./reducer";
import saga from "./saga";

import Page from 'containers/Page';
import Pusher from 'components/Pusher';
import RzTable from 'components/RzTable';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';

/* eslint-disable react/prefer-stateless-function */
export class InterruptionList extends React.Component {
  render() {
    return <div />;
  }
}

InterruptionList.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  interruptionlist: makeSelectInterruptionList()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "interruptionList", reducer });
const withSaga = injectSaga({ key: "interruptionList", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(InterruptionList);
