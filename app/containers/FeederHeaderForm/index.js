/**
 *
 * FeederHeaderForm
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import { withStyles } from '@material-ui/core/styles';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Grid from '@material-ui/core/Grid';
import DatePicker from 'material-ui/DatePicker';
import RzButton from 'components/RzButton';
import {fetchNewUsers} from 'containers/RequesterListPage/actions';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectFeederHeaderForm from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import dayjs from "dayjs";
import styled from 'styled-components';
import {getDiscomList, changeFeeder, changeDiscomList, changeZone, changeCircle, changeDivision, changeSubstation, changeInterruption, changeFromDate, changeToDate} from './actions';
const style = {
  label: {
    fontSize: '12px',
    color: '#808080',
    fontFamily: 'Helvetica Neue,Helvetica,Arial,sans-serif !important'
  }
}
const Wrapper = styled.div`
padding: 1em;
`;
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    justifyContent: 'space-around',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});
/* eslint-disable react/prefer-stateless-function */
export class FeederHeaderForm extends React.Component {
  componentDidMount(){
    this.props.dispatch(getDiscomList());
  }
  handleChangeDate = (val, type) => {
    let payload = dayjs(val).format('YYYY-MM-DD');
    if (type) {
      this.props.dispatch(changeFromDate(payload))
    } else {
      this.props.dispatch(changeToDate(payload))
    }
  }
  render() {
    const {
      classes,
      showSub,
      feederField=false,
    feederheaderform : {
      discomList,
      circle,
      division,
      subs,
      zone,
      discomV,
      circleV,
      divisionV,
      feederV,
      feederList,
      subsV,
      zoneV,
      Interruptions
    }

  } = this.props;
    return (
      <Wrapper>
        <div className={classes.root}>
          <Grid container spacing={24} className={classes.container}>
            <Grid container spacing={24} >
              <Grid
                item
                xs={6}
                sm={2}
              >
              <SelectField
                floatingLabelText="Discom"
                value={discomV}
                fullWidth
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                floatingLabelStyle={style.label}
                labelStyle={style.label}
                onChange={(e,key,val) => this.props.dispatch(changeDiscomList(val))}
              >
                {discomList && discomList.length && discomList.map((x) => <MenuItem value={x.id} primaryText={x.name} />)}
              </SelectField>
              </Grid>
              <Grid
                item
                xs={6}
                sm={2}
              >
              <SelectField
                floatingLabelText="Zone"
                value={zoneV}
                fullWidth
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                labelStyle={style.label}
                floatingLabelStyle={style.label}
                onChange={(e,key,val) => this.props.dispatch(changeZone(val))}
              >
                {zone && zone.length && zone.map((x) => <MenuItem value={x.id} primaryText={x.name} />)}
              </SelectField>
              </Grid>
              <Grid
                item
                xs={6}
                sm={2}
              >
              <SelectField
                floatingLabelText="Circle"
                value={circleV}
                fullWidth
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                labelStyle={style.label}
                floatingLabelStyle={style.label}
                onChange={(e,key,val) => this.props.dispatch(changeCircle(val))}
              >
                {circle && circle.length && circle.map((x) => <MenuItem value={x.id} primaryText={x.name} />)}
              </SelectField>
              </Grid>
              <Grid
                item
                xs={6}
                sm={2}
              >
              <SelectField
                floatingLabelText="Division"
                value={divisionV}
                fullWidth
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                labelStyle={style.label}
                floatingLabelStyle={style.label}
                onChange={(e,key,val) => this.props.dispatch(changeDivision(val))}
              >
                {division && division.length && division.map((x) => <MenuItem value={x.id} primaryText={x.name} />)}
              </SelectField>
              </Grid>
              <Grid
                item
                xs={6}
                sm={2}
              >
              <SelectField
                floatingLabelText="Substation"
                value={subsV}
                fullWidth
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                labelStyle={style.label}
                floatingLabelStyle={style.label}
                onChange={(e,key,val) => this.props.dispatch(changeSubstation(val))}
              >
                {subs && subs.length && subs.map((x) => <MenuItem value={x.id} primaryText={x.name} />)}
              </SelectField>
              </Grid>
              {feederField&&(
                <Grid
                  item
                  xs={6}
                  sm={2}
                >
                <SelectField
                  floatingLabelText="Feeders"
                  value={feederV}
                  fullWidth
                  underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                  labelStyle={style.label}
                  floatingLabelStyle={style.label}
                  onChange={(e,key,val) => this.props.dispatch(changeFeeder(val))}
                >
                  {feederList && feederList.length && feederList.map((x) => <MenuItem value={x.id} primaryText={x.name} />)}
                </SelectField>
                </Grid>
              )}
              {showSub?
                (<Grid
                  item
                  xs={6}
                  sm={2}
                >
                <SelectField
                floatingLabelText="Average Outage"
                value={Interruptions}
                fullWidth
                underlineStyle={{borderBottom: '1px solid #B3B3B3'}}
                labelStyle={style.label}
                floatingLabelStyle={style.label}
                onChange={(e,key,val) => this.props.dispatch(changeInterruption(val))}
              >
                <MenuItem value='total_interruption' primaryText="Total Interruptions" />
                <MenuItem value='less_than_15' primaryText="Less than 15" />
                <MenuItem value='between_15_to_60' primaryText="Between 15 to 60" />
                <MenuItem value='more_than_60' primaryText="More than 60"/>
              </SelectField>
            </Grid>):null
            }
              {showSub?(<Grid
                item
                xs={6}
                sm={2}
              >
              <DatePicker
                onChange={(event, date) => this.handleChangeDate(date, true)}
                floatingLabelText="From Date"
                fullWidth
              />
          </Grid>):null}
              {showSub?
              (<Grid
                item
                xs={6}
                sm={2}
              >
                <DatePicker
                  onChange={(event, date) => this.handleChangeDate(date, false)}
                  floatingLabelText="To Date"
                  fullWidth
                />
              </Grid>
              ):null}
                  <Grid
                    item
                    xs={6}
                    sm={2}
                    style={{position: 'relative', marginTop:`${feederField ? '20px': null}` }}
                  >
                    <RzButton bgColor="#00A99D" color="white" onClick={() => this.props.dispatch(this.props.handleSubmit())}> VIEW </RzButton>
                  </Grid>
            </Grid>
          </Grid>
        </div>
      </Wrapper>
  );
  }
}

FeederHeaderForm.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  feederheaderform: makeSelectFeederHeaderForm()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "feederHeaderForm", reducer });
const withSaga = injectSaga({ key: "feederHeaderForm", saga });

export default compose(
  withReducer,
  withSaga,
  withStyles(styles),
  withConnect
)(FeederHeaderForm);
