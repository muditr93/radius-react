/*
 *
 * FeederHeaderForm reducer
 *
 */

import { fromJS } from "immutable";
// import { DEFAULT_ACTION } from "./constants";
import {
  DEFAULT_ACTION,
  DISCOM_LIST,
  CIRLCE_LIST,
  DIVSION_LIST,
  SUBSTATION_LIST,
  ZONE_LIST,
  DISCOM_CHANGE,
  CIRLCE_CHANGE,
  DIVSION_CHANGE,
  SUBSTATION_CHANGE,
  ZONE_CHANGE,
  CHANGE_INTERRUPTION,
  CHAGE_FROM,
  CHANGE_TO,
  CHANGE_FEEDER,
  FEEDER_LIST,
} from "./constants";

export const initialState = fromJS({
  discomList: [],
  circle: [],
  division: [],
  subs: [],
  zone: [],
  discomV:'',
  circleV:'',
  divisionV:'',
  subsV:'',
  zoneV:'',
  feederList: [],
  feederV: '',
  Interruptions: '',
  from:'',
  to: '',
});

function feederHeaderFormReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case CHANGE_FEEDER:
      return state.set('feederV', action.value);
    case DIVSION_CHANGE:
      return state.set('divisionV', action.value);
    case CHANGE_INTERRUPTION:
      return state.set('Interruptions', action.value);
    case CHAGE_FROM:
      return state.set('from', action.value);
    case CHANGE_TO:
      return state.set('to', action.value);
    case DISCOM_CHANGE:
      return state.set('discomV', action.value);
    case CIRLCE_CHANGE:
      return state.set('circleV', action.value);
    case DIVSION_CHANGE:
      return state.set('divisionV', action.value);
    case SUBSTATION_CHANGE:
      return state.set('subsV', action.value);
    case ZONE_CHANGE:
      return state.set('zoneV', action.value);
    case `${FEEDER_LIST}_SUCCESS`:
      return state.set('feederList', action.data);
    case `${DISCOM_LIST}_SUCCESS`:
      return state.set('discomList', action.data);
    case `${CIRLCE_LIST}_SUCCESS`:
      return state.set('circle', action.data);
    case `${DIVSION_LIST}_SUCCESS`:
      return state.set('division', action.data);
    case `${ZONE_LIST}_SUCCESS`:
      return state.set('zone', action.data);
    case `${SUBSTATION_LIST}_SUCCESS`:
      return state.set('subs', action.data);
    default:
      return state;
  }
}

export default feederHeaderFormReducer;
