/*
 *
 * FeederHeaderForm constants
 *
 */

export const DISCOM_LIST = 'app/FeederHeaderForm/DISCOM_LIST';
export const CIRLCE_LIST = 'app/FeederHeaderForm/CIRCLE_LIST';
export const DIVSION_LIST = 'app/FeederHeaderForm/DIVSION_LIST';
export const SUBSTATION_LIST = 'app/FeederHeaderForm/SUBSTATION_LIST';
export const ZONE_LIST = 'app/FeederHeaderForm/ZONE_LIST';
export const DEFAULT_ACTION = 'app/FeederHeaderForm/DEFAULT_ACTION';
export const DISCOM_CHANGE = 'app/FeederHeaderForm/DISCOM_CHANGE';
export const CIRLCE_CHANGE = 'app/FeederHeaderForm/CIRCLE_CHANGE';
export const DIVSION_CHANGE = 'app/FeederHeaderForm/DIVSION_CHANGE';
export const SUBSTATION_CHANGE = 'app/FeederHeaderForm/SUBSTATION_CHANGE';
export const ZONE_CHANGE = 'app/FeederHeaderForm/ZONE_CHANGE';
export const CHANGE_INTERRUPTION = 'app/FeederHeaderForm/CHANGE_INTERRUPTION';
export const CHAGE_FROM = 'app/FeederHeaderForm/CHAGE_FROM';
export const CHANGE_TO = 'app/FeederHeaderForm/CHANGE_TO';
export const FEEDER_LIST = 'app/FeederHeaderForm/FEEDER_LIST';
export const CHANGE_FEEDER = 'app/FeederHeaderForm/CHANGE_FEEDER';
