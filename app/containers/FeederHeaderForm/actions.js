/*
 *
 * FeederHeaderForm actions
 *
 */

import {
  DEFAULT_ACTION,
  DISCOM_LIST,
  CIRLCE_LIST,
  DIVSION_LIST,
  SUBSTATION_LIST,
  ZONE_LIST,
  DISCOM_CHANGE,
  CIRLCE_CHANGE,
  DIVSION_CHANGE,
  SUBSTATION_CHANGE,
  ZONE_CHANGE,
  CHANGE_INTERRUPTION,
  CHAGE_FROM,
  CHANGE_TO,
  CHANGE_FEEDER,
} from './constants';



export function changeFeeder(value) {
  return {
    type: CHANGE_FEEDER,
    value,
  };
}
export function changeInterruption(value) {
  return {
    type: CHANGE_INTERRUPTION,
    value,
  };
}
export function changeFromDate(value) {
  return {
    type: CHAGE_FROM,
    value,
  };
}
export function changeToDate(value) {
  return {
    type: CHANGE_TO,
    value,
  };
}
export function changeDiscomList(value) {
  return {
    type: DISCOM_CHANGE,
    value,
  };
}

export function changeZone(value) {
  return {
    type: ZONE_CHANGE,
    value,
  };
}
export function changeCircle(value) {
  return {
    type: CIRLCE_CHANGE,
    value,
  };
}
export function changeDivision(value) {
  return {
    type: DIVSION_CHANGE,
    value,
  };
}
export function changeSubstation(value) {
  return {
    type: SUBSTATION_CHANGE,
    value,
  };
}
export function getDiscomList(value) {
  return {
    type: DISCOM_LIST,
    value,
  };
}

export function getZone(value) {
  return {
    type: ZONE_LIST,
    value,
  };
}
export function getCircle(value) {
  return {
    type: CIRLCE_LIST,
    value,
  };
}
export function getDivision(value) {
  return {
    type: DIVSION_LIST,
    value,
  };
}
export function getSubstation(value) {
  return {
    type: SUBSTATION_LIST,
    value,
  };
}

export function defaultAction(value) {
  return {
    type: DEFAULT_ACTION,
    value,
  };
}
