import request from 'superagent';
import {
  takeLatest, put, call, select, all, throttle,
} from 'redux-saga/effects';
import { fromJS } from 'immutable';
import makeSelectBasePage from './selectors';
import {
  DISCOM_LIST,
  CIRLCE_LIST,
  DIVSION_LIST,
  SUBSTATION_LIST,
  ZONE_LIST,
  DISCOM_CHANGE,
  CIRLCE_CHANGE,
  DIVSION_CHANGE,
  SUBSTATION_CHANGE,
  ZONE_CHANGE,
  FEEDER_LIST,
} from './constants';

export function* getDiscomSaga(action) {
  console.log(action);
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      login_id: 'webui',
      token_id: '24b2da-11e8-88ac-02d6f4b17064',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: 'EODB',
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getDiscoms')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${DISCOM_LIST}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }

    yield put({ type: `${DISCOM_LIST}_FAILURE`, error });
  }
}

export function* getCircleSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: 'EODB',
      discom_id: state.getIn(['feederHeaderForm', 'discomV']),
      zone_id: state.getIn(['feederHeaderForm', 'zoneV']),
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getCircle')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${CIRLCE_LIST}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }

    yield put({ type: `${CIRLCE_LIST}_FAILURE`, error });
  }
}

export function* getDivisionSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: 'EODB',
      discom_id: state.getIn(['feederHeaderForm', 'discomV']),
      zone_id: state.getIn(['feederHeaderForm', 'zoneV']),
      circle_id: state.getIn(['feederHeaderForm', 'circleV']),
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getDivision')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${DIVSION_LIST}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }

    yield put({ type: `${DIVSION_LIST}_FAILURE`, error });
  }
}

export function* getSubstationSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: 'EODB',
      discom_id: state.getIn(['feederHeaderForm', 'discomV']),
      zone_id: state.getIn(['feederHeaderForm', 'zoneV']),
      circle_id: state.getIn(['feederHeaderForm', 'circleV']),
      division_id: state.getIn(['feederHeaderForm', 'divisionV']),
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getSubstation')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${SUBSTATION_LIST}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }

    yield put({ type: `${SUBSTATION_LIST}_FAILURE`, error });
  }
}

export function* getZoneSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: 'EODB',
      discom_id: state.getIn(['feederHeaderForm', 'discomV']),
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getZone')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${ZONE_LIST}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }

    yield put({ type: `${ZONE_LIST}_FAILURE`, error });
  }
}
export function* getFeederSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: 'EODB',
      discom_id: state.getIn(['feederHeaderForm', 'discomV']),
      zone_id: state.getIn(['feederHeaderForm', 'zoneV']),
      circle_id: state.getIn(['feederHeaderForm', 'circleV']),
      division_id: state.getIn(['feederHeaderForm', 'divisionV']),
      "substation_id": state.getIn(['feederHeaderForm', 'subsV']),
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${FEEDER_LIST}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }

    yield put({ type: `${ZONE_LIST}_FAILURE`, error });
  }
}
export default function* defaultSaga() {
  yield takeLatest(DISCOM_LIST, getDiscomSaga);
  yield takeLatest(ZONE_CHANGE, getCircleSaga);
  yield takeLatest(CIRLCE_CHANGE, getDivisionSaga);
  yield takeLatest(DIVSION_CHANGE, getSubstationSaga);
  yield takeLatest(SUBSTATION_CHANGE, getFeederSaga);
  yield takeLatest(DISCOM_CHANGE, getZoneSaga);
}
