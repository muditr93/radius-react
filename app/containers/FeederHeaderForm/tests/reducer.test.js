import { fromJS } from 'immutable';
import feederHeaderFormReducer from '../reducer';

describe('feederHeaderFormReducer', () => {
  it('returns the initial state', () => {
    expect(feederHeaderFormReducer(undefined, {})).toEqual(fromJS({}));
  });
});
