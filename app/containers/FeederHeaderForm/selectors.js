import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the feederHeaderForm state domain
 */

const selectFeederHeaderFormDomain = state => state.get('feederHeaderForm', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by FeederHeaderForm
 */

const makeSelectFeederHeaderForm = () => createSelector(selectFeederHeaderFormDomain, substate => substate.toJS());

export default makeSelectFeederHeaderForm;
export { selectFeederHeaderFormDomain };
