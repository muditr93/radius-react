
import { fromJS } from 'immutable';
import requesterListPageReducer from '../reducer';

describe('requesterListPageReducer', () => {
  it('returns the initial state', () => {
    expect(requesterListPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
