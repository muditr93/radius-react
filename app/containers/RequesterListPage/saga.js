import { takeLatest, call, put, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import request from 'superagent';
import { newUsersLoaded, userLoadFailuer, preFillUserDetails } from './actions';
import { FETCH_NEW_USERS, SELECT_USER, EDIT_USER, OPEN_EDITOR } from './constants';// import { take, call, put, select } from 'redux-saga/effects';
import { getUsersCall } from './api';

export function* getUsers(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
    token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
    access_area: 'UPPCL',
    access_area_id: '1',
    project_id: 'EODB',
    discom_id: state.getIn(['feederHeaderForm', 'discomV']),
    zone_id: state.getIn(['feederHeaderForm', 'zoneV']),
    circle_id: state.getIn(['feederHeaderForm', 'circleV']),
    division_id: state.getIn(['feederHeaderForm', 'divisionV']),
    "substation_id": state.getIn(['feederHeaderForm', 'subsV']),
    }

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put(newUsersLoaded(data.resources));
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }
    yield put(userLoadFailuer());
  }
}

export function* updateEditor(action) {
  if (action.value) {
    // yield put(preFillUserDetails(action.value))
  }
}
export default function* defaultSaga() {
  yield takeLatest(OPEN_EDITOR, updateEditor);
  yield takeLatest(FETCH_NEW_USERS, getUsers);
}
