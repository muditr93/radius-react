/*
 *
 * RequesterListPage constants
 *
 */

export const OPEN_EDITOR = 'app/RequesterListPage/OPEN_EDITOR';
export const CLOSE_EDITOR = 'app/RequesterListPage/CLOSE_EDITOR';
export const FETCH_NEW_USERS = 'app/RequesterListPage/FETCH_NEW_USERS';
export const SELECT_USER = 'app/RequesterListPage/SELECT_USER';
export const EDIT_USER = 'app/RequesterListPage/EDIT_USER';
export const NEW_USERS_LOADED = 'app/RequesterListPage/NEW_USERS_LOADED';
export const USER_LOAD_FAILURE = 'app/RequesterListPage/USER_LOAD_FAILURE';
export const USER_LIST_UPDATED = 'app/RequesterListPage/USER_LIST_UPDATED';
export const PREFILL_USER = 'app/RequesterListPage/PREFILL_USER';
export const ALTER_OFFSET = 'app/RequesterListPage/ALTER_OFFSET';
export const columns = [
  {
    sort: 0,
    type: 'zone_name',
    name: 'ZONE',
    size: 1,
  },
  {
    sort: 1,
    type: 'circle_name',
    name: 'CIRCLE',
    size: 1,
  },
  {
    sort: 2,
    type: 'division_name',
    name: 'Division',
    size: 1,
  },
  {
    sort: 3,
    type: 'name',
    name: 'NAME',
    size: 1,
  },
  {
    sort: 4,
    type: 'serial_no',
    name: 'METER ID',
    size: 1,
  },
  {
    sort: 5,
    type: 'mc_UID',
    name: 'MODEM',
    size: 1,
  },
  {
    sort: 6,
    type: 'meter_ct_mf',
    name: 'UOM',
    size: 1,
  },
  {
    sort: 7,
    type: 'uom',
    name: 'UOM',
    size: 1,
  },
  {
    sort: 8,
    type: 'serial_no',
    name: 'Modem No',
    size: 1,
  },
  {
    sort: 9,
    type: 'r_Current',
    name: 'Reading(VAH)',
    size: 1,
  },
  {
    sort: 10,
    type: 'voltage',
    name: 'VOLTAGE (R|Y|B)',
    size: 1,
  },
  {
    sort: 11,
    type: 'current',
    name: 'Current (R|Y|B)',
    size: 1,
  },
  {
    sort: 12,
    type: 'pf',
    name: 'PF (R|Y|B|CUMM)',
    size: 1,
  },
  {
    sort: 13,
    type: 'phase_voltage',
    name: 'Phase Voltage (R|Y|B)',
    size: 1,
  },
  {
    sort: 14,
    type: 'instant_cum_KVA',
    name: 'LOAD',
    size: 1,
  },
  {
    sort: 15,
    type: 'max_demand_KVA',
    name: 'MD',
    size: 1,
  },
  {
    sort: 16,
    type: 'incomer',
    name: 'INCOMER',
    size: 1,
  },
  {
    sort: 17,
    type: 'cb',
    name: 'CB',
    size: 1,
  },
];
