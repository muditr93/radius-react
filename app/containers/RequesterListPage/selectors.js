import { createSelector } from 'reselect';

/**
 * Direct selector to the requesterListPage state domain
 */
const selectRequesterListPageDomain = (state) => state.get('requesterListPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RequesterListPage
 */

const makeSelectRequesterListPage = () => createSelector(
  selectRequesterListPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectRequesterListPage;
export {
  selectRequesterListPageDomain,
};
