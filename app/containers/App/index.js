/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import Auth from 'containers/Auth';
import { Switch, Route, browserHistory } from 'react-router-dom';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import WelcomePage from 'containers/WelcomePage/Loadable';
import BasePage from 'containers/BasePage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import AuthRoute from 'components/AuthRoute';
import RequesterListPage from 'containers/RequesterListPage';
import FeederDetailsPage from 'containers/FeederDetailsPage'
import GetGatewayDetails from 'containers/GetGatewayDetails';
import NoConsumptionFeeder from 'containers/NoConsumptionFeeder';
import AverageOuting from 'containers/AverageOuting';
export default function App() {
  return (
    <Auth
      renderRoutes={
        (isAuthenticated) => (
          <Switch history={browserHistory}>
            <Route exact path="/login" component={LoginPage} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/noConsumptionFeeder" component={NoConsumptionFeeder} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/feederDetails/:id?" component={FeederDetailsPage} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/feeder/:id?" component={FeederDetailsPage} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/feeders" component={RequesterListPage} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/averageOuting" component={AverageOuting} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/" component={BasePage} />
              <AuthRoute isAuthenticated={isAuthenticated} exact path="/gateway" component={GetGatewayDetails} />
              <AuthRoute component={NotFoundPage} />
          </Switch>
        )
      }
    />
  );
}
