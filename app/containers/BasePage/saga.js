import request from 'superagent';
import {
  takeLatest, put, call, select, all, throttle,
} from 'redux-saga/effects';
import { fromJS } from 'immutable';
import dayjs from 'dayjs';
import makeSelectBasePage from './selectors';

import {
  GET_FEEDER_STATS,
  GET_FEEDER_GRAPH_DATA,
  GET_FEEDER_MAP_DATA,
  GET_ENERGY_SUPPLIED_DATA,
  AREA_CHANGE,
  GET_INSTANT_LOAD_DATA,
  GET_FEEDER_INFO,
  GET_INTERRUPTION_DATA,
  GET_ELU_DATA,
  GET_PF_DATA,
  GET_BEST_WORST_DATA,
  GET_COMPARISON_DATA,
  GET_STATISTICAL_DATA,
} from './constants';

export function* getFeederStatsSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederStatistics')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${GET_FEEDER_STATS}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_FEEDER_STATS}_FAILURE`, error });
  }
}

export function* getFeederGraphDataSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederStatisticsGraphData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }

        return response.body;
      });
    yield put({ type: `${GET_FEEDER_GRAPH_DATA}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_FEEDER_GRAPH_DATA}_FAILURE`, error });
  }
}

export function* getFeederMapDataSaga(action) {
  const resp = {};
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getMapData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }
        return response.body;
      });
    const m = data.resources.reduce((prev, next) => {
      next.latitude ? (prev[next.latitude] = next) : '';
      return prev;
    }, {});
    resp.resources = Object.keys(m).map(x => m[x]);
    yield put({ type: `${GET_FEEDER_MAP_DATA}_SUCCESS`, data: resp });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_FEEDER_MAP_DATA}_FAILURE`, error });
  }
}

export function* getEnergySuppliedDataSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederEnergySupplied')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${GET_ENERGY_SUPPLIED_DATA}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_FEEDER_MAP_DATA}_FAILURE`, error });
  }
}

export function* getInstantLoadDataSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederEnergyLoad')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }

        return response.body;
      });

    yield put({ type: `${GET_INSTANT_LOAD_DATA}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_FEEDER_MAP_DATA}_FAILURE`, error });
  }
}

export function* getFeederInfoSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const { sensorId } = action.data;
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
      sensor_id: sensorId,
    };

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederMapDetails')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }

        return response.body;
      });

    yield put({ type: `${GET_FEEDER_INFO}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}
export function* getInterruptionDataSaga() {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      project_id: project,
    };
    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getInterruptionData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${GET_INTERRUPTION_DATA}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    // yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}

export function* getELUAmountSaga() {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      token_id: 'feeder',
      access_area: 'feeder',
      project_id: project,
    };
    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getELUAmountData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${GET_ELU_DATA}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    // yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}

export function* getStatisticalDataSaga() {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      "login_id":"webui",
      "token_id":"24b2fc50-f3da-11e8-88ac-02d6f4b17064",
      "access_area":"UPPCL",
      "access_area_id":"1",
      "project_id":"ALL"
    }
      const data = yield request
        .post('https://feeder.myxenius.com/webapi/v1/getFeederStatisticsHistory')
        .send(payload)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Something went wrong with API');
          }
          return response.body;
        });
      yield put({ type: `${GET_STATISTICAL_DATA}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    // yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}

export function* getPowerFactorDataSaga() {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      token_id: 'feeder',
      access_area: 'feeder',
      project_id: project,
    };

    if (project !== 'ALL') {
      const data = yield request
        .post('https://feeder.myxenius.com/webapi/v1/getFeederPF')
        .send(payload)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Something went wrong with API');
          }
          return response.body;
        });
      yield put({ type: `${GET_PF_DATA}_SUCCESS`, data });
    }
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    // yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}

export function* getBestWorstDataSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
      'login_id ': 'xxxx@9999',
      'token_id ': '8002ZZZ11',
      access_area: 'UPPCL',
      access_area_id: '1',
      type: action && action.data && action.data.type ? action.data.type : 'interruption',
      month: dayjs().format('YYYY-MM'),
      feederCount: action && action.data && action.data.feederCount ? action.data.feederCount : '4',
      project_id: project,
    };

    if (project !== 'ALL') {
      const data = yield request
        .post('https://feeder.myxenius.com/webapi/v1/getBestAndWorstFeeder')
        .send(payload)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Something went wrong with API');
          }
          return response.body;
        });


      yield put({ type: `${GET_BEST_WORST_DATA}_SUCCESS`, data });
    }
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    // yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}

export function* getComparisonDataSaga(action) {
  console.log('in saga action: ', action);


  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);

    let url;
    let payload;


    if (action.data && action.data.type === 'interruption') {
      url = 'https://feeder.myxenius.com/webapi/v1/getInterruptionData';
      payload = {
        'login_id ': 'xxxx@9999',
        'token_id ': '8002ZZZ11',
        access_area: 'UPPCL',
        access_area_id: '1',
        project_id: project,
        fromTime: action.data.startDate,
        toTime: action.data.endDate,
      };
    }

    if (action.data && action.data.type === 'feederEnergy') {
      url = 'https://feeder.myxenius.com/webapi/v1/getFeederEnergySupplied';
      payload = {
        'login_id ': 'xxxx@9999',
        'token_id ': '8002ZZZ11',
        access_area: 'UPPCL',
        access_area_id: '1',
        project_id: project,
        fromTime: action.data.startDate,
        toTime: action.data.endDate,
      };
    }

    if (action.data && action.data.type === 'elu') {
      url = 'https://feeder.myxenius.com/webapi/v1/getELUAmountData';
      payload = {
        'login_id ': 'xxxx@9999',
        'token_id ': '8002ZZZ11',
        access_area: 'feeder',
        project_id: project,
        fromTime: action.data.startDate,
        toTime: action.data.endDate,
      };
    }

    if (action.data && action.data.type === 'bestWorst') {
      url = 'https://feeder.myxenius.com/webapi/v1/getBestAndWorstFeeder';
      payload = {
        "login_id":"webui",
        "token_id":"24b2fc50-f3da-11e8-88ac-02d6f4b17064",
        access_area: 'UPPCL',
        access_area_id: '1',
        type: 'INTERRUPTION',
        feederCount: action && action.data && action.data.feederCount ? action.data.feederCount : '4',
        project_id: project,
        fromMonth: action.data.startDate.split('-').slice(0, 2).join('-'),
        toMonth: action.data.endDate.split('-').slice(0, 2).join('-'),
      };
    }

    console.log('payload: ', payload);

    if (url) {
      const response = yield request
        .post(url)
        .send(payload)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Something went wrong with API');
          }
          return response.body;
        });

      const { data } = action;
      data.body = response;


      yield put({ type: `${GET_COMPARISON_DATA}_SUCCESS`, data });
    }
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    // yield put({ type: `${GET_FEEDER_INFO}_FAILURE`, error });
  }
}

export function* areaChangeSaga(action) {
  yield all([
    call(getInterruptionDataSaga),
    call(getELUAmountSaga),
    call(getFeederGraphDataSaga),
    call(getEnergySuppliedDataSaga),
    call(getFeederMapDataSaga),
    call(getInstantLoadDataSaga),
    call(getPowerFactorDataSaga),
    call(getBestWorstDataSaga),
  ]);
}
export default function* defaultSaga() {
  yield takeLatest(GET_ELU_DATA, getELUAmountSaga);
  yield takeLatest(GET_STATISTICAL_DATA, getStatisticalDataSaga);
  yield takeLatest(GET_INTERRUPTION_DATA, getInterruptionDataSaga);
  yield takeLatest(GET_FEEDER_STATS, getFeederStatsSaga);
  yield takeLatest(GET_FEEDER_GRAPH_DATA, getFeederGraphDataSaga);
  yield takeLatest(GET_FEEDER_MAP_DATA, getFeederMapDataSaga);
  yield takeLatest(GET_ENERGY_SUPPLIED_DATA, getEnergySuppliedDataSaga);
  yield takeLatest(GET_INSTANT_LOAD_DATA, getInstantLoadDataSaga);
  yield takeLatest(GET_FEEDER_INFO, getFeederInfoSaga);
  yield takeLatest(GET_PF_DATA, getPowerFactorDataSaga);
  yield takeLatest(AREA_CHANGE, areaChangeSaga);
  yield takeLatest(GET_COMPARISON_DATA, getComparisonDataSaga);
  yield takeLatest(GET_BEST_WORST_DATA, getBestWorstDataSaga);
}
