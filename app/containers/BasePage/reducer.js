/*
 *
 * BasePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  GET_FEEDER_STATS,
  GET_FEEDER_GRAPH_DATA,
  GET_FEEDER_MAP_DATA,
  GET_ENERGY_SUPPLIED_DATA,
  AREA_CHANGE,
  GET_INSTANT_LOAD_DATA,
  GET_FEEDER_INFO,
  GET_INTERRUPTION_DATA,
  GET_ELU_DATA,
  GET_PF_DATA,
  GET_BEST_WORST_DATA,
  GET_COMPARISON_DATA,
  CLEAR_COMPARISON_DATA,
  GET_STATISTICAL_DATA
} from './constants';

export const initialState = fromJS({
  area: 'ALL',
  feederStats: {},
  comparisonData: [],
  statistic:{},
});

function basePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case `${GET_FEEDER_STATS}_SUCCESS`:
      return state.set('feederStats', action.data);
    case `${GET_STATISTICAL_DATA}_SUCCESS`:
      return state.set('statistic', action.data);
    case `${GET_ELU_DATA}_SUCCESS`:
      return state.set('eluData', action.data);
    case AREA_CHANGE:
      return state.set('feederMapData', []).set('area', action.data);
    case `${GET_INTERRUPTION_DATA}_SUCCESS`:
      return state.set('interruptionData', action.data);
    case `${GET_FEEDER_GRAPH_DATA}_SUCCESS`:
      return state.set('feederGraphData', action.data);
    case GET_FEEDER_MAP_DATA:
      return state.set('feederMapData', []);
    case `${GET_FEEDER_MAP_DATA}_SUCCESS`:
      return state.set('feederMapData', action.data);
    case `${GET_ENERGY_SUPPLIED_DATA}_SUCCESS`:
      return state.set('energySuppliedData', action.data);
    case `${GET_INSTANT_LOAD_DATA}_SUCCESS`:
      return state.set('instantLoadData', action.data);
    case `${GET_FEEDER_INFO}_SUCCESS`:
      return state.set('feederInfo', action.data);
    case `${GET_PF_DATA}_SUCCESS`:
      return state.set('pfData', action.data);
    case `${GET_BEST_WORST_DATA}_SUCCESS`:
      return state.set('bestWorst', action.data);
    case `${GET_COMPARISON_DATA}_SUCCESS`:
      return state.set('comparisonData', action.data);
    case `${CLEAR_COMPARISON_DATA}`:
      return state.set('comparisonData', {});
    default:
      return state;
  }
}

export default basePageReducer;
