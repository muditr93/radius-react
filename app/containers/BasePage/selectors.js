import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the basePage state domain
 */

const selectBasePageDomain = state => state.get('basePage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by BasePage
 */

const makeSelectBasePage = () => createSelector(selectBasePageDomain, substate => substate.toJS());

export default makeSelectBasePage;
export { selectBasePageDomain };
