/*
 *
 * BasePage constants
 *
 */

export const DEFAULT_ACTION = 'app/BasePage/DEFAULT_ACTION';
export const GET_FEEDER_STATS = 'app/BasePage/GET_FEEDER_STATS';
export const GET_FEEDER_GRAPH_DATA = 'app/BasePage/GET_FEEDER_GRAPH_DATA';
export const GET_FEEDER_MAP_DATA = 'app/BasePage/GET_FEEDER_MAP_DATA';
export const GET_ENERGY_SUPPLIED_DATA = 'app/BasePage/GET_ENERGY_SUPPLIED_DATA';
export const GET_INSTANT_LOAD_DATA = 'app/BasePage/GET_INSTANT_LOAD_DATA';
export const GET_FEEDER_INFO = 'app/BasePage/GET_FEEDER_INFO';
export const GET_INTERRUPTION_DATA = 'app/BasePage/GET_INTERRUPTION_DATA,';
export const GET_ELU_DATA = 'app/BasePage/GET_ELU_DATA';
export const GET_PF_DATA = 'app/BasePage/GET_PF_DATA';
export const GET_BEST_WORST_DATA = 'app/BasePage/GET_BEST_WORST_DATA';
export const AREA_CHANGE = 'app/BasePage/AREA_CHANGE';
export const GET_COMPARISON_DATA = 'app/BasePage/GET_COMPARISON_DATA';
export const CLEAR_COMPARISON_DATA = 'app/BasePage/CLEAR_COMPARISON_DATA';
export const GET_STATISTICAL_DATA = 'app/BasePage/GET_STATISTICAL_DATA';
