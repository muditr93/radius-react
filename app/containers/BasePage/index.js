/**
 *
 * BasePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import ReactHighcharts from 'react-highcharts';
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectBasePage from './selectors';
import reducer from './reducer';
import saga from './saga';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import RzDates from 'components/RzDates';
import RzCustomDate from 'components/RzCustomDate';
import ComparisonBoard from 'components/ComparisonBoard';
import RzELUTotal from 'components/RzELUTotal';
import RzCustomColumn from 'components/RzCustomColumn';
import RzBulletGraph from 'components/RzBulletGraph';
import RzGraphTable from 'components/RzGraphTable';
import RzStackedBarGraph from 'components/RzStackedBarGraph';
import RzLineGraph from 'components/RzLineGraph';
import RzPieChart from 'components/RzPieChart';
import HeaderButton from 'components/HeaderButton';
import HeaderButton0 from 'components/HeaderButton0';

import Pusher from 'components/Pusher';
import Page from 'containers/Page';
import FeederMap from 'components/FeederMap';
import FeederStatusBar from 'components/FeederStatusBar';
import InstantLoad from 'components/InstantLoad';
import PowerFactor from 'components/PowerFactor';
import Modal from 'react-modal';
import dayjs from 'dayjs';
import RzGauge from 'components/RzGauge';
import {
  getFeederStats,
  getFeederGraphData,
  getFeederMapData,
  handleAreaChange,
  getEnergySuppliedData,
  getInstantLoadData,
  getFeederInfo,
  getInterruptionData,
  getPowerFactorData,
  getBestWorstData,
  getComparisonData,
  clearComparisonData,
  getStatisticalData,
} from './actions';

/* eslint-disable react/prefer-stateless-function */

const style1 = {
  grid: {
    boxSizing: 'border-box',
  },
  title: {
    textAlign: 'center',
  },
  gridGauge: {
    boxSizing: 'border-box',
    height: '100%',
    background: 'white',
  },
  pieInfo: {
    position: 'relative',
    boxSizing: 'border-box',
    background: 'white',
    fontSize: '11px',
    height: '100%',
    padding: '10px',
  },
  pieText:{
    borderRadius: '5px',
    lineHeight: '24px',
    fontWeight: '600',
  },
  gridMinor: {
    paddingLeft: 0,
    paddingRight: 0,
  },
};
const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '2em 2em 0',
  },
  container: {
    display: 'flex',
    justifyContent: 'space-around',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
// Modal.setAppElement('#BasePage');

export class BasePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      maponly: true,
      modalIsOpen: false,
      comparisonType: '',
      bWCount: 4,
      performanceType: 'interruption',
    };
    this.markerClick = this.markerClick.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
  }
  openModal() {
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
    this.props.dispatch(clearComparisonData());
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }
  generatePieLegend = (data) => {
    let total = 0;
    data.forEach((element) => {
        total += Number(element['kvah']);
      })
    return data.map(x => (
    <div style={style1.pieText}>
      <div style={{ color: '#3E3D71', fontSize: '11px'}}>
        {x.discomName} :
      </div>
      <div style={{ color: '#777', fontSize: '10px'}}>
        { ` ${Number(((x['kvah'] / total) * 100).toFixed(2))} % (${x['kvah'].toFixed(2)} MVAh)`}
      </div>
    </div>
  ))}
  componentDidMount() {
    this.props.dispatch(getFeederStats());
    this.props.dispatch(getFeederGraphData());
    this.props.dispatch(getFeederMapData());
    this.props.dispatch(getEnergySuppliedData());
    this.props.dispatch(getInstantLoadData());
    this.props.dispatch(getInterruptionData());
    this.props.dispatch(getPowerFactorData());
    this.props.dispatch(getStatisticalData());
  }
  getCurrentTime = () => {
    var currentTime = new Date();
    var currentOffset = currentTime.getTimezoneOffset();
    var ISTOffset = 330;   // IST offset UTC +5:30
    var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);
    var hoursIST = ISTTime.getHours()
    var minutesIST = ISTTime.getMinutes()
    return (" - " + hoursIST + ":" + minutesIST + " " )
  }
  getDate = () => {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '/' + mm + '/'  + yyyy;
    return today;
  }
  hanleClick = () => {
    //
  };
handleTypeSelect = (passed) => {
  this.setState({
    performanceType: passed.type,
  })
  this.props.dispatch(getBestWorstData({ feederCount: this.state.bWCount, type: passed.type }));
}
  markerClick(data) {
    this.state.maponly
      ? this.setState({ maponly: false, feederName: data.name })
      : this.setState({ feederName: data.name });
    this.props.dispatch(getFeederInfo(data));
  }

  render() {
    const {
      classes,
      basepage: {
        area,
        feederStats,
        feederGraphData,
        feederMapData,
        energySuppliedData,
        instantLoadData,
        feederInfo,
        eluData,
        interruptionData,
        pfData,
        bestWorst,
        comparisonData,
        statistic,
      },
    } = this.props;

    const { maponly, modalIsOpen, comparisonType, bWCount } = this.state;

    return (
      <Page>
        <Pusher needPaper={false}>
          <div className={classes.root}>
            <Modal
              isOpen={modalIsOpen}
              onAfterOpen={this.afterOpenModal}
              onRequestClose={this.closeModal}
              style={customStyles}
              style={{
                content: {
                  top: '50%',
                  left: '50%',
                  right: 'auto',
                  bottom: 'auto',
                  marginRight: '-50%',
                  transform: 'translate(-50%, -50%)',
                  width: '90%',
                  maxHeight: '85%',
                },
              }}
            >
              <h2 ref={subtitle => (this.subtitle = subtitle)} />
              <ComparisonBoard
                dispatch={this.props.dispatch}
                getComparisonData={getComparisonData}
                comparisonData={comparisonData}
                comparisonDataOne={
                  comparisonData && comparisonData.pane === 'one' ? comparisonData : undefined
                }
                comparisonDataTwo={
                  comparisonData && comparisonData.pane === 'two' ? comparisonData : undefined
                }
                reset={!this.state.modalIsOpen}
                comparisonType={comparisonType}
              />
            </Modal>

            <Grid container spacing={24}>
              <Grid container spacing={24} className={classes.container}>
                {feederStats && feederStats.resources
                  ? feederStats.resources.map(item => (
                      <Grid
                        item
                        style={style1.gridMinor}
                        xs={6}
                        sm={2}
                        onClick={() => this.props.dispatch(handleAreaChange(item.id))}
                      >
                        <Zoom left>
                          <HeaderButton
                            active={area == item.id ? true : false}
                            all={item.installed}
                            live={item.live}
                            name={item.id}
                          />
                      </Zoom>
                      </Grid>
                    ))
                  : null}
              </Grid>
              <Grid>{/* <RzCustomDate /> */}</Grid>
              {area == 'ALL' ? (
                <Grid container spacing={24} style={{ paddingTop: '10px' }}>
                  <Grid item style={style1.grid} xs={12}>
                    <Zoom>
                      <FeederStatusBar
                        title={`Feeder Status - As on Date  (${this.getDate()} ${this.getCurrentTime()})`}
                        data={
                          feederGraphData &&
                          feederGraphData.resources &&
                          feederGraphData.resources.length > 0
                            ? feederGraphData.resources : null
                        }
                        categories={
                          feederGraphData &&
                          feederGraphData.resources &&
                          feederGraphData.resources.length > 0
                            ? feederGraphData.resources.map(x => x.discomName)
                            : null
                        }
                        notConnected={
                          feederGraphData &&
                          feederGraphData.resources &&
                          feederGraphData.resources.length > 0
                            ? feederGraphData.resources.map(x => +x.totalNCPercent.toFixed(2))
                            : null
                        }
                        down={
                          feederGraphData &&
                          feederGraphData.resources &&
                          feederGraphData.resources.length > 0
                            ? feederGraphData.resources.map(x => +x.totalDNPercent.toFixed(2))
                            : null
                        }
                        up={
                          feederGraphData &&
                          feederGraphData.resources &&
                          feederGraphData.resources.length > 0
                            ? feederGraphData.resources.map(x => +x.totalUPPercent.toFixed(2))
                            : null
                        }
                        nonParticipating={
                          feederGraphData &&
                          feederGraphData.resources &&
                          feederGraphData.resources.length > 0
                            ? feederGraphData.resources.map(x => +x.totalNPPercent.toFixed(2))
                            : null
                        }
                      />
                    </Zoom>
                  </Grid>
                  <Grid container spacing={24} className={classes.container}>
                    {
                      statistic && (
                        <Grid
                          item
                          style={style1.title}
                          xs={12}
                          sm={12}
                        >
                        <h4>
                          Feeder Status of Last 7 Days
                        </h4>
                      </Grid>
                      )
                    }
                    {statistic && statistic.resources
                      ? statistic.resources.map(item => (
                          <Grid
                            item
                            style={style1.grid}
                            xs={6}
                            sm={4}
                          >
                            <Zoom left>
                              <HeaderButton0
                                down={item.down}
                                id={item.id}
                                name={item.name}
                                up={item.up}
                              />
                            </Zoom>
                          </Grid>
                        ))
                      : null}
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    {energySuppliedData &&
                      energySuppliedData.resources &&
                      energySuppliedData.resources.length > 0 && (
                      <Grid container style={style1.gridGauge} spacing={0}>
                        <Grid item style={style1.gridGauge} sm={8}>
                          <Fade left>
                            {/*<RzPieChart
                              data={energySuppliedData.resources.map(x => {
                                return { name: x.discomName, y: x['kvah'] };
                              })}
                              compare={() => {
                                this.openModal();
                                this.setState({ comparisonType: 'feederEnergy' });
                              }}
                            />*/}
                            <RzGauge
                              data={energySuppliedData.resources.map(x => {
                                return { name: x.discomName, y: x['kvah'] };
                              })}
                              compare={() => {
                                this.openModal();
                                this.setState({ comparisonType: 'feederEnergy' });
                                }}
                              />
                          </Fade>
                        </Grid>
                        <Grid item style={style1.pieInfo} sm={4}>
                          <Zoom>
                            {this.generatePieLegend(energySuppliedData.resources)}
                          </Zoom>
                        </Grid>
                      </Grid>
                      )}
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    {instantLoadData &&
                      instantLoadData.resources &&
                      instantLoadData.resources.length > 0 && (
                      <Zoom right>
                        <InstantLoad
                          categories={
                            instantLoadData &&
                            instantLoadData.resources &&
                            instantLoadData.resources.length > 0
                              ? instantLoadData.resources.map(x => x.discomName)
                              : null
                          }
                          mw={
                            instantLoadData &&
                            instantLoadData.resources &&
                            instantLoadData.resources.length > 0
                              ? instantLoadData.resources.map(x =>
                                  Number(Math.abs(x.uom1Value).toFixed(2)),
                                )
                              : null
                          }
                          mva={
                            instantLoadData &&
                            instantLoadData.resources &&
                            instantLoadData.resources.length > 0
                              ? instantLoadData.resources.map(x =>
                                  Number(Math.abs(x.uom2Value).toFixed(2)),
                                )
                              : null
                          }
                        />
                    </Zoom>
                      )}
                  </Grid>

                  <Grid item style={style1.grid} xs={12}>
                    <FeederMap
                      dispatch={this.props.dispatch}
                      feeders={
                        feederMapData && feederMapData.resources ? feederMapData.resources : []
                      }
                      maponly={maponly}
                      feederInfo={
                        feederInfo && feederInfo.resources && feederInfo.resources.length > 0
                          ? feederInfo.resources[0]
                          : {}
                      }
                      getFeederInfo={this.markerClick}
                    />
                  </Grid>
                </Grid>
              ) : (
                <Grid container spacing={24} style={{ paddingTop: '10px' }}>
                  <Grid item style={style1.grid} xs={7}>
                    <FeederStatusBar
                      title={`Feeder Status - As on Date  (${this.getDate()} ${this.getCurrentTime()})`}
                      categories={
                        feederGraphData &&
                        feederGraphData.resources &&
                        feederGraphData.resources.length > 0
                          ? feederGraphData.resources.map(x => x.discomName)
                          : null
                      }
                      notConnected={
                        feederGraphData &&
                        feederGraphData.resources &&
                        feederGraphData.resources.length > 0
                          ? feederGraphData.resources.map(x => {
                            if (x) {
                              return +x.totalNCPercent.toFixed(2)
                            }
                            return 0
                          })
                          : null
                      }
                      down={
                        feederGraphData &&
                        feederGraphData.resources &&
                        feederGraphData.resources.length > 0
                          ? feederGraphData.resources.map(x => +x.totalDNPercent.toFixed(2))
                          : null
                      }
                      up={
                        feederGraphData &&
                        feederGraphData.resources &&
                        feederGraphData.resources.length > 0
                          ? feederGraphData.resources.map(x => +x.totalUPPercent.toFixed(2))
                          : null
                      }
                      nonParticipating={
                        feederGraphData &&
                        feederGraphData.resources &&
                        feederGraphData.resources.length > 0
                          ? feederGraphData.resources.map(x => +x.totalNPPercent.toFixed(2))
                          : null
                      }
                    />
                  </Grid>
                  <Grid item style={style1.grid} xs={5}>
                    {energySuppliedData &&
                      energySuppliedData.resources &&
                      energySuppliedData.resources.length > 0 && (
                        <Grid container style={style1.gridGauge} spacing={0}>
                          <Grid item style={style1.gridGauge} sm={8}>
                            <Fade left>
                              <RzGauge
                                data={energySuppliedData.resources.map(x => {
                                  return { name: x.discomName, y: x['kvah'] };
                                })}
                                compare={() => {
                                  this.openModal();
                                  this.setState({ comparisonType: 'feederEnergy' });
                                }}
                              />
                            </Fade>
                          </Grid>
                          <Grid item style={style1.pieInfo} sm={4}>
                            <Zoom>
                              {this.generatePieLegend(energySuppliedData.resources)}
                            </Zoom>
                          </Grid>
                        </Grid>
                      )}
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    {instantLoadData &&
                      instantLoadData.resources &&
                      instantLoadData.resources.length > 0 && (
                        <Fade right>
                          <InstantLoad
                            categories={
                              instantLoadData &&
                              instantLoadData.resources &&
                              instantLoadData.resources.length > 0
                                ? instantLoadData.resources.map(x => x.discomName)
                                : null
                            }
                            mw={
                              instantLoadData &&
                              instantLoadData.resources &&
                              instantLoadData.resources.length > 0
                                ? instantLoadData.resources.map(x => Number(x.uom1Value.toFixed(2)))
                                : null
                            }
                            mva={
                              instantLoadData &&
                              instantLoadData.resources &&
                              instantLoadData.resources.length > 0
                                ? instantLoadData.resources.map(x => Number(x.uom2Value.toFixed(2)))
                                : null
                            }
                          />
                        </Fade>
                      )}
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Zoom>
                      <PowerFactor
                        categories={
                          pfData && pfData.resources && pfData.resources.length > 0
                            ? pfData.resources.map(x => x.discomName)
                            : null
                        }
                        pf={
                          pfData && pfData.resources && pfData.resources.length > 0
                            ? pfData.resources.map(x => Number(Math.abs(x.pf).toFixed(2)))
                            : null
                        }
                      />
                    </Zoom>
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Fade right>
                      <RzGraphTable
                        title="Interruption"
                        series={interruptionData || []}
                        compare={() => {
                          this.openModal();
                          this.setState({ comparisonType: 'interruption' });
                        }}
                      />
                    </Fade>
                  </Grid>
                  {/*<Grid item style={style1.grid} xs={3}>
                    <Fade left>
                      <RzELUTotal
                        series={eluData}
                        title="Total Amount"
                        compare={() => {
                          this.openModal();
                          this.setState({ comparisonType: 'elu' });
                        }}
                      />
                    </Fade>
                  </Grid>*/}
                  {/*<Grid item style={style1.grid} xs={6}>
                    <Zoom>
                    <RzLineGraph />
                    </Zoom>
                  </Grid>*/}
                  <Grid item style={style1.grid} xs={6}>
                    <RzCustomColumn
                      best={
                        bestWorst && bestWorst.resources && bestWorst.resources.length > 0
                          ? bestWorst.resources.filter(x => x.status === 'BEST')
                          : []
                      }
                      worst={
                        bestWorst && bestWorst.resources && bestWorst.resources.length > 0
                          ? bestWorst.resources.filter(x => x.status === 'WORST')
                          : []
                      }
                      // bWCountUpdate={count => {
                      //
                      //   this.props.dispatch(getBestWorstData({ feederCount: count }));
                      // }}
                      selectType={this.handleTypeSelect}
                      performanceType={this.state.performanceType}
                      bWCount={bWCount}
                      bWCountUpdate={data => {
                        if (data.bWCount) {
                          this.setState(data);
                          this.props.dispatch(getBestWorstData({ feederCount: data.bWCount, type: this.state.performanceType }));
                        }
                      }}
                    />
                  </Grid>
                  <Grid item style={style1.grid} xs={12}>
                    <Zoom left>
                    <FeederMap
                      dispatch={this.props.dispatch}
                      feeders={
                        feederMapData && feederMapData.resources ? feederMapData.resources : []
                      }
                      maponly={maponly}
                      feederInfo={
                        feederInfo && feederInfo.resources && feederInfo.resources.length > 0
                          ? feederInfo.resources[0]
                          : {}
                      }
                      getFeederInfo={this.markerClick}
                    />
                  </Zoom>
                  </Grid>
                </Grid>
              )}
            </Grid>
          </div>
        </Pusher>
      </Page>
    );
  }
}

BasePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  basepage: makeSelectBasePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'basePage', reducer });
const withSaga = injectSaga({ key: 'basePage', saga });

export default compose(
  withReducer,
  withSaga,
  withStyles(styles),
  withConnect,
)(BasePage);
