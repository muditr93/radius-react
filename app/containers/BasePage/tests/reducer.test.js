import { fromJS } from 'immutable';
import basePageReducer from '../reducer';

describe('basePageReducer', () => {
  it('returns the initial state', () => {
    expect(basePageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
