import {
  DEFAULT_ACTION,
  GET_FEEDER_STATS,
  GET_FEEDER_GRAPH_DATA,
  GET_FEEDER_MAP_DATA,
  GET_ENERGY_SUPPLIED_DATA,
  AREA_CHANGE,
  GET_INSTANT_LOAD_DATA,
  GET_FEEDER_INFO,
  GET_INTERRUPTION_DATA,
  GET_ELU_DATA,
  GET_PF_DATA,
  GET_BEST_WORST_DATA,
  GET_COMPARISON_DATA,
  CLEAR_COMPARISON_DATA,
  GET_STATISTICAL_DATA,
} from './constants';

export function getELUAmountData() {
  return {
    type: GET_ELU_DATA,
  };
}
export function getStatisticalData() {
  return {
    type: GET_STATISTICAL_DATA,
  };
}

export function getInterruptionData() {
  return {
    type: GET_INTERRUPTION_DATA,
  };
}

export function handleAreaChange(data) {
  return {
    type: AREA_CHANGE,
    data,
  };
}

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getFeederStats(data) {
  return {
    type: GET_FEEDER_STATS,
    data,
  };
}

export function getFeederGraphData(data) {
  return {
    type: GET_FEEDER_GRAPH_DATA,
    data,
  };
}
export function getFeederMapData(data) {
  return {
    type: GET_FEEDER_MAP_DATA,
    data,
  };
}

export function getEnergySuppliedData(data) {
  return {
    type: GET_ENERGY_SUPPLIED_DATA,
    data,
  };
}

export function getInstantLoadData(data) {
  return {
    type: GET_INSTANT_LOAD_DATA,
    data,
  };
}

export function getFeederInfo(data) {
  return {
    type: GET_FEEDER_INFO,
    data,
  };
}

export function getPowerFactorData(data) {
  return {
    type: GET_PF_DATA,
    data,
  };
}

export function getBestWorstData(data) {
  return {
    type: GET_BEST_WORST_DATA,
    data,
  };
}

export function getComparisonData(data) {
  return {
    type: GET_COMPARISON_DATA,
    data,
  };
}

export function clearComparisonData(data) {
  return {
    type: CLEAR_COMPARISON_DATA,
    data,
  };
}
