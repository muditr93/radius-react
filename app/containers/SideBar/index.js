/**
 *
 * Sidebar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Drawer from 'material-ui/Drawer';
import Menu from 'material-ui/Menu';
import { List, ListItem } from 'material-ui/List';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSidebar from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { toggle } from './actions';

export class Sidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  itemClickHandler(url) {
    this.props.dispatch(toggle());
    this.props.dispatch(push(url));
  }

  render() {
    return (
      <Drawer
        open={this.props.sidebar.show}
        docked={false}
        style={{ zIndex: 1300, color: 'white' }}
        containerStyle={{ paddingTop: '70px', backgroundColor: '#2C4F6F', color: 'white' }}
        onRequestChange={() => this.props.dispatch(toggle())}
      >
        <List nestedListStyle={{ color: 'white' }}  style={{ color: 'white' }}>
          <ListItem
            style={{ color: 'white', borderBottom: '1px solid white' }}
            onClick={() => this.itemClickHandler('/')}>
            <img src="https://image.ibb.co/gfHaXq/Menu-Dashboard.png" alt="Menu-Dashboard" style={{marginRight: '10px'}}height="25" border="0" />
            <FormattedMessage {...messages.dashboard} />
          </ListItem>
          <ListItem
            style={{ color: 'white', borderBottom: '1px solid white' }} onClick={() => this.itemClickHandler('/feeders')}>
            <img src="https://image.ibb.co/kObPQA/Menu-Reports.png" alt="Menu-Reports" style={{marginRight: '10px'}}height="25" border="0" />
            <FormattedMessage {...messages.feeders} />
          </ListItem>
          <ListItem
            style={{ color: 'white', borderBottom: '1px solid white' }} onClick={() => this.itemClickHandler('/')}>
            <img src="https://image.ibb.co/mfRr5A/Menu-Feeders.png" alt="Menu-Feeders" style={{marginRight: '10px'}}height="25" border="0" />
            <FormattedMessage {...messages.feederData} />
          </ListItem>
          <ListItem
            style={{ color: 'white', borderBottom: '1px solid white' }} onClick={() => this.itemClickHandler('/feederDetails')}>
            <img src="https://image.ibb.co/i3xndV/Menu-Feeder-Details.png" alt="Menu-Feeder-Details" style={{marginRight: '10px'}}height="25" border="0" />
            <FormattedMessage {...messages.FeederDetails} />
          </ListItem>
          <ListItem
            style={{ color: 'white', borderBottom: '1px solid white' }}
              nestedItems={[
                <ListItem
                  onClick={() => this.itemClickHandler('/gateway')}
                  style={{ color: 'white', borderBottom: '1px solid white' }}
                  primaryText="Gateway  Details"
                />,
                <ListItem
                  onClick={() => this.itemClickHandler('/noConsumptionFeeder')}
                  style={{ color: 'white', borderBottom: '1px solid white' }}
                  primaryText="No Consumption Feeder"
                />,
                <ListItem
                  onClick={() => this.itemClickHandler('/averageOuting')}
                  style={{ color: 'white', borderBottom: '1px solid white' }}
                  primaryText="Average Outage"
                />
              ]}
            >
            <img src="https://image.ibb.co/kObPQA/Menu-Reports.png" alt="Menu-Reports" style={{marginRight: '10px'}}height="25" border="0" />
            <FormattedMessage {...messages.MisReport} />

          </ListItem>
        </List>
      </Drawer>
    );
  }
}

Sidebar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  sidebar: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sidebar: makeSelectSidebar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'sidebar', reducer });
const withSaga = injectSaga({ key: 'sidebar', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Sidebar);
