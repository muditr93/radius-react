/*
 * Sidebar Messages
 *
 * This contains all the text for the Sidebar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  dashboard: {
    id: 'app.containers.Sidebar.dashboard',
    defaultMessage: 'Dashboard',
  },
  feeders: {
    id: 'app.containers.Sidebar.feeders',
    defaultMessage: 'Feeders',
  },
  feederData: {
    id: 'app.containers.Sidebar.feederData',
    defaultMessage: 'Feeder Data',
  },
  FeederDetails: {
    id: 'app.containers.Sidebar.FeederDetails',
    defaultMessage: 'Feeder Details',
  },
  gateway: {
    id: 'app.containers.Sidebar.gateway',
    defaultMessage: 'Gateway Details',
  },
  MisReport: {
    id: 'app.containers.Sidebar.MisReport',
    defaultMessage: 'MIS Report',
  },
  caseEdit: {
    id: 'app.containers.Sidebar.caseEdit',
    defaultMessage: 'Case Edit',
  },
  report: {
    id: 'app.containers.Sidebar.report',
    defaultMessage: 'Report',
  },
  qcReview: {
    id: 'app.containers.Sidebar.qcReview',
    defaultMessage: 'qcReview',
  },
  caseDetails: {
    id: 'app.containers.Sidebar.caseDetails',
    defaultMessage: 'caseDetails',
  },
  requesterList: {
    id: 'app.containers.Sidebar.requesterList',
    defaultMessage: 'Requesters',
  },
});
