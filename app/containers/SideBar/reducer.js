/*
 *
 * Sidebar reducer
 *
 */

import { fromJS } from 'immutable';
import {
  TOGGLE,
} from './constants';

const initialState = fromJS({
  show: false,
});

function sidebarReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE:
      return state.set('show', !state.get('show'));
    default:
      return state;
  }
}

export default sidebarReducer;
