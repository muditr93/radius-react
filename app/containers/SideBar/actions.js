/*
 *
 * Sidebar actions
 *
 */

import {
  TOGGLE,
} from './constants';

export function toggle() {
  return {
    type: TOGGLE,
  };
}
