/*
 *
 * RequesterListPage actions
 *
 */

 import {
   OPEN_EDITOR,
   CLOSE_EDITOR,
   FETCH_NEW_USERS,
   EDIT_USER,
   NEW_USERS_LOADED,
   USER_LOAD_FAILURE,
   USER_LIST_UPDATED,
   PREFILL_USER,
   ALTER_OFFSET,
 } from './constants';
 export function alterOffset(value) {
   return {
     type: ALTER_OFFSET,
     value,
   };
 }
 export function preFillUserDetails(value) {
   return {
     type: PREFILL_USER,
     value,
   };
 }
 export function userListUpdated(value) {
   return {
     type: USER_LIST_UPDATED,
     value,
   };
 }
 export function openEditor(value) {
   return {
     type: OPEN_EDITOR,
     value,
   };
 }
 export function closeEditor() {
   return {
     type: CLOSE_EDITOR,
   };
 }
 export function fetchNewUsers(value) {
   return {
     type: FETCH_NEW_USERS,
     value,
   };
 }

 export function newUsersLoaded(value) {
   return {
     type: NEW_USERS_LOADED,
     value,
   };
 }
 export function editUser() {
   return {
     type: EDIT_USER,
   };
 }
 export function userLoadFailuer() {
   return {
     type: USER_LOAD_FAILURE,
   };
 }
