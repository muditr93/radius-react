/**
 *
 * NoConsumptionFeeder
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { push } from 'react-router-redux';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectNoConsumptionFeeder from "./selectors";
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import Dialog from 'material-ui/Dialog';
import Edit from 'material-ui/svg-icons/image/edit';
import { Helmet } from 'react-helmet';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Page from 'containers/Page';
import Pusher from 'components/Pusher';
import RzTable from 'components/RzTable';
import { columns } from './constants';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {
  openEditor,
  closeEditor,
  alterOffset,
  fetchNewUsers,
} from './actions';
const Wrapper = styled.div`
   overflow: hidden;
   height: inherit;
   .dot {

  }
 `;

 const styles = theme => ({
  fab: {

  },
  absolute: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
});
 const Greendot = styled.div`
   height: 13px;
   width: 13px;
   background-color: green;
   border-radius: 50%;
   display: inline-block;
 `;
 const Reddot = styled.div`
   height: 13px;
   width: 13px;
   background-color: red;
   border-radius: 50%;
   display: inline-block;
 `;

export class NoConsumptionFeeder extends React.Component {
  componentDidMount() {
    const {
      noconsumptionfeeder: {
        offset,
      },
    } = this.props;
    const Noffset = offset * 10;
    // this.props.dispatch(fetchNewUsers(Noffset));
  }
  handleSelect = () => {

  }
  handleClickName = (e, id) => {
    this.props.dispatch(push(`feederDetails/${id}`));
  }
  handleClick(i) {
    const {
      noconsumptionfeeder: {
        offset,
      },
    } = this.props;
    if (i !== offset) {
      const Noffset = i * 10;
      this.props.dispatch(alterOffset(i));
      this.props.dispatch(fetchNewUsers(Noffset));
    }
  }

  render() {
    const { classes } = this.props;
    const {
      dispatch,
      noconsumptionfeeder: {
        substations,
        offset,
        totalCount,
        loaded,
      },
    } = this.props;
    const renderCell = (type, rowData) => {
      switch (type) {
        case 'name':
          return (
            <div onClick={() => this.handleClickName(rowData.feederId)}>{rowData[type]||"--"}</div>
          )
        case 'voltage':
          return (
            <div>{`${rowData['r_Voltage']} | ${rowData['y_Voltage']} | ${rowData['b_Voltage']}`}</div>
          )
        case 'current':
          return (
            <div>{`${rowData['r_Current']} | ${rowData['y_Current']} | ${rowData['b_Current']}`}</div>
          )
        case 'pf':
          return (
            <div>{`${rowData['r_PF']} | ${rowData['y_PF']} | ${rowData['b_PF']} | ${rowData['cumm_pf']}`}</div>
          )
        case 'incomer':
          return (
            <div title={`SINCE ${rowData['incommerStatusTime']|| '--'}`}>
              <IconButton aria-label="Delete">
              <div>{rowData['incomer'] ? (<Greendot />): (<Reddot/>)}</div>
              </IconButton>
            </div>
          )
        case 'cb':
          return (
            <div title={`SINCE ${rowData['cbStatusTime'] || '--'}`}>
              {rowData['cb'] ? (<Greendot />): (<Reddot />)}
              <div style={{position: 'absolute', bottom: '0', right: '7px', textAlign: 'right'}}>
              </div>
            </div>

          )
          break;
        default:
        return (<div>
          {rowData[type]||"--"}
        </div>)
      }
    };
    return (
      <Page>
        <Pusher>
          <Wrapper>
            <Helmet>
              <title>Radius</title>
              <meta name="description" content="Description of RequesterList" />
            </Helmet>
              <FormattedMessage {...messages.header}>
                {
                  message => (
                    <RzTable
                      handleSubmit={fetchNewUsers}
                      loaded={loaded}
                      data={substations}
                      totalCount={totalCount}
                      columns={columns}
                      handleSelect={this.handleSelect}
                      title={message}
                      pageCount={offset}
                      handleClick={this.handleClick.bind(this)}
                      renderCell={renderCell}
                      filter={{
                        search: true,
                      }}
                    />
                  )
                }
              </FormattedMessage>
          </Wrapper>
        </Pusher>
      </Page>
    );
  }
}

NoConsumptionFeeder.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  noconsumptionfeeder: makeSelectNoConsumptionFeeder()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "noConsumptionFeeder", reducer });
const withSaga = injectSaga({ key: "noConsumptionFeeder", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(NoConsumptionFeeder);
