/*
 *
 * NoConsumptionFeeder constants
 *
 */

 export const DEFAULT_ACTION = "app/NoConsumptionFeeder/DEFAULT_ACTION";
 export const OPEN_EDITOR = 'app/NoConsumptionFeeder/OPEN_EDITOR';
 export const CLOSE_EDITOR = 'app/NoConsumptionFeeder/CLOSE_EDITOR';
 export const FETCH_NEW_USERS = 'app/NoConsumptionFeeder/FETCH_NEW_USERS';
 export const SELECT_USER = 'app/NoConsumptionFeeder/SELECT_USER';
 export const EDIT_USER = 'app/NoConsumptionFeeder/EDIT_USER';
 export const NEW_USERS_LOADED = 'app/NoConsumptionFeeder/NEW_USERS_LOADED';
 export const USER_LOAD_FAILURE = 'app/NoConsumptionFeeder/USER_LOAD_FAILURE';
 export const USER_LIST_UPDATED = 'app/NoConsumptionFeeder/USER_LIST_UPDATED';
 export const PREFILL_USER = 'app/NoConsumptionFeeder/PREFILL_USER';
 export const ALTER_OFFSET = 'app/NoConsumptionFeeder/ALTER_OFFSET';
 export const columns = [
   {
     sort: 0,
     type: 'name',
     name: 'Name',
     size: 1,
   },
   {
     sort: 1,
     type: 'serialNo',
     name: 'Meter ID ',
     size: 1,
   },
   {
     sort: 2,
     type: 'mcUID',
     name: 'Modem Number',
     size: 1,
   },
   {
     sort: 3,
     type: 'projectName',
     name: 'Project',
     size: 1,
   },
   {
     sort: 4,
     type: 'lastReadingUpdated',
     name: 'Incremental data ',
     size: 1,
   },
   {
     sort: 5,
     type: 'lastPacketTime',
     name: 'Last Update ',
     size: 1,
   },
   {
     sort: 6,
     type: 'incomer',
     name: 'Incomer Status',
     size: 1,
   },
   {
     sort: 7,
     type: 'cb',
     name: 'CB Status',
     size: 1,
   },
 ];
