/**
 *
 * Asynchronously loads the component for NoConsumptionFeeder
 *
 */

import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
