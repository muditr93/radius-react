import { fromJS } from 'immutable';
import noConsumptionFeederReducer from '../reducer';

describe('noConsumptionFeederReducer', () => {
  it('returns the initial state', () => {
    expect(noConsumptionFeederReducer(undefined, {})).toEqual(fromJS({}));
  });
});
