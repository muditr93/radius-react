import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the noConsumptionFeeder state domain
 */

const selectNoConsumptionFeederDomain = state =>
  state.get("noConsumptionFeeder", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by NoConsumptionFeeder
 */

const makeSelectNoConsumptionFeeder = () =>
  createSelector(selectNoConsumptionFeederDomain, substate => substate.toJS());

export default makeSelectNoConsumptionFeeder;
export { selectNoConsumptionFeederDomain };
