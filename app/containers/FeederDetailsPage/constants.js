/*
 *
 * FeederDetailsPage constants
 *
 */

export const DEFAULT_ACTION = "app/FeederDetailsPage/DEFAULT_ACTION";
export const FETCH_FEEDER_DETAILS = "app/FeederDetailsPage/FETCH_FEEDER_DETAILS";
export const FETCH_FEEDER_DATA = "app/FeederDetailsPage/FETCH_FEEDER_DATA";
export const FETCH_NEW_USERS = "app/FeederDetailsPage/FETCH_NEW_USERS";
export const FETCH_MONTHLY_FEEDER = "app/FeederDetailsPage/FETCH_MONTHLY_FEEDER";
export const FETCH_FEEDER = "app/FeederDetailsPage/FETCH_FEEDER";
export const GET_MONTHLY_SLA = "app/FeederDetailsPage/GET_MONTHLY_SLA";
export const GET_INSTALLATION_DETAILS = "app/FeederDetailsPage/GET_INSTALLATION_DETAILS";
export const GET_REMARKS = "app/FeederDetailsPage/GET_REMARKS";
export const GET_INTERRUPTION_DETAIL = "app/FeederDetailsPage/GET_INTERRUPTION_DETAIL";
