/*
 *
 * FeederDetailsPage reducer
 *
 */

import { fromJS } from "immutable";
import { DEFAULT_ACTION, FETCH_FEEDER, FETCH_FEEDER_DETAILS, FETCH_FEEDER_DATA, FETCH_MONTHLY_FEEDER, GET_MONTHLY_SLA, GET_INSTALLATION_DETAILS, GET_REMARKS } from "./constants";

export const initialState = fromJS({
  logData: {},
  feeder: {},
  loaded: false,
  monthly:{},
  sla:{},
  logError: false,
  monthlyLoaded: false,
  install: [],
  remarks: {},
  idFound: false,
});

function feederDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FETCH_FEEDER_DETAILS:
      return state;
    case `${GET_REMARKS}_SUCCESS`:
      return state.set('remarks', action.data);
    case `${GET_INSTALLATION_DETAILS}_SUCCESS`:
      return state.set('install', action.data);
    case `${FETCH_FEEDER}_SUCCESS`:
      return state.set('idFound', true);
    case `${GET_MONTHLY_SLA}_SUCCESS`:
      return state.set('sla', action.data);
    case `${FETCH_MONTHLY_FEEDER}_SUCCESS`:
      return state
        .set('monthlyLoaded', true)
        .set('monthly', action.data);
    case `${FETCH_FEEDER_DATA}_FAILURE`:
      return state
        .set('loaded', true)
        .set('logError', action.data)
    case `${FETCH_FEEDER_DATA}_SUCCESS`:
      return state
      .set('loaded', true)
      .set('logData', action.data);
    case `${FETCH_FEEDER_DETAILS}_SUCCESS`:
      return state.set('feeder', action.data);
    default:
      return state;
  }
}

export default feederDetailsPageReducer;
