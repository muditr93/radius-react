/*
 *
 * FeederDetailsPage actions
 *
 */

import { DEFAULT_ACTION, FETCH_FEEDER_DETAILS, FETCH_FEEDER_DATA, FETCH_MONTHLY_FEEDER, FETCH_FEEDER, GET_MONTHLY_SLA, GET_INSTALLATION_DETAILS, GET_REMARKS } from "./constants";

export function getInstallationDetails(value){
  return {
    type: GET_INSTALLATION_DETAILS,
    value
  }
}

export function getRemarks(value) {
  return {
    type: GET_REMARKS,
    value
  };
}

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function getMonthlySLA(id, date) {
  return {
    type: GET_MONTHLY_SLA,
    id,
    date,
  };
}

export function fetchMonthlyFeeder(id, date) {
  return {
    type: FETCH_MONTHLY_FEEDER,
    id,
    date,
  };
}

export function fetchFeederData(id, date) {
  return {
    type: FETCH_FEEDER_DATA,
    id,
    date,
  };
}

export function fetchFeederDetails(id, date) {
  return {
    type: FETCH_FEEDER_DETAILS,
    id,
    date,
  };
}

export function fetchFeeder(value) {
  return {
    type: FETCH_FEEDER,
    value,
  };
}
