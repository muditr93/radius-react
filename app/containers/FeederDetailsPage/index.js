/**
 *
 * FeederDetailsPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectFeederDetailsPage from "./selectors";
import reducer from "./reducer";
import saga from "./saga";

import Dialog from 'material-ui/Dialog';
import Edit from 'material-ui/svg-icons/image/edit';
import Delete from 'material-ui/svg-icons/action/delete';
import RzLineGraph from 'components/RzLineGraph';
import RzLineGraph1 from 'components/RzLineGraph1';
import RzLineGraph2 from 'components/RzLineGraph2';

import Tooltip from '@material-ui/core/Tooltip';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Page from 'containers/Page';
import Pusher from 'components/Pusher';
import FeederHeaderForm from 'containers/FeederHeaderForm';
import {fetchFeederData, getMonthlySLA, fetchFeederDetails, fetchMonthlyFeeder, fetchFeeder, getInstallationDetails, getRemarks} from './actions';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import RzLineGraph0 from 'components/RzLineGraph0';

import GraphTitle from 'components/GraphTitle';
const Wrapper = styled.div`
   overflow: hidden;
   height: inherit;
   .dot {

  }
 `;
 const styles = theme => ({
   full:{
     width: '100%'
   },
   root1: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  title: {
    fontSize:'16px',
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
   root: {
     flexGrow: 1,
     padding: '2em 2em 0',
   },
   heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
   containerSmall:{
     padding:'20px',
     display: 'flex',
     justifyContent: 'space-around',
   },
   container: {
     display: 'flex',
     justifyContent: 'space-around',
   },
   containerMinor: {
     padding: '10px',
     display: 'flex',
     justifyContent: 'space-around',
   },
   paper: {
     padding: theme.spacing.unit * 2,
     textAlign: 'center',
     color: theme.palette.text.secondary,
   },
   selectEmpty: {
     marginTop: theme.spacing.unit * 2,
   },
  absolute: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
});

const Greendot = styled.div`
  height: 13px;
  width: 13px;
  background-color: green;
  border-radius: 50%;
  display: inline-block;
`;
const Reddot = styled.div`
  height: 13px;
  width: 13px;
  background-color: red;
  border-radius: 50%;
  display: inline-block;
`;

const style1 = {
  grid: {
    boxSizing: 'border-box',
  },
  grid2:{
    boxSizing: 'border-box',
    height: '100%',
  },
  label: {width: '100%', color: 'grey', fontSize: '13px', textAlign: 'left'},
  block: { width: '100%', textAlign: 'center', fontSize: '13px', textAlign: 'left'},
  pieInfo: {
    position: 'relative',
    boxSizing: 'border-box',
    background: 'white',
    fontSize: '11px',
    padding: '10px',
  },
  pieText:{
    borderRadius: '5px',
    lineHeight: '24px',
    fontWeight: '600',
  },
  gridMinor: {
    paddingLeft: 0,
    paddingRight: 0,
  },
};
const monthFinder = () => {
  var d = new Date();
  var n = d.getMonth() + 1;
  if (n < 10) {
    n = `0${n}-2019`
  }
  return n;
}
export class FeederDetailsPage extends React.Component {
  constructor(props){
    super();
    this.state = {
      searchBox: false,
      month: monthFinder()
    }
  }
  componentDidMount(){
    const {match: {params: {id } } } = this.props
    console.log(id)
    // this.props.dispatch(fetchFeederData(id))
    if (!id) {
      this.setState({
        searchBox: true
      })
    } else {
      this.props.dispatch(fetchFeederData(id))
      this.props.dispatch(fetchMonthlyFeeder(id))
      this.props.dispatch(fetchFeederDetails(id))
      this.props.dispatch(getMonthlySLA(id))
      this.props.dispatch(getRemarks(id))
      this.props.dispatch(getInstallationDetails(id))
    }
  }
  componentWillReceiveProps(nextProps, state) {
    const {match: {params: {id } } } = this.props;
    if (nextProps.feederdetailspage.idFound && !this.props.feederdetailspage.idFound) {
      this.props.dispatch(fetchFeederData(id))
      this.props.dispatch(fetchMonthlyFeeder(id))
      this.props.dispatch(fetchFeederDetails(id))
      this.props.dispatch(getMonthlySLA(id))
      this.props.dispatch(getRemarks(id))
      this.props.dispatch(getInstallationDetails(id))
      this.setState({
        searchBox: false
      })
    }
  }
  fetchFeeder = () => {
    this.props.dispatch(fetchFeeder())
  }
  handleDateChangeSLA = (date) => {
    const {match: {params: {id } } } = this.props;
    this.props.dispatch(getMonthlySLA(id, date))
  }
  handleDateChange = (date) => {
    const {match: {params: {id } } } = this.props;
    this.props.dispatch(fetchFeederData(id, date))
  }
  handleDateChange2 = (date) => {
    const {match: {params: {id } } } = this.props;
    let dt = date.split('-');
    this.setState({
      month: `${dt[1]}-${dt[0]}`
    })
    this.props.dispatch(fetchMonthlyFeeder(id, date))
  }
  logGrpahRender = () => {
    return (
      <RzLineGraph1
        data={logData}
        handleDateChange={this.handleDateChange}
        />
    )
  }
  render() {
    const { searchBox } = this.state;
    const {classes, feederdetailspage:{ logData, monthly, loaded, sla, install, logError, monthlyLoaded, monthly: {monthlyVitalStatistics, energySupplied}, feeder }} = this.props;
    return (
      <Page>
        <Pusher
          needPaper={false}
          >
          <div className={classes.root}>
            <Grid container spacing={24}>
              {searchBox ? (
              <Grid container spacing={24} className={classes.containerSmall} >
                <Grid item style={style1.grid} xs={12}>
                  <FeederHeaderForm
                    feederField
                    handleSubmit={fetchFeeder}
                  />
                </Grid>
              </Grid>
              ):(
                <Grid container spacing={24} className={classes.container}>
                  <Grid item style={style1.grid} xs={12}>
                    {loaded && logData && logData.load && logData.load.length ?
                      (
                        <RzLineGraph1
                          data={logData}
                          logError={logError}
                          handleDateChange={this.handleDateChange}
                          />):(<div style={{textAlign: 'center'}}><img src="https://i.ibb.co/9G9K0RN/Double-Ring-1s-200px.gif" alt="Double-Ring-1s-200px" border="0" /> </div>) }

                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    {energySupplied && energySupplied.kvah && energySupplied.kvah.length ?(  <RzLineGraph2
                      data={energySupplied}
                      handleDateChange={this.handleDateChange2}
                      />):(<div style={{textAlign: 'center'}}>{monthlyLoaded && energySupplied.kvah && energySupplied.kvah.length ? (<img src="https://i.ibb.co/9G9K0RN/Double-Ring-1s-200px.gif" alt="Double-Ring-1s-200px" border="0" />):('Data not Available')}</div>)}
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Paper style={style1.grid2}>
                      <GraphTitle title="Details" />
                      {feeder && feeder.cbIncommerSatus && feeder.cbIncommerSatus.length ?(
                        <Grid container spacing={24} className={classes.containerMinor} >
                          <Grid item style={style1.grid} xs={6}>
                              <div title={`SINCE ${feeder.cbIncommerSatus[0].cb_status_time|| '--'}`} style={style1.label}>
                                CB
                              </div>
                              <div style={style1.block}>
                                {feeder.cbIncommerSatus[0].digital_input3 == "1" ? (<Greendot />):(<Reddot />)}
                              </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6} >
                            <div style={style1.label} title={`SINCE ${feeder.cbIncommerSatus[0].incommer_status_time|| '--'}`}>
                              Incommer
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].digital_input2 == "1" ? (<Greendot />):(<Reddot />)}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              Modem Number
                            </div>
                            <div style={style1.block}>
                            {feeder.cbIncommerSatus[0].mc_UID}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              Meter Number
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].serial_no}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              Signal Strength
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].csq_signal_strength}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                            Last Packet
                            </div>
                            <div style={style1.block}>
                            {feeder.cbIncommerSatus[0].last_packet_time}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                            UOM
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].uom}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              CTMF
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].meter_ct_mf}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              Reading (VAH)
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].kvah}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              Load (VA)
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].instant_cum_KVA}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              MD
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].max_demand_KVA}
                            </div>
                          </Grid>
                          <Grid item style={style1.grid} xs={6}>
                            <div style={style1.label}>
                              MD Time
                            </div>
                            <div style={style1.block}>
                              {feeder.cbIncommerSatus[0].max_demand_time}
                            </div>
                          </Grid>
                        </Grid>
                      ):(<div style={{textAlign: 'center'}}><img src="https://i.ibb.co/9G9K0RN/Double-Ring-1s-200px.gif" alt="Double-Ring-1s-200px" border="0" /></div>)}
                    </Paper>
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Paper style={style1.grid2}>
                      <GraphTitle title="Today's Vital Stats" />
                        {monthly.monthlyVitalStatistics ? (<Grid container spacing={24} className={classes.containerMinor} >
                          <Grid item style={style1.grid} xs={9}>
                            Interruption Count
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            {feeder.todayVitalStatistics.outage_count}
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            Interruption Duration
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            {feeder.todayVitalStatistics.outage_duration_min}
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            {'EST EUL(+15%)MVAh'}
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            {feeder.todayVitalStatistics.kvah_loss_EUL1}
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            {'EST EUL(0%)MVAh'}
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            {feeder.todayVitalStatistics.kvah_loss_EUL2}
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            {'EST EUL(-15%)MVAh'}
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            {feeder.todayVitalStatistics.kvah_loss_EUL3}
                          </Grid>
                        </Grid>):(null)}
                    </Paper>
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Paper style={style1.grid2}>
                      <GraphTitle title={`Month ${this.state.month}`} />
                      {monthlyVitalStatistics ? (
                      <Grid container spacing={24} className={classes.containerMinor} >
                        <Grid item style={style1.grid} xs={6}>
                            Interruption Count
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          {monthlyVitalStatistics.no_of_interuption}
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          Less Than 5 mins
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          {`${monthlyVitalStatistics.outagelessthan5min} - ${monthlyVitalStatistics.outagelessthan5min_duration}`}
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          Between 5 to 30 mins
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          {`${monthlyVitalStatistics.outageduration5to30min} - ${monthlyVitalStatistics.outageduration5to30min_duration}`}
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          More than 30 mins
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          {`${monthlyVitalStatistics.outagemorethan30min} - ${monthlyVitalStatistics.outagemorethan30min_duration}`}
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          SAIFI
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          --
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          SAIDI
                        </Grid>
                        <Grid item style={style1.grid} xs={6}>
                          --
                        </Grid>
                      </Grid>
                    ):(null)}
                    </Paper>
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Paper style={style1.grid2}>
                      <GraphTitle title="Overview" />
                        <Grid container spacing={24} className={classes.containerMinor} >
                          <Grid item style={style1.grid} xs={9}>
                            HV 1
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            NA
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            HV 2
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            NA
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            Commercial
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            NA
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            Contracted Load
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            NA
                          </Grid>
                          <Grid item style={style1.grid} xs={9}>
                            Ideal Load
                          </Grid>
                          <Grid item style={style1.grid} xs={3}>
                            NA
                          </Grid>
                        </Grid>
                    </Paper>
                  </Grid>
                  <Grid item style={style1.grid} xs={6}>
                    <Paper style={style1.grid2}>
                      <GraphTitle title="Feeder Contact Details" />
                      {feeder && feeder.cbIncommerSatus && feeder.contactDetails.length ? (
                        <Grid container spacing={24} className={classes.containerMinor} >
                          <Grid item style={style1.grid} xs={4}>
                            <span style={{fontWeight: '600'}}>DESIG.</span>
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            <span style={{fontWeight: '600'}}>NAME</span>
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            <span style={{fontWeight: '600'}}>MOBILE NO.</span>
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            EE
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].ee_name}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].ee_mob}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            SDO
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].sdo_name}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].sdo_mob}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            JE
                          </Grid>
                          <Grid item style={style1.je_name} xs={4}>
                            {feeder.contactDetails[0].je_name}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].je_mob}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            SSO
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].sso_name}
                          </Grid>
                          <Grid item style={style1.grid} xs={4}>
                            {feeder.contactDetails[0].sso_mob}
                          </Grid>
                        </Grid>): (null)}
                    </Paper>
                  </Grid>
                </Grid>
              )}
              <br />
              <Grid item style={style1.grid} style={{marginTop: '12px'}} xs={12}>
                <div className={classes.full}>
                        {
                          install && install.length ? (
                            <ExpansionPanel>
                              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                Install Details
                              </ExpansionPanelSummary>
                              <ExpansionPanelDetails>
                                <div className={classes.root0}>
                                <GridList className={classes.gridList} cols={2.5}>
                                  {install.map((item, i) => (
                                    <GridListTile key={i} >
                                      <img src={item.imageUrl} alt={item.imageUrl} />
                                      <GridListTileBar
                                        title={item.title}
                                        classes={{
                                          root: classes.titleBar,
                                          title: classes.title,
                                        }}
                                      />
                                    </GridListTile>
                                  ))}
                                  </GridList>
                                </div>
                              </ExpansionPanelDetails>
                            </ExpansionPanel>
                          ): (
                            null
                          )
                        }
                    {
                      sla && sla.requiredPacket && sla.requiredPacket.length ? (
                        <ExpansionPanel>
                          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            Monthly SLA
                          </ExpansionPanelSummary>
                          <ExpansionPanelDetails>
                            <div className={classes.full}>
                              <RzLineGraph0
                                data={sla}
                                handleDateChange={this.handleDateChangeSLA}
                                />
                            </div>
                          </ExpansionPanelDetails>
                        </ExpansionPanel>
                      ): ( null)
                    }
                </div >
              </Grid>

            </Grid>
          </div>
        </Pusher>
      </Page>
    );
  }
}

FeederDetailsPage.propTypes = {
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  feederdetailspage: makeSelectFeederDetailsPage()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "feederDetailsPage", reducer });
const withSaga = injectSaga({ key: "feederDetailsPage", saga });

export default compose(
  withStyles(styles),
  withReducer,
  withSaga,
  withConnect
)(FeederDetailsPage);
