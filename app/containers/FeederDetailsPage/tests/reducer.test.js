import { fromJS } from 'immutable';
import feederDetailsPageReducer from '../reducer';

describe('feederDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(feederDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
