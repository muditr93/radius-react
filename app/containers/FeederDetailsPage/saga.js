import { takeLatest, call, put, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import request from 'superagent';
import { FETCH_MONTHLY_FEEDER, FETCH_FEEDER_DATA, FETCH_FEEDER_DETAILS, FETCH_FEEDER, GET_MONTHLY_SLA,  GET_INSTALLATION_DETAILS, GET_REMARKS  } from './constants';// import { take, call, put, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
export function* getUsers(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
    token_id: '24b2fc50-f3da-11e8-88ac-02d6f4b17064',
    access_area: 'UPPCL',
    access_area_id: '1',
    project_id: 'EODB',
    discom_id: state.getIn(['feederHeaderForm', 'discomV']),
    zone_id: state.getIn(['feederHeaderForm', 'zoneV']),
    circle_id: state.getIn(['feederHeaderForm', 'circleV']),
    division_id: state.getIn(['feederHeaderForm', 'divisionV']),
    "substation_id": state.getIn(['feederHeaderForm', 'subsV']),
    }

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getFeederData')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something went wrong with API');
        }
        return response.body;
      });
    yield put(push(`feeder/${data.resources[0].id}`));
    yield put({ type: `${FETCH_FEEDER}_SUCCESS`, data });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
      console.error(error.response.body.reason);
    }
    // yield put(userLoadFailuer());
  }
}

export function* getFeederDataSaga(action) {
  const payload = {
    "login_id": "webui",
    "token_id": "24b2fc50-f3da-11e8-88ac-02d6f4b17064",
    "sensor_id": action.id
  }
  if(action.date){
    payload.fromTime=action.date
    payload.toTime=action.date
  }
  console.log('triggereeed');
  const data = yield request
    .post('https://feeder.myxenius.com/webapi/v1/getFeederLogData')
    .send(payload)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong with API');
      }
      return response.body;
    });
    let obj = {
      rvoltage:[],
      yvoltage: [],
      bvoltage:[],
      load:[],
      time:[],
    }
    if (data.resources) {
      data.resources.map(x => {
        obj.yvoltage.push(x.yvoltage)
        obj.rvoltage.push(x.rvoltage)
        obj.bvoltage.push(x.bvoltage)
        obj.load.push(x.instantcumKVA)
        obj.time.push(x.creationTime)
      })
      // debugger;
      yield put({ type: `${FETCH_FEEDER_DATA}_SUCCESS`, data: obj });
    } else {
      yield put({ type: `${FETCH_FEEDER_DATA}_FAILURE`, data: data.message });
      alert(data.message)
    }

}

export function* getFeederDetailsSaga(action) {
  const payload = {
    "token_id": "1b05cf90-f3a1-11e8-8b36-0a0027000011",
    "feeder_id": action.id,
    "project_id": "ALL"
  }
  const data = yield request
    .post('https://feeder.myxenius.com/webapi/v1/getFeederDetails')
    .send(payload)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong with API');
      }
      return response.body;
    });
    if (data.message=="Success") {
      yield put({ type: `${FETCH_FEEDER_DETAILS}_SUCCESS`, data });
    } else {
      alert(data.message)
    }
}

export function* getRemarksSaga(action) {
  const payload = {
    "login_id": "webui",
    "token_id": "24b2fc50-f3da-11e8-88ac-02d6f4b17064",
    "feeder_id": action.value,
    "log_type": "EXTERNAL"
}

  const data = yield request
    .post('https://feeder.myxenius.com/webapi/v1/getViewRemarks')
    .send(payload)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong with API');
      }
      return response.body;
    });
    if (data.message=="Success") {
      yield put({ type: `${GET_REMARKS}_SUCCESS`, data });
    } else {
      // alert(data.message)
    }
}

export function* getInstallationDetailsSaga(action) {
  const payload = {
   "login_id": "webui",
   "token_id": "24b2fc50-f3da-11e8-88ac-02d6f4b17064",
   "feeder_id": action.value
 }
  const data = yield request
    .post('https://feeder.myxenius.com/webapi/v1/getFeederInstallationDetails')
    .send(payload)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong with API');
      }
      return response.body;
    });
    if (data.message=="Success") {
      let img_arr = [];
      if (data.resources.length) {
        data.resources.map(item => {
          item.categoryList.map( subItem => {
            img_arr.push(subItem)
          })
        })

      }
      yield put({ type: `${GET_INSTALLATION_DETAILS}_SUCCESS`, data: img_arr });
    } else {
      alert(data.message)
    }
}

export function* getMonthlySLASaga(action) {
  const payload = {
    "login_id": "webui",
    "token_id": "24b2fc50-f3da-11e8-88ac-02d6f4b17064",
    "feeder_id": action.id,
  }
  if(action.date){
    let dt = action.date.split('-');
    payload.month = `${dt[0]}-${dt[1]}`
  } else {
    var d = new Date();
    var n = d.getMonth() + 1;
    if (n < 10) {
      n = `0${n}`
    }
    payload.month = `2019-${n}`
  }
  const data = yield request
    .post('https://feeder.myxenius.com/webapi/v1/getFeederSLA')
    .send(payload)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong with API');
      }
      return response.body;
    });
    if (data.resources) {
      let obj = {
        requiredPacket:[],
        dataPacketCount: [],
        duration:[],
        date:[],
      }
      data.resources.map(x => {
        obj.duration.push(+x.duration)
        obj.date.push(x.meterDate)
        obj.dataPacketCount.push(+x.dataPacketCount)
        obj.requiredPacket.push(+x.requiredPacket)
      })
  yield put({ type: `${GET_MONTHLY_SLA}_SUCCESS`, data: obj });
  } else {
    // alert(data.message)
  }
}

export function* getMonthlySaga(action) {
  const payload = {
    "login_id": "webui",
    "token_id": "24b2fc50-f3da-11e8-88ac-02d6f4b17064",
    "feeder_id":  action.id,
    "project_id": "ALL",
    "access_area":"UPPCL",
    "access_area_id":"1",
    "year_month":"2018-12"
  }
  if(action.date){
    let dt = action.date.split('-');
    payload.year_month = `${dt[0]}-${dt[1]}`
  } else {
    var d = new Date();
    var n = d.getMonth() + 1;
    if (n < 10) {
      n = `0${n}`
    }
    payload.year_month = `2019-${n}`
  }
  const data = yield request
    .post('https://feeder.myxenius.com/webapi/v1/getFeederMonthlyEnergySupplied')
    .send(payload)
    .then((response) => {
      if (!response.ok) {
        throw new Error('Something went wrong with API');
      }
      return response.body;
    });
    if (data.message=="Success") {
      yield put({ type: `${FETCH_MONTHLY_FEEDER}_SUCCESS`, data });
    } else {
      alert(data.message)
    }
}
export default function* defaultSaga() {
  // yield takeLatest(GET_INTERRUPTION_DETAIL, getRemarksSaga);
  yield takeLatest(GET_REMARKS, getRemarksSaga);
  yield takeLatest(GET_INSTALLATION_DETAILS, getInstallationDetailsSaga);
  yield takeLatest(GET_MONTHLY_SLA, getMonthlySLASaga);
  yield takeLatest(FETCH_FEEDER, getUsers);
  yield takeLatest(FETCH_MONTHLY_FEEDER, getMonthlySaga);
  yield takeLatest(FETCH_FEEDER_DATA, getFeederDataSaga);
  yield takeLatest(FETCH_FEEDER_DETAILS, getFeederDetailsSaga);
}
