import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the feederDetailsPage state domain
 */

const selectFeederDetailsPageDomain = state =>
  state.get("feederDetailsPage", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by FeederDetailsPage
 */

const makeSelectFeederDetailsPage = () =>
  createSelector(selectFeederDetailsPageDomain, substate => substate.toJS());

export default makeSelectFeederDetailsPage;
export { selectFeederDetailsPageDomain };
