/**
 *
 * NotificationSidebar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Drawer from 'material-ui/Drawer';
import { List, ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import RzButton from 'components/RzButton';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectNotificationSidebar from './selectors';
import reducer from './reducer';
import saga from './saga';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  toggleNotifications,
  getNotifications,
} from './actions';

const HeaderTile = styled.div`
  padding: 10px 0 5px 10px;
  box-sizing: border-box;
  font-size: 13px;
  font-weight: bold;
  color: #cf3446;
  text-transform: uppercase;
  .compareIcon {
    box-sizing: border-box;
    float: right;
    padding-right: 5px;
    cursor: pointer;
    &:hover {
      // box-shadow: 1px 2px 1px 0px rgba(186,179,186,1)
    }
  }
`;
const style = {
  buttonContainer: {
    height: '80px',
    width: '100px',
    position: 'relative',
  }
}
const styles = theme => ({
  root: {
    width: '100%',
    fontSize: '13px',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

export class NotificationSidebar extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    expanded: 'ALL',
    size: 10,
  }
  scrollToBottom = () => {
  this.messagesEnd.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }
  componentDidMount() {
  this.scrollToBottom();
  this.props.dispatch(getNotifications('ALL'))
  }
  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
      size:10,
    });
    this.props.dispatch(getNotifications(panel))
  };
  handleLoadMore = () => {
    const { size } = this.state;
    this.setState({
      size: size+10,
    })
  }
  listData = () => {
    const { noti } = this.props.notificationsidebar;
    const { size } = this.state;
    return (
      <div>
        {noti && noti.slice(0, size).map((item) => (
          <div style={{ backgroundColor: '#FFFFFF' }}>
            <ListItem
              primaryText={item.name}
              secondaryText={item.value}
              secondaryTextLines={1}
              style={{ fontSize: '14px' }}
            />
            <Divider />
          </div>
        ))}
        {noti && noti.length ? (
          <div style={style.buttonContainer}>
            <RzButton bgColor="#00A99D" color="white" onClick={this.handleLoadMore}> Load More</RzButton>
          </div>
        ):(null) }
      </div>
    );
  }
  render() {
    const {
      notificationsidebar,
    } = this.props;
    const { expanded } = this.state;
    const { classes } = this.props;
    const { noti } = this.props.notificationsidebar;

    return (
      <div>
        <Drawer
          open={notificationsidebar.showNotificationSidebar}
          open
          docked
          openSecondary
          width={300}
          style={{ color: 'white', zIndex: 0}}
          containerStyle={{ paddingTop: '70px', backgroundColor: '#FFFFFF', color: 'black', zIndex: 0 }}
          onRequestChange={() => this.props.dispatch(toggleNotifications())}
        >
        <div className={classes.root}>
          <HeaderTile>
            Notification/Exception
          </HeaderTile>
          <ExpansionPanel expanded={expanded === 'ALL'} onChange={this.handleChange('ALL')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>All</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <List>
                <div ref={(ref) => { this.messagesEnd = ref}}>
                  {this.listData()}
                </div>
              </List>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel expanded={expanded === 'DownFeeder'} onChange={this.handleChange('DownFeeder')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Down Feeders</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <List>
                <div ref={(ref) => { this.messagesEnd = ref}}>
                  {this.listData()}
                </div>
              </List>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel expanded={expanded === 'LeanLoadFeeder'} onChange={this.handleChange('LeanLoadFeeder')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Lean Load Feeders</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <List>
                <div ref={(ref) => { this.messagesEnd = ref}}>
                  {this.listData()}
                </div>
              </List>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel expanded={expanded === 'LowPerformingFeeder'} onChange={this.handleChange('LowPerformingFeeder')}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>Low Performing Feeders</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <List>
                <div ref={(ref) => { this.messagesEnd = ref}}>
                  {this.listData()}
                </div>
              </List>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>
        </Drawer>
      </div>
    );
  }
}

NotificationSidebar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  notificationsidebar: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  notificationsidebar: makeSelectNotificationSidebar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'notificationSidebar', reducer });
const withSaga = injectSaga({ key: 'notificationSidebar', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
 withStyles(styles),
)(NotificationSidebar);
