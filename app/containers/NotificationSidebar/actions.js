/*
 *
 * NotificationSidebar actions
 *
 */

import {
  DEFAULT_ACTION,
  TOGGLE_NOTIFICATIONS,
  GET_NOTIFICATIONS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getNotifications(value) {
  return {
    type: GET_NOTIFICATIONS,
    value,
  };
}
export function toggleNotifications() {
  return {
    type: TOGGLE_NOTIFICATIONS,
  };
}
