/**
 *
 * Asynchronously loads the component for NotificationSidebar
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
