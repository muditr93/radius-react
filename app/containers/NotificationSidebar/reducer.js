/*
 *
 * NotificationSidebar reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  TOGGLE_NOTIFICATIONS,
  GET_NOTIFICATIONS,
} from './constants';

const initialState = fromJS({
  showNotificationSidebar: false,
  noti: []
});

function notificationSidebarReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case `${GET_NOTIFICATIONS}_SUCCESS`:
      return state.set('noti', action.data);
    case TOGGLE_NOTIFICATIONS:
      return state.set('showNotificationSidebar', !state.get('showNotificationSidebar'));
    default:
      return state;
  }
}

export default notificationSidebarReducer;
