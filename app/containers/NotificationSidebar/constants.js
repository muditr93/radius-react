/*
 *
 * NotificationSidebar constants
 *
 */

export const DEFAULT_ACTION = 'app/NotificationSidebar/DEFAULT_ACTION';
export const TOGGLE_NOTIFICATIONS = 'app/NotificationSidebar/TOGGLE_NOTIFICATIONS';
export const GET_NOTIFICATIONS = 'app/NotificationSidebar/GET_NOTIFICATIONS';
