/*
 * NotificationSidebar Messages
 *
 * This contains all the text for the NotificationSidebar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.NotificationSidebar.header',
    defaultMessage: 'This is NotificationSidebar container !',
  },
});
