
import { fromJS } from 'immutable';
import notificationSidebarReducer from '../reducer';

describe('notificationSidebarReducer', () => {
  it('returns the initial state', () => {
    expect(notificationSidebarReducer(undefined, {})).toEqual(fromJS({}));
  });
});
