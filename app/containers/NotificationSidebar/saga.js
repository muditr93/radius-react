import request from 'superagent';
import {
  takeLatest, put, call, select, all, throttle,
} from 'redux-saga/effects';
import { fromJS } from 'immutable';
import dayjs from 'dayjs';
import makeSelectBasePage from './selectors';

import {
  GET_NOTIFICATIONS,
} from './constants';

// Individual exports for testing
export function* getNotificationSaga(action) {
  try {
    const state = yield select();
    const project = state.getIn(['basePage', 'area']);
    const payload = {
    "login_id": "webui",
    "token_id": "24b2fc50-f3da-11e8-88ac-02d6f4b17064",
    "access_area": "All",
    "project_id": "EODB",
    "notificationType": action.value
    }

    const data = yield request
      .post('https://feeder.myxenius.com/webapi/v1/getNotificationDetails')
      .send(payload)
      .then((response) => {
        if (!response.ok) {
          throw new Error('Something goes wrong with API');
        }
        return response.body;
      });
    yield put({ type: `${GET_NOTIFICATIONS}_SUCCESS`, data: data.resources });
  } catch (error) {
    if (error.status === 400) {
      window.alert(error.response.body.reason);
    }

    yield put({ type: `${GET_NOTIFICATIONS}_FAILURE`, error });
  }
}

export default function* defaultSaga() {
  yield takeLatest(GET_NOTIFICATIONS, getNotificationSaga);
}
