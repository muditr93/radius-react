import { createSelector } from 'reselect';

/**
 * Direct selector to the notificationSidebar state domain
 */
const selectNotificationSidebarDomain = (state) => state.get('notificationSidebar');

/**
 * Other specific selectors
 */


/**
 * Default selector used by NotificationSidebar
 */

const makeSelectNotificationSidebar = () => createSelector(
  selectNotificationSidebarDomain,
  (substate) => substate.toJS()
);

export default makeSelectNotificationSidebar;
export {
  selectNotificationSidebarDomain,
};
