/*
 *
 * GetGatewayDetails constants
 *
 */

export const DEFAULT_ACTION = "app/GetGatewayDetails/DEFAULT_ACTION";
export const OPEN_EDITOR = 'app/GetGatewayDetails/OPEN_EDITOR';
export const CLOSE_EDITOR = 'app/GetGatewayDetails/CLOSE_EDITOR';
export const FETCH_NEW_USERS = 'app/GetGatewayDetails/FETCH_NEW_USERS';
export const SELECT_USER = 'app/GetGatewayDetails/SELECT_USER';
export const EDIT_USER = 'app/GetGatewayDetails/EDIT_USER';
export const NEW_USERS_LOADED = 'app/GetGatewayDetails/NEW_USERS_LOADED';
export const USER_LOAD_FAILURE = 'app/GetGatewayDetails/USER_LOAD_FAILURE';
export const USER_LIST_UPDATED = 'app/GetGatewayDetails/USER_LIST_UPDATED';
export const PREFILL_USER = 'app/GetGatewayDetails/PREFILL_USER';
export const ALTER_OFFSET = 'app/GetGatewayDetails/ALTER_OFFSET';
export const columns = [
  {
    sort: 0,
    type: 'zoneName',
    name: 'ZONE',
    size: 1,
  },
  {
    sort: 1,
    type: 'circleName',
    name: 'CIRCLE',
    size: 1,
  },
  {
    sort: 2,
    type: 'divisionName',
    name: 'Division',
    size: 1,
  },
  {
    sort: 3,
    type: 'discomName',
    name: 'Discom Name',
    size: 1,
  },
  {
    sort: 4,
    type: 'ssName',
    name: 'SS Name ',
    size: 1,
  },
  {
    sort: 5,
    type: 'feederName',
    name: 'Feeder Name',
    size: 1,
  },
  {
    sort: 6,
    type: 'projectName',
    name: 'Project',
    size: 1,
  },
  {
    sort: 7,
    type: 'meterType',
    name: 'Meter type',
    size: 1,
  },
  {
    sort: 8,
    type: 'meterCTMF',
    name: 'meter ctmf',
    size: 1,
  },
  {
    sort: 9,
    type: 'currentLoad',
    name: 'Current Load',
    size: 1,
  },
  {
    sort: 10,
    type: 'currentMD',
    name: 'Current MD ',
    size: 1,
  },
  {
    sort: 11,
    type: 'lastReadingUpdated',
    name: 'Last Incremental',
    size: 1,
  },
  {
    sort: 12,
    type: 'installationTime',
    name: 'Installation Date ',
    size: 1,
  },
  {
    sort: 13,
    type: 'lastPacketTime',
    name: 'Last communication ',
    size: 1,
  },
  {
    sort: 14,
    type: 'status',
    name: 'status',
    size: 1,
  },
  {
    sort: 15,
    type: 'imeiNo',
    name: 'Gateway No  ',
    size: 1,
  },
];
