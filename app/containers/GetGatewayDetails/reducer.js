/*
 *
 * GetGatewayDetails reducer
 *
 */

import { fromJS } from "immutable";
import {
 OPEN_EDITOR,
 CLOSE_EDITOR,
 NEW_USERS_LOADED,
 USER_LIST_UPDATED,
 PREFILL_USER,
 ALTER_OFFSET,
} from './constants';

const initialState = fromJS({
  showEditor: false,
  substations: [],
  totalCount: 0,
  offset: 0,
  loaded: false,
});
function getGatewayDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case ALTER_OFFSET:
      return state.set('offset', action.value);
    case PREFILL_USER:
      return state.set('userDetails', action.value);
    case OPEN_EDITOR:
      return state.set('showEditor', true);
    case CLOSE_EDITOR:
      return state.set('showEditor', false);
    case NEW_USERS_LOADED:
      return state
                .set('loaded', true)
                .set('substations', action.value)
                .set('totalCount', action.value);
    case USER_LIST_UPDATED:
      const arr = state.get('userList');
      const i = arr.findIndex((item) => item.id === action.value.id);
      i >= 0 ? (arr[i] = action.value) : (arr.push(action.value));
      return state
            .set('userList', arr);
    default:
      return state;
  }
}

export default getGatewayDetailsReducer;
