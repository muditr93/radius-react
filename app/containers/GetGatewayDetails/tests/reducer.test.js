import { fromJS } from 'immutable';
import getGatewayDetailsReducer from '../reducer';

describe('getGatewayDetailsReducer', () => {
  it('returns the initial state', () => {
    expect(getGatewayDetailsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
