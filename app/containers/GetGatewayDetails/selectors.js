import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the getGatewayDetails state domain
 */

const selectGetGatewayDetailsDomain = state =>
  state.get("getGatewayDetails", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by GetGatewayDetails
 */

const makeSelectGetGatewayDetails = () =>
  createSelector(selectGetGatewayDetailsDomain, substate => substate.toJS());

export default makeSelectGetGatewayDetails;
export { selectGetGatewayDetailsDomain };
