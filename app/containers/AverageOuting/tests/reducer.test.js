import { fromJS } from 'immutable';
import averageOutingReducer from '../reducer';

describe('averageOutingReducer', () => {
  it('returns the initial state', () => {
    expect(averageOutingReducer(undefined, {})).toEqual(fromJS({}));
  });
});
