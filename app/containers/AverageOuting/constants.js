/*
 *
 * AverageOuting constants
 *
 */

 export const DEFAULT_ACTION = "app/AverageOuting/DEFAULT_ACTION";
 export const OPEN_EDITOR = 'app/AverageOuting/OPEN_EDITOR';
 export const CLOSE_EDITOR = 'app/AverageOuting/CLOSE_EDITOR';
 export const FETCH_NEW_USERS = 'app/AverageOuting/FETCH_NEW_USERS';
 export const SELECT_USER = 'app/AverageOuting/SELECT_USER';
 export const EDIT_USER = 'app/AverageOuting/EDIT_USER';
 export const NEW_USERS_LOADED = 'app/AverageOuting/NEW_USERS_LOADED';
 export const USER_LOAD_FAILURE = 'app/AverageOuting/USER_LOAD_FAILURE';
 export const USER_LIST_UPDATED = 'app/AverageOuting/USER_LIST_UPDATED';
 export const PREFILL_USER = 'app/AverageOuting/PREFILL_USER';
 export const ALTER_OFFSET = 'app/AverageOuting/ALTER_OFFSET';
 export const columns = [
   {
     sort: 0,
     type: 'discomName',
     name: 'Discom Name',
     size: 1,
   },
   {
     sort: 1,
     type: 'zoneName',
     name: 'ZONE',
     size: 1,
   },
   {
     sort: 2,
     type: 'circleName',
     name: 'CIRCLE',
     size: 1,
   },
   {
     sort: 3,
     type: 'divisionName',
     name: 'Division',
     size: 1,
   },
   {
     sort: 4,
     type: 'ssName',
     name: 'SS Name ',
     size: 1,
   },
   {
     sort: 5,
     type: 'feederName',
     name: 'Feeder Name',
     size: 1,
   },
   {
     sort: 6,
     type: 'projectName',
     name: 'Project',
     size: 1,
   },
   {
     sort: 7,
     type: 'serialNo',
     name: 'SerialNo',
     size: 1,
   },
   {
     sort: 15,
     type: 'mcUID',
     name: 'Gateway No  ',
     size: 1,
   },
   {
     sort: 8,
     type: 'fromDate',
     name: 'From',
     size: 1,
   },
   {
     sort: 9,
     type: 'toDate',
     name: 'To',
     size: 1,
   },
   {
     sort: 10,
     type: 'numOfDays',
     name: 'No of Days',
     size: 1,
   },
   {
     sort: 11,
     type: 'outageCount',
     name: 'Outage Count (A)',
     size: 1,
   },
   {
     sort: 12,
     type: 'outageDuration',
     name: 'Outage Duration (B)',
     size: 1,
   },
   {
     sort: 13,
     type: 'roundAvgOutageCount',
     name: 'Rounded Average Outage Count(C=A/No of Days) ',
     size: 1,
   },
   {
     sort: 13,
     type: 'avgOutageCount',
     name: 'Acutal Avg Outage Count(D=A/No of Days) ',
     size: 1,
   },
   {
     sort: 14,
     type: 'avgOutageDuration',
     name: 'Average Outage Duration (E=B/A)',
     size: 1,
   },
   {
     sort: 14,
     type: 'avgOutageDurationPerDay',
     name: 'Average Outage Duration Per Day(F=B/No of Days)',
     size: 1,
   },
 ];
