/**
 *
 * Asynchronously loads the component for AverageOuting
 *
 */

import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
