/*
 * UserListPage Messages
 *
 * This contains all the text for the UserListPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.UserListPage.header',
    defaultMessage: 'FEEDERS',
  },
  creation: {
    id: 'app.containers.UserListPage.creation',
    defaultMessage: 'Requester Creation',
  },
});
