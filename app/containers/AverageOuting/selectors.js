import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the averageOuting state domain
 */

const selectAverageOutingDomain = state =>
  state.get("averageOuting", initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AverageOuting
 */

const makeSelectAverageOuting = () =>
  createSelector(selectAverageOutingDomain, substate => substate.toJS());

export default makeSelectAverageOuting;
export { selectAverageOutingDomain };
