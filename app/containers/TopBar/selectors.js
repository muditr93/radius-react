import { createSelector } from 'reselect';

/**
 * Direct selector to the topbar state domain
 */
const selectTopbarDomain = (state) => state.get('topbar');
const selectPageDomain = (state) => state.get('page');

/**
 * Other specific selectors
 */
const makeSelectPage = () => createSelector(
  selectTopbarDomain,
  (substate) => substate.toJS()
);

/**
 * Default selector used by Topbar
 */

const makeSelectTopbar = () => createSelector(
  selectPageDomain,
  (substate) => substate.toJS()
);

export default makeSelectTopbar;
export {
  selectTopbarDomain,
  makeSelectTopbar,
  selectPageDomain,
  makeSelectPage,
};
