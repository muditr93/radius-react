/*
 *
 * Topbar actions
 *
 */

import {
  DEFAULT_ACTION,
  CLOSE_SNACK_BAR,
  OPEN_SNACK_BAR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function closeSnackBar() {
  return {
    type: CLOSE_SNACK_BAR,
  };
}

export function openSnackBar(value) {
  return {
    type: OPEN_SNACK_BAR,
    value,
  };
}
