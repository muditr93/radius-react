/*
 * Topbar Messages
 *
 * This contains all the text for the Topbar component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Topbar.header',
    defaultMessage: 'Xenius',
  },
});
