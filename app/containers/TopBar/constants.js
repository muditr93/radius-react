/*
 *
 * Topbar constants
 *
 */

export const TOGGLE_SIDEBAR = 'app/Topbar/SidebarToggled';
export const OPEN_SNACK_BAR = 'app/Topbar/OPEN_SNACK_BAR';
export const CLOSE_SNACK_BAR = 'app/Topbar/CLOSE_SNACK_BAR';
export const DEFAULT_ACTION = 'app/Topbar/DEFAULT_ACTION';
