/*
 *
 * Topbar reducer
 *
 */

import { fromJS } from 'immutable';
import {
  TOGGLE_SIDEBAR,
  OPEN_SNACK_BAR,
  CLOSE_SNACK_BAR,
} from './constants';

const initialState = fromJS({
  loading: false,
  snackBarState: false,
  snackBarText: '',
});

function topbarReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_SIDEBAR:
      return state;
    case OPEN_SNACK_BAR:
      return state
              .set('snackBarText', action.value)
              .set('snackBarState', true);
    case CLOSE_SNACK_BAR:
      return state.set('snackBarState', false);
    default:
      return state;
  }
}

export default topbarReducer;
