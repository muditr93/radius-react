/**
 *
 * Topbar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { push } from 'react-router-redux';
import styled from 'styled-components';

import Avatar from 'material-ui/Avatar';
import Toggle from 'material-ui/Toggle';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import RaisedButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import Person from 'material-ui/svg-icons/social/person';
import Notifications from 'material-ui/svg-icons/social/notifications';
import Badge from 'material-ui/Badge';

import Clock from 'react-digital-clock';
import RzAppBar from 'components/RzAppBar';
import { changeTheme } from 'containers/Page/actions';
import { toggleNotifications } from 'containers/NotificationSidebar/actions';
import { toggle } from 'containers/SideBar/actions';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { closeSnackBar } from './actions';
import { makeSelectTopbar, makeSelectPage } from './selectors';
import Snackbar from 'material-ui/Snackbar';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

const RightMenu = styled.div`
  display: flex;
  margin-top: -8px;
`;

const MenuButton = styled.div`
  display: flex;
  width: 54px;
  height: 54px;
  justify-content: center;
  align-items: center;
  ${'' /* background: rgba(213, 84, 0, 1); */}

  & > button {
    border: none;
    outline: none;

    img {
      max-width: 100%;
    }
  }
`;

const Profile = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .basic-info, .contact-info {
    width: 100%;
    padding:10px;
    margin: 20px 0;
    text-align: center;
  }

  .contact-info {
    text-align: center;
  }

  img {
    max-width: 100px;
  }

  button {
    background:#eee;
    padding: 15px 30px;

    &:hover {
      border-color: black;
    }
  }
`;
export class Topbar extends React.Component { // eslint-disable-line react/prefer-stateless-function

  logoutHandler() {
    alert('Feature unavailable')
  }

  handleChangeTheme = () => {
    this.props.dispatch(changeTheme('strandd'));
    // this.forceUpdate();
  }

  notificationButtonHandler() {
    this.props.dispatch(toggleNotifications());
  }

  getDate = () => {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = dd + '/' + mm + '/'  + yyyy;
    return today;
  }

  render() {
    const {
      topbar: {
        loading,
        snackBarState,
        snackBarText
      },
      page,
      dispatch
    } = this.props;

    const rightMenu = (
      <RightMenu>
        <div style={{background: '#354870', color: 'white', padding: '15px'}}>
        {this.getDate()}
          </div>
        <div style={{background: '#354870', padding: '15px'}}>
          <Clock />
        </div>
        <MenuButton onClick={() => this.notificationButtonHandler()}>
          <Badge
            badgeContent={4}
            primary
            style={{ padding: '12px' }}
            badgeStyle={{ backgroundColor: '#00A99D', cursor: 'pointer' }}
          >
            <img src="https://image.ibb.co/mycg4q/Notifications.png" alt="Notifications" height={24} border="0" />
          </Badge>
        </MenuButton>
        <MenuButton>
          <IconMenu
            iconButtonElement={
              <IconButton><img src="https://image.ibb.co/dEtccA/Login.png" alt="Login" height={24} border="0" /></IconButton>
            }
            targetOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
          >
          <Profile key="b">
            <div>
              <Avatar icon={<Person />} />
            </div>
            <div className="basic-info">
              <strong>Webui</strong>
              <div>CM</div>
              <div>
              </div>
            </div>
            <div className="contact-info"></div>
            <div>
              <RaisedButton
                label="Logout"
                backgroundColor="#a2a2a2"
                style={{ color: 'white' }}
                onClick={() => this.logoutHandler()}
              />
            </div>
          </Profile>
          </IconMenu>
        </MenuButton>
      </RightMenu>
    );
    const centerMenu = (
      <div style={{height: '54px'}}>
        <img src="https://thumb.ibb.co/eVWTk0/upcl.png" alt="upcl" border="0" style={{height: '30px', paddingRight: '10px'}} />
        <img src="https://image.ibb.co/kKRm50/Xenius.png" alt="Xenius" border="0" style={{ height: '30px'}} />
      </div>
    )

    return (
      <div style={{position: 'sticky', top: 0, zIndex: 1301,}}>
        <RzAppBar
          title={centerMenu}
          iconElementRight={rightMenu}
          titleStyle={{
            position: 'sticky',
            height: '54px',
          }}
          onLeftIconButtonClick={() => this.props.dispatch(toggle())}
          style={{
            position: 'sticky',
            zIndex: 1301,
            minHeight: 54,
          }}
        />
        <Snackbar
          open={snackBarState}
          message={snackBarText}
          autoHideDuration={2500}
          onRequestClose={() => dispatch(closeSnackBar())}
        />
      </div>
    );
  }
}

Topbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  topbar: makeSelectTopbar(),
  page: makeSelectPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'topbar', reducer });
const withSaga = injectSaga({ key: 'topbar', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Topbar);
