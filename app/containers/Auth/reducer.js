/*
 *
 * Auth reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOGOUT,
  PERSIST_TOKEN,
  SYNC_STORAGE,
  SYNC_STORAGE_SUCCESS,
  SYNC_STORAGE_FAILURE,
} from './constants';

const initialState = fromJS({
  isAuthenticated: false,
  user: {},
  loaded: false,
});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case PERSIST_TOKEN:
      return state;
    case LOGOUT:
      return state
        .set('isAuthenticated', false);
    case SYNC_STORAGE_SUCCESS:
      return state
      .set('user', action.data)
      .set('isAuthenticated', true)
      .set('loaded', true);
    case SYNC_STORAGE_FAILURE:
        return state
          .set('isAuthenticated', false)
          .set('loaded', true);
    default:
      return state;
  }
}

export default authReducer;
