/*
 *
 * Auth constants
 *
 */

export const LOGOUT = 'app/Auth/LOGOUT';
export const SYNC_STORAGE = 'app/Auth/SYNC_STORAGE';
export const SYNC_STORAGE_SUCCESS = 'app/Auth/SYNC_STORAGE_SUCCESS';
export const SYNC_STORAGE_FAILURE = 'app/Auth/SYNC_STORAGE_FAILURE';
export const PERSIST_TOKEN = 'app/Auth/PERSIST_TOKEN';
export const START_REFRESH_TIMER = 'app/Auth/START_REFRESH_TIMER';
export const REFRESH_TOKEN = 'app/Auth/REFRESH_TOKEN';
export const LOGIN_SUCCESS = 'app/Auth/LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'app/Auth/LOGIN_FAILURE';
