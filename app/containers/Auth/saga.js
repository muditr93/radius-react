import localForage from 'localforage';
import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import request from 'superagent';
// import { BaseURL } from 'utils/helper';
import {
  LOGOUT,
  SYNC_STORAGE,
  START_REFRESH_TIMER,
  REFRESH_TOKEN,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
} from './constants';
import {
  syncStorageSuccess,
  syncStorageFailed,
  logout,
  authSuccess,
} from './actions';

// wait :: Number -> Promise
const wait = (ms) => (
  new Promise((resolve) => {
    setTimeout(() => resolve(), ms);
  })
);

function* clearAuthCache(key) {
  yield localForage.removeItem(key);
}

function* setAuthCache(key, value) {
  console.log('done');
  yield localForage.setItem(key, value);
}

export function* loginFlow(product, response) {
  yield all(Object.keys(response).map((key) => (
      console.log(`${product}@${key}`),
      call(setAuthCache, `${product}@${key}`, response[key])
  )
));
}

export function* syncStorageAuthSaga(action) {
      console.log('fine');
    const logged = yield localForage.getItem('loggedIn');
    if (logged) {
      const user = yield localForage.getItem('user');
      yield put(syncStorageSuccess(user));
    } else {
      yield put(syncStorageFailed());
      yield put(push('/login'));
  }
}

export function* logoutFlow(action) {
  // const keys = yield localForage.keys();
  // const matchedKeys = keys.filter((key) => key.startsWith(`${action.value}@`));
  // if (matchedKeys.length) {
  //   yield all(matchedKeys.map((key) => call(clearAuthCache, key)));
  // }
  yield put(push('/login'));
}

export function* loginSuccessCall(action) {
  yield localForage.setItem('loggedIn', true);
  yield localForage.setItem('user', action.value);
  yield put(syncStorageSuccess(action.value))
  yield put(push('/'));
}

export function* loginFailureCall(action) {
  yield localForage.setItem('user', false);
  yield put(syncStorageFailed());
  yield put(push('/login'));
}

export default function* authSaga() {
  yield takeLatest(LOGIN_FAILURE, loginFailureCall);
  yield takeLatest(LOGIN_SUCCESS, loginSuccessCall);
  yield takeLatest(LOGOUT, logoutFlow);
  yield takeLatest(SYNC_STORAGE, syncStorageAuthSaga);
}
