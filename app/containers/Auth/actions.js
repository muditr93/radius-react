/*
 *
 * Auth actions
 *
 */

import {
  LOGOUT,
  PERSIST_TOKEN,
  SYNC_STORAGE,
  SYNC_STORAGE_SUCCESS,
  SYNC_STORAGE_FAILURE,
  START_REFRESH_TIMER,
  REFRESH_TOKEN,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from './constants';

export function loginSuccess(value) {
  return {
    type: LOGIN_SUCCESS,
    value,
  };
}

export function loginFailure() {
  return {
    type: LOGIN_FAILURE,
  };
}

export function logout(value) {
  return {
    type: LOGOUT,
    value,
  };
}
export function startRefreshTimer() {
  return {
    type: START_REFRESH_TIMER,
  };
}

export function refreshToken() {
  return {
    type: REFRESH_TOKEN,
  };
}
export function syncStorage(value) {
  return {
    type: SYNC_STORAGE,
    value,
  };
}

export function syncStorageSuccess(data) {
  return {
    type: SYNC_STORAGE_SUCCESS,
    data,
  };
}

export function syncStorageFailed(error) {
  return {
    type: SYNC_STORAGE_FAILURE,
    error,
  };
}

export function persistToken(data) {
  return {
    type: PERSIST_TOKEN,
    data,
  };
}
