/*
 *
 * LoginPage actions
 *
 */

import { DEFAULT_ACTION, LOGIN_CALL } from "./constants";

export function defaultAction() {
  return {
    type: DEFAULT_ACTION
  };
}

export function makeLoginCall(value) {
  return {
    type: LOGIN_CALL,
    value
  };
}
