import request from 'superagent';
import { takeLatest, take, call, put, select } from 'redux-saga/effects';
import {LOGIN_CALL} from './constants'
import {loginSuccess, loginFailure} from 'containers/Auth/actions'
// Individual exports for testing

export function* loginCall(action){
  try {
    const data = yield request
                    .post('https://feeder.myxenius.com/webapi/v1/login')
                    .send(action.value)
                    .then(response => {
                        if(!response.ok){
                            throw new Error('Something goes wrong with API');
                        }
                        return response.body;
                    });
      yield put(loginSuccess(data))
  } catch (error) {
    if(error){
        window.alert(error.response.body.reason);
        console.error(error.response.body.reason);
    }
    yield put(loginFailure())
  }

}

export default function* defaultSaga() {
  yield takeLatest(LOGIN_CALL, loginCall);
}
