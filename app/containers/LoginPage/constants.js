/*
 *
 * LoginPage constants
 *
 */

 export const DEFAULT_ACTION = "app/LoginPage/DEFAULT_ACTION";
 export const LOGIN_CALL = "app/LoginPage/LOGIN_CALL";
