/**
 *
 * RzCustomDate
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import { styledComponents } from './styledComponents';
// import dayjs from 'moment';
import dayjs from 'dayjs';
/* eslint-disable react/prefer-stateless-function */
import isLeapYear from 'dayjs/plugin/isLeapYear';

dayjs.extend(isLeapYear);

const yearItems = [];

for (let i = dayjs().year(); i > 2014; i -= 1) {
  // dayjs().year() gives latest year
  yearItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
}

const monthItems = [
  <MenuItem value="January" primaryText="January" />,
  <MenuItem value="February" primaryText="February" />,
  <MenuItem value="March" primaryText="March" />,
  <MenuItem value="April" primaryText="April" />,
  <MenuItem value="May" primaryText="May" />,
  <MenuItem value="June" primaryText="June" />,
  <MenuItem value="July" primaryText="July" />,
  <MenuItem value="August" primaryText="August" />,
  <MenuItem value="September" primaryText="September" />,
  <MenuItem value="October" primaryText="October" />,
  <MenuItem value="November" primaryText="November" />,
  <MenuItem value="December" primaryText="December" />,
];

const daysInMonth = (month, year) => {
  var d = new Date(year, month, 0);
  return d.getDate();
};

class RzCustomDate extends React.Component {
  constructor() {
    super();
    this.state = {
      dayItems: this.getDayItems(dayjs().format('MMMM')),
      selectedMonthDonut: dayjs().format('MMMM'),
      selectedYearDonut: dayjs().year(), // type- number
      selectedYearBar: dayjs().year(), // type- number
      selectedDayCase: dayjs().date(), // type- number
      selectedMonthCase: dayjs().format('MMMM'),
      selectedYearCase: dayjs().year(), // type- number
    };
  }

  setDayItems = () => {
    const { selectedMonthCase } = this.state;

    const dayItems = [];

    let maxDate = 31;
    if (
      selectedMonthCase === 'April' ||
      selectedMonthCase === 'June' ||
      selectedMonthCase === 'September' ||
      selectedMonthCase === 'November'
    ) {
      maxDate = 30;
    }
    if (selectedMonthCase === 'February') {
      this.updateDayItems();
    }

    for (let i = 1; i <= maxDate; i += 1) {
      dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
    }
    this.setState({ dayItems });
  };
  handleChangeCalender = (event, key, value, name) => {
    const { selectedMonthDonut, selectedYearDonut } = this.state;
    // const { dispatch } = this.props;

    this.setState({ [name]: value });
    if (name === 'selectedMonthDonut') {
      // dispatch(fetchCaseStatusCount('groupBy', { type: 'donutChart', groupBy: 'state', year: selectedYearDonut, month: dayjs().month(value).format('M') }));
    } else if (name === 'selectedYearDonut') {
      // dispatch(fetchCaseStatusCount('groupBy', { type: 'donutChart', groupBy: 'state', year: value, month: dayjs().month(selectedMonthDonut).format('M') }));
    } else if (name === 'selectedYearBar') {
      // dispatch(fetchCaseStatusCount('groupBy', { type: 'barChart', groupBy: 'month', year: value }));
    }
  };

  handleChangeCaseCalender = (event, key, value, name) => {
    // const { dispatch } = this.props;
    const { selectedDayCase, selectedMonthCase, selectedYearCase } = this.state;

    this.setState({ [name]: value });
    if (name === 'selectedMonthCase') {
      this.updateDayItems(name, value);
      // const weekNumber = dayjs(
      //   `${selectedDayCase}-${value}-${selectedYearCase}`,
      //   'DD-MMM-YYYY',
      // ).week();
      // dispatch(fetchCaseStatusCount('groupBy', { type: 'weekTable', groupBy: 'day', week: weekNumber }));
    } else if (name === 'selectedYearCase') {
      this.updateDayItems(name, value);
      // const weekNumber = dayjs(
      //   `${selectedDayCase}-${selectedMonthCase}-${value}`,
      //   'DD-MMM-YYYY',
      // ).week();
      // dispatch(fetchCaseStatusCount('groupBy', { type: 'weekTable', groupBy: 'day', week: weekNumber }));
    } else {
      // const weekNumber = dayjs(
      //   `${value}-${selectedMonthCase}-${selectedYearCase}`,
      //   'DD-MMM-YYYY',
      // ).week();
      // dispatch(fetchCaseStatusCount('groupBy', { type: 'weekTable', groupBy: 'day', week: weekNumber }));
    }
  };

  getDayItems = (month, year) => {
    const dayItems = [];
    let resetSelectedDay = false;

    if (month === 'February') {
      if (dayjs([year]).isLeapYear()) {
        // it is a leap year
        for (let i = 1; i <= 29; i += 1) {
          dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }
        // if (selectedDayCase > 29) {
        //   resetSelectedDay = true;
        // }
      } else {
        for (let i = 1; i <= 28; i += 1) {
          dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }
        // if (selectedDayCase > 28) {
        //   resetSelectedDay = true;
        // }
      }
    } else if (
      month === 'April' ||
      month === 'June' ||
      month === 'September' ||
      month === 'November'
    ) {
      for (let i = 1; i <= 30; i += 1) {
        dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
      }
      // if (selectedDayCase > 30) {
      //   resetSelectedDay = true;
      // }
    } else {
      for (let i = 1; i <= 31; i += 1) {
        dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
      }

      // if (selectedDayCase > 31) {
      //   resetSelectedDay = true;
      // }
    }
    return dayItems;
  };
  updateDayItems = (name, value) => {
    const { selectedDayCase, selectedMonthCase, selectedYearCase } = this.state;
    console.log(
      'selectedDayCase, selectedMonthCase, selectedYearCase: ',
      selectedDayCase,
      selectedMonthCase,
      selectedYearCase,
    );
    const dayItems = [];
    let resetSelectedDay = false;

    if (name === 'selectedMonthCase' && value === 'February') {
      if (dayjs([selectedYearCase]).isLeapYear()) {
        // it is a leap year
        for (let i = 1; i <= 29; i += 1) {
          dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }
        if (selectedDayCase > 29) {
          resetSelectedDay = true;
        }
      } else {
        for (let i = 1; i <= 28; i += 1) {
          dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }
        if (selectedDayCase > 28) {
          resetSelectedDay = true;
        }
      }
    } else if (name === 'selectedYearCase' && selectedMonthCase === 'February') {
      if (dayjs([value]).isLeapYear()) {
        // it is a leap year
        for (let i = 1; i <= 29; i += 1) {
          dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }
        if (selectedDayCase > 29) {
          resetSelectedDay = true;
        }
      } else {
        for (let i = 1; i <= 28; i += 1) {
          dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
        }
        if (selectedDayCase > 28) {
          resetSelectedDay = true;
        }
      }
    } else if (
      selectedMonthCase === 'April' ||
      selectedMonthCase === 'June' ||
      selectedMonthCase === 'September' ||
      selectedMonthCase === 'November'
    ) {
      for (let i = 1; i <= 30; i += 1) {
        dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
      }
      if (selectedDayCase > 30) {
        resetSelectedDay = true;
      }
    } else {
      for (let i = 1; i <= 31; i += 1) {
        dayItems.push(<MenuItem value={i} key={i} primaryText={`${i}`} />);
      }

      if (selectedDayCase > 31) {
        resetSelectedDay = true;
      }
    }

    this.setState({
      dayItems,
      selectedDayCase: resetSelectedDay ? 1 : selectedDayCase,
    });
    return dayItems;
  };

  render() {
    const {
      dayItems,
      selectedMonthDonut,
      selectedYearDonut,
      selectedYearBar,
      selectedDayCase,

      selectedMonthCase,
      selectedYearCase,
    } = this.state;

    const { onSelect, active, auto } = this.props;

    const date = dayjs(
      `${
        active === 'daily' || active === 'weekly' ? selectedDayCase : 1
      }-${selectedMonthCase}-${selectedYearCase}`,
    ).format('YYYY-MM-DD');

    return (
      <div className="case-calender">
        {(active === 'daily' || active === 'weekly' || active === 'monthly') && (
          <DropDownMenu
            value={selectedMonthCase}
            onChange={(event, key, value) => {
              this.handleChangeCaseCalender(event, key, value, 'selectedMonthCase');
              const date = dayjs(
                `${value}-${
                  active === 'daily' || active === 'weekly'
                    ? selectedDayCase
                    : auto === 'start'
                      ? 1
                      : daysInMonth(key + 1, selectedYearCase)
                }-${selectedYearCase}`,
              ).format('YYYY-MM-DD');

              onSelect(date);
            }}
            underlineStyle={{ display: 'none' }}
            style={{
              fontSize: '12px',
              height: '24px',
              borderRadius: '50px',
              border: '1px solid #B3B3B3',
              marginRight: '5px',
            }}
            labelStyle={{
              height: '25px',
              lineHeight: '25px',
              paddingLeft: '15px',
              paddingRight: '30px',
            }}
            iconStyle={{ top: '-12px', right: '-5px' }}
            selectedMenuItemStyle={{ color: '#000' }}
            maxHeight={300}
            menuItemStyle={{ fontSize: '12px', height: '20px' }}
          >
            {monthItems}
          </DropDownMenu>
        )}

        {(active === 'daily' || active === 'weekly') && (
          <DropDownMenu
            value={selectedDayCase}
            onChange={(event, key, value) => {
              this.handleChangeCaseCalender(event, key, value, 'selectedDayCase');
              const date = dayjs(`${value}-${selectedMonthCase}-${selectedYearCase}`).format(
                'YYYY-MM-DD',
              );

              onSelect(date);
            }}
            underlineStyle={{ display: 'none' }}
            style={{
              fontSize: '12px',
              height: '24px',
              borderRadius: '50px',
              border: '1px solid #B3B3B3',
              marginRight: '5px',
            }}
            labelStyle={{
              height: '25px',
              lineHeight: '25px',
              paddingLeft: '15px',
              paddingRight: '30px',
            }}
            iconStyle={{ top: '-12px', right: '-5px' }}
            selectedMenuItemStyle={{ color: '#000' }}
            maxHeight={300}
            // menuItemStyle={{ fontSize: '12px', height: '20px' }}
          >
            {dayItems}
          </DropDownMenu>
        )}

        <DropDownMenu
          value={selectedYearCase}
          onChange={(event, key, value) => {
            this.handleChangeCaseCalender(event, key, value, 'selectedYearCase');

            const date = dayjs(
              `${active === 'yearly' ? (auto === 'start' ? 1 : 12) : selectedMonthCase}-${
                active === 'daily' || active === 'weekly'
                  ? selectedDayCase
                  : auto
                    ? 1
                    : active === 'monthly'
                      ? daysInMonth(selectedMonthCase)
                      : 31
              }-${value}`,
            ).format('YYYY-MM-DD');
            onSelect(date);
          }}
          underlineStyle={{ display: 'none' }}
          style={{
            fontSize: '12px',
            height: '24px',
            borderRadius: '50px',
            border: '1px solid #B3B3B3',
          }}
          labelStyle={{
            height: '25px',
            lineHeight: '25px',
            paddingLeft: '15px',
            paddingRight: '30px',
          }}
          iconStyle={{ top: '-12px', right: '-5px' }}
          selectedMenuItemStyle={{ color: '#000' }}
          maxHeight={300}
          // menuItemStyle={{ fontSize: '12px', height: '20px' }}
        >
          {yearItems}
        </DropDownMenu>
      </div>
    );
  }
}

RzCustomDate.propTypes = {};

export default RzCustomDate;
