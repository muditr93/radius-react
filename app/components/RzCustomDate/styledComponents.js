import styled from 'styled-components';

export const styledComponents = {
  Wrapper: styled.div`
    display: flex;
    height: inherit;
    flex-direction: column;
    padding: 8px 50px 8px 50px;
    ${'' /* .donutChart{
      width: auto;
      height: 90%;
    } */} .layer1 {
      position: relative;
      display: flex;
      justify-content: center;
      text-align: center;
      flex: 1.2;
      .box {
        height: 80%;
        display: flex;
        margin: 0px 20px;
        width: 170px;
        border-radius: 10px !important;
        .cases-graph {
          border-radius: 10px 0px 0px 10px;
          display: flex;
          flex-direction: column;
          flex: 1;
          background: #ffd221;
        }
        .cases-values {
          display: flex;
          flex: 2;
        }
      }
    }
    .layer2 {
      flex: 5;
      display: flex;
      margin-top: -12px;
      margin-bottom: 8px;
      .mapBox {
        flex: 10;
        height: 100%;
        padding: 10px;
      }
      .donut {
        flex: 6;
        height: 100%;
        padding: 10px;
        ${'' /* .recharts-pie-label-line {
          stroke: #FFF;
        } */} hr {
          margin: 2px 15px 0px !important;
        }
      }
    }
    .layer3 {
      display: flex;
      flex: 5;
      margin-left: 10px;
      margin-bottom: 8px;
      .bargraph {
        flex: 3;
        height: 100%;
        padding: 10px;
        .recharts-layer.recharts-cartesian-axis.recharts-xAxis.xAxis {
          font-size: 12px;
        }
        .recharts-responsive-container {
          margin: auto;
        }
        hr {
          margin: 2px 0px 0px !important;
        }
      }
      .dates {
        flex: 2;
        height: 100%;
        padding: 10px;
        ${''} margin: 0px -10px 0px 20px;
        .case-calender {
          margin-top: 4px;
        }
        .cases-table {
          height: 85%;
          padding-bottom: 12px;
          div {
            max-height: 100%;
          }
        }
        hr {
          margin: 2px 0px 0px !important;
        }
      }
      ${'' /* .agents {
        flex: 2;
        height: 100%;
        padding: 10px;
        margin-right: -10px;
        .agents-table {
          height: 92%;
          padding-bottom: 12px;
          div {
            max-height: 100%;
          }
        }
        hr {
          margin: 2px 0px 0px !important;
        }
      } */};
    }
  `,
};
