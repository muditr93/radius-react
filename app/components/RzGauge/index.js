import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import SolidGauge from 'highcharts-solid-gauge';
import GraphTitle from 'components/GraphTitle';

HighchartsMore(ReactHighcharts.Highcharts);
SolidGauge(ReactHighcharts.Highcharts);

class RzGauge extends Component {
  constructor (props) {
    super(props);
  }
  render () {
    const {data, compare} = this.props;
    let total = 0;
    data.forEach((element) => {
      total += Number(element.y);
    });
    return (
      <div style={{ background: 'white', height: '100%'}}>
        <GraphTitle title="Energy Supplied" compare={compare} />
          <ReactHighcharts isPureConfig config={
          multiChartsConfig(data[0].y,data[1].y, data[2].y, data[0].name, data[1].name, data[2].name, total)}></ReactHighcharts>
      </div>
    );
  }
}

const multiChartsConfig = (val1, val2, val3, name1, name2, name3, total) => {
  return {
    chart: {
        type: 'solidgauge',
        height: '100%',
    },
    title: null,
    liveRedraw: false,
    reDraw: false,
    animation: false,
    tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            marginTop: '-20px',
            fontSize: '11px'
        },
        pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
        positioner: function (labelWidth) {
            return {
                x: (this.chart.chartWidth - labelWidth) / 2,
                y: (this.chart.plotHeight / 2) + 15
            };
        }
    },
    // the value axis
    yAxis: {
      min: 0,
      max: 100,
      lineWidth: 0,
      tickPositions: []
    },
    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: true
        }
    },
    credits:     {
        enabled: false
    },
    pane: {
        startAngle: 0,
        endAngle: 360,
        background: [
          {
            backgroundColor: '#4D8CC63F',
            outerRadius: '110%',
            innerRadius: '100%',
            borderWidth: 0
          },{
            backgroundColor: '#4DB4384D',
            outerRadius: '93%',
            innerRadius: '83%',
            borderWidth: 0
          },{
            backgroundColor: '#4D3A4D6C',
            outerRadius: '76%',
            innerRadius: '66%',
            borderWidth: 0
          }
        ]
    },
    series: [
      {
        name: name1,
        rounded: true,
        data: [{
          color: '#3A4D6C',
          radius: '110%',
          innerRadius: '100%',
          y: Number(((val1 / total) * 100).toFixed(2)),
        }]
      },
      {
        name: name2,
        data: [{
          color: '#8CC63F',
          radius: '93%',
          innerRadius: '83%',
          y: Number(((val2 / total) * 100).toFixed(2))
        }]
      },
      {
        name: name3,
        data: [{
          color: '#B4384D',
          radius: '76%',
          innerRadius: '66%',
          y: Number(((val3 / total) * 100).toFixed(2))
        }]
      }
    ]
  };
}

export default RzGauge;
