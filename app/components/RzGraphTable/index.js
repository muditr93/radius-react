/**
 *
 * RzGraphTable
 *
 */

import React from 'react';
import GraphTitle from 'components/GraphTitle';
import styled from 'styled-components';

const Wrapper = styled.div`
  background: white;
  height: 100%;
  thead {
    margin-bottom: 20px;
    background: #2C4F6F;

  th {
    font-size: 13px;
    text-align: center!important;
    height: 66px;
    text-align: left;
    color: #ffffff;
    font-weight: 600;
    border: 0px solid transparent;

    &:first-child {
      border-left-style: solid;
      border-top-left-radius: 8px;
      border-bottom-left-radius: 8px;
    }
    &:last-child {
      border-right-style: solid;
      border-bottom-right-radius: 8px;
      border-top-right-radius: 8px;
    }
  }
  }
  table {
    width: 100%;
    height: auto;
    text-align: center;
    border-collapse: collapse;
    font-size: 11px;
    line-height: 36px;
    tbody {
      tr {
        transform: perspective(1px) translateZ(0);
        transition-duration: 0.3s;
        transition-property: box-shadow, transform;
        box-shadow: 0 0 1px transparent;

        &:hover {
          cursor: pointer;
          border-radius: 10px;
          box-shadow: 0 4px 10px 1px rgba(0, 0, 0, .3);
          transform: scale(1.01);
        }
      }
    }
    td {
      font-size:12px;
      text-align: center;
      position: relative;
      padding: 10px;
      border: 0px solid transparent;
      background: #9999995e;

      &:first-child {
        border-left-style: solid;
        border-top-left-radius: 8px;
        border-bottom-left-radius: 8px;
      }
      &:last-child {
        border-right-style: solid;
        border-bottom-right-radius: 8px;
        border-top-right-radius: 8px;
      }
    }
  }
`;
/* eslint-disable react/prefer-stateless-function */
class RzGraphTable extends React.Component {
  render() {
    const { series, title, compare } = this.props;
    return (
      <Wrapper>
        <GraphTitle title={title} compare={compare} />
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Count</th>
              <th>Duratrion</th>
            </tr>
          </thead>
          <tbody>
          {(series
            && series.length
            && series.map(x => (
              <tr>
                <td>{x.discomName}</td>
                <td>{x.interruptionCount}</td>
                <td>{x.interruptionDuration}</td>
              </tr>
            )))
            || 'Data not available..'}
            </tbody>
        </table>
      </Wrapper>
    );
  }
}

RzGraphTable.propTypes = {};

export default RzGraphTable;
