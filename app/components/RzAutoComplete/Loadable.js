/**
 *
 * Asynchronously loads the component for RzAutoComplete
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
