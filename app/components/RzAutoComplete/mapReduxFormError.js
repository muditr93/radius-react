const mapReduxFormError = (
  {
    meta: { touched, error, warning } = {},
  },
  errorProp = 'errorText'
) => touched && (error || warning) ? { [errorProp]: error || warning } : null;

export default mapReduxFormError;
