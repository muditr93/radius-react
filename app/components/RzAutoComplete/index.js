/**
*
* RzAutoComplete
*
*/

import pick from 'lodash/pick';
import React from 'react';
// import styled from 'styled-components';
import PropTypes from 'prop-types';
import AutoComplete from 'material-ui/AutoComplete';
import mapReduxFormError from './mapReduxFormError';

class RzAutoComplete extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  state = {
    searchText: '',
  };

  handleUpdateInput = (searchText) => {
    const { input, dataSourceConfig, onUpdateInput } = this.props;

    if (!dataSourceConfig) {
      input.onChange(searchText);
    }

    if (onUpdateInput) {
      onUpdateInput(searchText);
    }
  };

  handleNewRequest = (result) => {
    const { input, onNewRequest, dataSourceConfig } = this.props;

    input.onChange(
      typeof result === 'object' && dataSourceConfig ? result[dataSourceConfig.value] : result
    );

    if (onNewRequest) {
      onNewRequest(result);
    }
  };

  handleKeyUp = (event) => {
    this.setState({ searchText: event.target.value });
  };

  render() {
    const { floatingLabelText, dataSource, dataSourceConfig } = this.props;
    const otherProps = pick(this.props, [
      'disabled',
      'onUpdateInput',
      'onNewRequest',
      'dataSourceConfig',
      'errorText',
      'hintText',
      'fullWidth',
      'style',
      'listStyle',
      'menuStyle',
      'textFieldStyle',
      'inputStyle',
      'id',
      'floatingLabelStyle',
    ]);

    return (
      <AutoComplete
        fullWidth
        floatingLabelText={floatingLabelText}
        searchText={dataSourceConfig && dataSource ? (dataSource.find((item) => item[dataSourceConfig.value] === this.state.searchText) || {})[dataSourceConfig.text] : this.state.searchText}
        onUpdateInput={this.handleUpdateInput}
        onNewRequest={this.handleNewRequest}
        dataSource={dataSource}
        filter={AutoComplete.fuzzyFilter}
        onKeyUp={this.handleKeyUp}
        {...mapReduxFormError(this.props)}
        {...otherProps}
      />
    );
  }
}

RzAutoComplete.propTypes = {
  floatingLabelText: PropTypes.string.isRequired,
  dataSource: PropTypes.array.isRequired,
  dataSourceConfig: PropTypes.object,
  onUpdateInput: PropTypes.func,
  onNewRequest: PropTypes.func,
  input: PropTypes.object,
};

export default RzAutoComplete;
