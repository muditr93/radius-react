/**
 *
 * ComparisonBoard
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

import RzCustomDate from 'components/RzCustomDate';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import dayjs from 'dayjs';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import RzGraphTable from 'components/RzGraphTable';
import DateRangeGraph from 'components/DateRangeGraph';

const Button = styled.div`
  margin-top: 15px;
  padding: 5px 10px 5px 10px;
  box-sizing: border-box;
  font-size: 12px;
  font-weight: light;
  font-family: Montserrat;
  border-bottom: 1px solid #d3d3d361;
  color: #ffffff;
  text-transform: uppercase;
  background: #00a99d;
  border-radius: 30px;
  text-align: center;
  box-shadow: 1px 2px 1px 0px rgba(0, 0, 0, 0.2);
  width: 140px;
  .compareIcon {
    box-sizing: border-box;
    float: right;
    padding-right: 5px;
    cursor: pointer;
    &:hover {
      //box-shadow: 1px 2px 1px 0px rgba(186,179,186,1)
    }
  }
`;

const style1 = {
  grid: {
    boxSizing: 'border-box',
  },
  gridMinor: {
    paddingLeft: 0,
    paddingRight: 0,
  },
};
const styles = {
  root: {
    color: green[600],
    '&$checked': {
      color: green[500],
    },
  },
  checked: {},
};

/* eslint-disable react/prefer-stateless-function */
class ComparisonBoard extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedValue: 'daily',
      type: '',
      panes: 2,
    };
  }

  handleChange = event => {
    this.setState({ selectedValue: event.target.value });
  };

  dateSubmit = (type, pane, startDate, endDate, dispatch, getComparisonData, active) => {
    console.log('startDate, endDate: ', startDate, endDate);

    return dispatch(
      getComparisonData({
        type,
        pane,
        startDate,
        endDate,
      }),
    );
  };

  handleSelectChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  render() {
    const {
      classes,
      dispatch,
      getComparisonData,
      comparisonData,
      reset,
      comparisonType,
    } = this.props;
    let { type, panes } = this.state;

    if (!type) {
      type = comparisonType;
      this.state.type = comparisonType;
    }

    return (
      <div>
        <div
          style={{
            borderBottom: '1px solid #d3d3d361',
            marginBottom: '20px',
          }}
        >
          <Grid container spacing={24} style={{ paddingTop: '10px' }}>
            <Grid item style={style1.grid} xs={6}>
              <div>
                <Select
                  value={this.state.type}
                  onChange={this.handleSelectChange}
                  inputProps={{
                    name: 'type',
                    id: 'comparison Type',
                  }}
                  style={{
                    fontSize: '12px',
                    fontWeight: 'bold',
                    color: '#cf3446',
                    fontFamily: 'Montserrat',
                  }}
                >
                  <MenuItem
                    style={{
                      fontSize: '12px',
                      color: '#B3B3B3',
                      fontFamily: 'Montserrat',
                    }}
                    value={'interruption'}
                  >
                    Interruption
                  </MenuItem>
                  <MenuItem
                    style={{
                      fontSize: '12px',
                      color: '#B3B3B3',
                      fontFamily: 'Montserrat',
                    }}
                    value={'feederEnergy'}
                  >
                    Feeder Energy
                  </MenuItem>
                  <MenuItem
                    style={{
                      fontSize: '12px',
                      color: '#B3B3B3',
                      fontFamily: 'Montserrat',
                    }}
                    value={'elu'}
                  >
                    Total Amount
                  </MenuItem>
                  {/* <MenuItem
                    style={{
                      fontSize: '12px',
                      color: '#B3B3B3',
                      fontFamily: 'Montserrat',
                    }}
                    value={'bestWorst'}
                  >
                    Performance
                  </MenuItem> */}
                </Select>
              </div>
            </Grid>
            <Grid item style={style1.grid} xs={6}>
              <div>
                <Radio
                  checked={this.state.selectedValue === 'daily'}
                  onChange={this.handleChange}
                  value="daily"
                  name="radio-button-daily"
                  aria-label="A"
                  classes={{
                    root: classes.root,
                    checked: classes.checked,
                  }}
                />
                Daily
                <Radio
                  checked={this.state.selectedValue === 'weekly'}
                  onChange={this.handleChange}
                  value="weekly"
                  name="radio-button-demo"
                  aria-label="B"
                  classes={{
                    root: classes.root,
                    checked: classes.checked,
                  }}
                />
                Weekly
                <Radio
                  checked={this.state.selectedValue === 'monthly'}
                  onChange={this.handleChange}
                  value="monthly"
                  name="radio-button-demo"
                  aria-label="C"
                  classes={{
                    root: classes.root,
                    checked: classes.checked,
                  }}
                />
                Monthly
                <Radio
                  checked={this.state.selectedValue === 'yearly'}
                  onChange={this.handleChange}
                  value="yearly"
                  name="radio-button-demo"
                  aria-label="D"
                  classes={{
                    root: classes.root,
                    checked: classes.checked,
                  }}
                />
                Yearly
              </div>
            </Grid>
          </Grid>
        </div>

        <Grid container spacing={24} style={{ paddingTop: '10px' }}>
          {new Array(panes)
            .fill()
            .map((e, i) => (
              <DateRangeGraph
                submit={(startDate, endDate) =>
                  this.dateSubmit(type, i, startDate, endDate, dispatch, getComparisonData)
                }
                pane={i}
                graphData={{}}
                graphData={comparisonData && comparisonData.pane === i ? comparisonData : {}}
                timeOrder={this.state.selectedValue}
                type={type}
                border={i % 2 ? {} : { borderRight: '1px solid #d3d3d361' }}
                key={i.toString()}
              />
            ))}
          <Grid item style={style1.grid} xs={6}>
            <Button onClick={() => this.setState({ panes: panes + 1 })}>+ Add another</Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

ComparisonBoard.propTypes = {};

export default withStyles(styles)(ComparisonBoard);
