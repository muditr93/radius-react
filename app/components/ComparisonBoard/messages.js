/*
 * ComparisonBoard Messages
 *
 * This contains all the text for the ComparisonBoard component.
 */

import { defineMessages } from "react-intl";

export default defineMessages({
  header: {
    id: "app.components.ComparisonBoard.header",
    defaultMessage: "This is the ComparisonBoard component !"
  }
});
