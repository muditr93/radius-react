/**
 *
 * RzStackedBarGraph
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
import ReactHighcharts from "react-highcharts";

/* eslint-disable react/prefer-stateless-function */
class RzStackedBarGraph extends React.Component {
  render() {
    const config = {
      chart: {
        type: 'column'
      },
      title: {
          text: 'Feeder Status'
      },

      xAxis: {
          categories: ['PWNL', 'DWNL', 'MWNL', 'KEESco', 'puWNL']
      },

      yAxis: {
          allowDecimals: false,
          min: 0,
      },
      plotOptions: {
          column: {
              stacking: 'normal'
          }
      },

      series: [{
          name: 'John',
          data: [5, 3, 4, 7, 2],
          stack: 'male'
      }, {
          name: 'Joe',
          data: [3, 4, 4, 2, 5],
          stack: 'male'
      }]
    }
    return <ReactHighcharts config={config}/>;
  }
}

RzStackedBarGraph.propTypes = {};

export default RzStackedBarGraph;
