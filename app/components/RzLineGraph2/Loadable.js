/**
 *
 * Asynchronously loads the component for RzLineGraph
 *
 */

import Loadable from "react-loadable";

export default Loadable({
  loader: () => import("./index"),
  loading: () => null
});
