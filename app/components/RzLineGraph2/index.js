/**
 *
 * RzLineGraph2
 *
 */

import React from 'react';
import GraphTitle from 'components/GraphTitle';
import ReactHighcharts from 'react-highcharts';
import RzCustomDate from 'components/RzCustomDate';
const config = {
  title: {
    text: false,
  },
  colors: ['#00A99D'],
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },
  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
      },
    },
  },
  yAxis: {
    gridLineWidth: 0,
    minorGridLineWidth: 0,
  },
  series: [
    {
      name: 'R Phase',
      data: [43934, 52503, 77177, 69658, 43934],
    },
  ],
  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 400,
        },
      },
    ],
  },
};
class RzLineGraph2 extends React.PureComponent {
  render() {
    const {data} = this.props;
    const config = {
      title: {
        text: false,
      },
      colors: ['#3A4D6C', '#B4384D', '#00A99D', '#8CC63F'],
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
        },
      },
      yAxis:{
        gridLineWidth: 0,
        minorGridLineWidth: 0,
      },
      xAxis: {
              categories: data.date,
              labels: {
                  enabled:true,
                  style: {
                      color: '#BDBDBD',
                      font: '11px Trebuchet MS, Verdana, sans-serif'
                  }
              },
              title: {
                  style: {
                      color: '#BDBDBD',
                      font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
                  },
                  text: 'Days',
                  margin: 0
              }
              },
    series: [{name: 'MWh', type:'spline', data: data.kvah.map(x => +x)}],
    };
    return (
      <div style={{ background: 'white' }}>
        <div style={{ padding: '10px' }} >
          <RzCustomDate
            onSelect={date => this.props.handleDateChange(date)}
            active={'monthly'}
            auto="start"
          />
        </div>
        <GraphTitle title="Current Month Energy Supplied" />
        <ReactHighcharts config={config} />
      </div>
    );
  }
}

RzLineGraph2.propTypes = {};

export default RzLineGraph2;
