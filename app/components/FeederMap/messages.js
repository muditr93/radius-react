/*
 * FeederMap Messages
 *
 * This contains all the text for the FeederMap component.
 */

import { defineMessages } from "react-intl";

export default defineMessages({
  header: {
    id: "app.components.FeederMap.header",
    defaultMessage: "This is the FeederMap component !"
  }
});
