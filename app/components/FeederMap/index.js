/**
 *
 * FeederMap
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// import { FormattedMessage } from "react-intl";
// import messages from "./messages";
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { push } from 'react-router-redux';
import Map from 'components/Map';
import dayjs from 'dayjs';
import GreenLight from 'images/green circle.png';
import RedLight from 'images/red-circle.png';

import styled from 'styled-components';

const Button = styled.div`
  margin-top: 30px;
  padding: 5px 10px 5px 10px;
  box-sizing: border-box;
  font-size: 10px;
  font-weight: light;
  font-family: Montserrat;
  // border-bottom: 1px solid #d3d3d361;
  color: #FFFFFF;
  text-transform: uppercase;
  background: #00A99D;
  border-radius: 30px;
  text-align: center;
  box-shadow: 1px 2px 1px 0px rgba(0, 0, 0, 0.2);
  .compareIcon {
    box-sizing: border-box;
    float: right;
    padding-right: 5px;
    cursor: pointer;
    &:hover {
      //box-shadow: 1px 2px 1px 0px rgba(186,179,186,1)
    }
  }
`;

const InfoTitle = styled.div`
font-weight: light;
font-family: Montserrat;
color: #B3B3B3;
font-size: 13px;
text-transform: uppercase;
`;

const InfoValue = styled.div`
font-weight: light;
font-family: Montserrat;
color: #000000;
font-size: 16px;
margin-bottom: 10px;
`;
const style1 = {
  grid: {
    boxSizing: 'border-box',
  },
};

const style2 = {
  grid: {
    boxSizing: 'border-box',
    height: '424px',
  },
  boxSizing: 'border-box',
};

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    height: '100%',
  },
  list: {
    color: 'red',
    fontSize: '40px',
  },
});
/* eslint-disable react/prefer-stateless-function */
class FeederMap extends React.Component {
  itemClickHandler = (url) => {
    this.props.dispatch(push(url));
  }
  handleClick = (data) => {
    this.setState({
      id: data.sensorId,
    })
    console.log(data);
    this.props.getFeederInfo(data)
  }
  render() {
    const {
      classes, feeders, feederInfo, getFeederInfo, maponly ,
    } = this.props;

    const {
      name, meterSrNo, md_kVA, rcurrent, ycurrent, bcurrent, instantLoad, cbstatus, incommerStatus, lastReadingTime,
    } = feederInfo;

    const markers = feeders
      .filter(x => typeof x.latitude === 'number' && typeof x.logitude === 'number')
      .map(x => ({ lat: x.latitude, long: x.logitude, ...x }));

    return (
      <div>
        <div>
          <div style={maponly ? { display: 'block' } : { display: 'none' }}>
            <Paper>
              <Map height="400px" markers={markers} markerClick={this.handleClick} />
            </Paper>
          </div>
          <div style={maponly ? { display: 'none' } : { display: 'block' }}>


            <Grid container spacing={24}>
              <Grid item style={style1.grid} xs={12} sm={8}>
                <Paper>
                  <Map height="400px" markers={markers} markerClick={this.handleClick} />
                </Paper>
              </Grid>
              <Grid item style={style2.grid} xs={12} sm={4}>
                <Paper className={classes.root} elevation={1}>
                  <Grid container spacing={24}>

                    <Grid item style={style1.grid} xs={7} sm={7} />
                    <Grid item style={style1.grid} xs={5} sm={5}>
                      <div style={{
                      marginBottom: '10px',
                      fontFamily: 'Montserrat',
                    }}
                    >
                      {dayjs(lastReadingTime).format('DD/MM/YYYY')}
                    </div>
                    </Grid>
                  </Grid>
                  <Grid container spacing={24}>
                    <Grid item style={style1.grid} xs={7} sm={7}>
                      <InfoTitle>Feeder Name</InfoTitle>
                      <InfoValue>{name}</InfoValue>
                    </Grid>
                    <Grid item style={style1.grid} xs={5} sm={5} />
                  </Grid>
                  <div style={{ height: '40px' }} />
                  <Grid container spacing={24}>
                    <Grid item style={style1.grid} xs={7} sm={7}>
                      <InfoTitle>Meter Sr. No.</InfoTitle>
                      <InfoValue>{meterSrNo}</InfoValue>
                    </Grid>
                    <Grid item style={style1.grid} xs={5} sm={5}>
                      <InfoTitle>Instant Load (VA)</InfoTitle>
                      <InfoValue>{instantLoad}</InfoValue>
                    </Grid>
                  </Grid>
                  <Grid container spacing={24}>
                    <Grid item style={style1.grid} xs={7} sm={7}>
                      <InfoTitle>Current(R/Y/B)</InfoTitle>
                      <InfoValue>{`${rcurrent|| '--'}/${ycurrent|| '--'}/${bcurrent|| '--'}`}</InfoValue>
                    </Grid>
                    <Grid item style={style1.grid} xs={5} sm={5}>
                      <InfoTitle>MD (kVA)</InfoTitle>
                      <InfoValue>{md_kVA}</InfoValue>
                    </Grid>
                  </Grid>
                  <Grid container spacing={24}>
                    <Grid item style={style1.grid} xs={7} sm={7}>
                      <InfoTitle>Incomer</InfoTitle>
                      { incommerStatus === 'ON' ? <img src={GreenLight} /> : <img src={RedLight} /> }

                    </Grid>
                    <Grid item style={style1.grid} xs={5} sm={5}>
                      <InfoTitle>CB</InfoTitle>
                      { cbstatus === 'ON' ? <img src={GreenLight} /> : <img src={RedLight} /> }

                    </Grid>
                  </Grid>
                  <Grid container spacing={24}>
                    <Grid item style={style1.grid} xs={7} sm={7} />
                    <Grid item style={style1.grid} xs={5} sm={5}>
                      <Button onClick={() => this.itemClickHandler(`/feederDetails/${this.state.id}`)}> View Details </Button>
                    </Grid>
                  </Grid>


                </Paper>
              </Grid>
              {/*<Grid item style={style1.grid}>
                <div style={{ height: '400px' }} />
              </Grid>*/}
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

FeederMap.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FeederMap);
