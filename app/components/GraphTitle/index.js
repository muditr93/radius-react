/**
 *
 * GraphTitle
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import styled from 'styled-components';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { renderComponent } from 'recompose';

const HeaderTile = styled.div`
  padding: 10px 0 5px 10px;
  box-sizing: border-box;
  font-size: 13px;
  font-weight: bold;
  color: #cf3446;
  text-transform: uppercase;
  .compareIcon {
    box-sizing: border-box;
    float: right;
    padding-right: 5px;
    cursor: pointer;
    &:hover {
      // box-shadow: 1px 2px 1px 0px rgba(186,179,186,1)
    }
  }
`;

class GraphTitle extends React.Component {
  // handleSelectChange = event => {
  //   this.setState({ [event.target.name]: event.target.value });
  //   console.log('{ [event.target.name]: event.target.value }: ', {
  //     [event.target.name]: event.target.value,
  //   });
  // };

  render() {
    const {
      compare, title, bWCountUpdate, bWCount, selectType, performanceType
    } = this.props;

    return (
      <HeaderTile>
        {title}
        {compare && (
          <div className="compareIcon">
            <img
              src="https://image.ibb.co/jEC3JV/Comparison.png"
              alt="Comparison"
              height="18"
              border="0"
              onClick={() => {
                compare();
              }}
            />
          </div>
        )}
        {performanceType && (
          <div
            style={{
              float: 'right',
              paddingRight: '5px',
            }}
            >
            <Select
              value={performanceType}
              name="Type"
              inputProps={{
                name: 'type',
                id: 'feederType',
              }}
              onChange={event => selectType({
                [event.target.name]: event.target.value,
              })
              }
              >
          <MenuItem
            style={{
              fontSize: '12px',
              color: '#B3B3B3',
              fontFamily: 'Montserrat',
            }}
            value='interruption'
          >
            Interruption
          </MenuItem>
          <MenuItem
            style={{
              fontSize: '12px',
              color: '#B3B3B3',
              fontFamily: 'Montserrat',
            }}
            value='average_duration'
          >
            Average Duration
          </MenuItem>
          <MenuItem
            style={{
              fontSize: '12px',
              color: '#B3B3B3',
              fontFamily: 'Montserrat',
            }}
            value='duration'
          >
            Duration
          </MenuItem>
        </Select>
        </div>
        )}
        {bWCount && (
          <div
            style={{
              float: 'right',
              paddingRight: '5px',
            }}
          >
            <Select
              value={bWCount}
              onChange={event => bWCountUpdate({
                [event.target.name]: event.target.value,
              })
              }
              inputProps={{
                name: 'bWCount',
                id: 'feederCount',
              }}
              style={{
                fontSize: '12px',
                fontFamily: 'Roboto',
                borderRadius: '15px',
                boxShadow: '1px 2px 1px 2px rgba(0, 0, 0, 0.2)',
                paddingLeft: '15px',
              }}
            >
              {Array(6)
                .fill('0')
                .map((e, i) => (
                  <MenuItem
                    style={{
                      fontSize: '12px',
                      color: '#B3B3B3',
                      fontFamily: 'Montserrat',
                    }}
                    value={i + 1}
                    key={i.toString()}
                  >
                    {i + 1}
                  </MenuItem>
                ))}
            </Select>
          </div>
        )}
      </HeaderTile>
    );
  }
}

GraphTitle.propTypes = {};

export default GraphTitle;
