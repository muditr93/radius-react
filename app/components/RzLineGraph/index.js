/**
 *
 * RzLineGraph
 *
 */

import React from 'react';
import GraphTitle from 'components/GraphTitle';
import ReactHighcharts from 'react-highcharts';

const config = {
  title: {
    text: false,
  },
  colors: ['#3A4D6C', '#B4384D', '#00A99D', '#8CC63F'],
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },
  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
      },
    },
  },
  yAxis: {
    gridLineWidth: 0,
    minorGridLineWidth: 0,
  },
  series: [
    {
      type:'spline',
      name: '+15% MVAh',
      data: [
            6248,
            6248,
            6247,
            6247,
            6244,
            6246,
            6246,
            6243],
    },
    {
      type:'spline',
      name: '+15% Amount',
      data: [24916, 24064, 29742, 29851, 32490],
    },
    {
      type:'spline',
      name: '0% MVAh',
      data: [11744, 17722, 16005, 19771, 20185],
    },
    {
      type:'spline',
      name: '0% Amount',
      data: [10000, 10000, 7988, 12169, 15112],
    },
    {
      type:'spline',
      name: '-15% MVAh',
      data: [12908, 5948, 8105, 11248, 8989],
    },
    {
      type:'spline',
      name: '-15% Amount',
      data: [5000, 2948, 5105, 7248, 5989],
    },
  ],
  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 400,
        },
      },
    ],
  },
};
class RzLineGraph extends React.Component {
  render() {
    return (
      <div style={{ background: 'white' }}>
        <GraphTitle title="ESTIMATED ENERGY UNIT LOSES (MVAH)" />
        <ReactHighcharts config={config} />
      </div>
    );
  }
}

RzLineGraph.propTypes = {};

export default RzLineGraph;
