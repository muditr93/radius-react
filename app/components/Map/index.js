/**
 *
 * Map
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';

import {
  withScriptjs, withGoogleMap, GoogleMap, Marker, Polyline,
} from 'react-google-maps';

import FeederIcon from 'images/30x40-green.png';
import { compose, withProps } from 'recompose';
import messages from './messages';

// import m1 from 'images/clusterImages/m1.png';
// import m2 from 'images/clusterImages/m2.png';
// import m3 from 'images/clusterImages/m3.png';
// import m4 from 'images/clusterImages/m4.png';
// import m5 from 'images/clusterImages/m5.png';
import clusterIcons from 'images/clusterImages/cluster-icons.png';

const { MarkerClusterer } = require('react-google-maps/lib/components/addons/MarkerClusterer');

const MyNewMapComponent = withScriptjs(
  withGoogleMap(props => (
    <div>
      {
        <GoogleMap
          defaultZoom={props.center && props.center.lat !== 0 && props.center.long !== 0 ? 11 : 6}
          defaultCenter={{
            lat: 26.8449,
            lng: 80.9236,
          }}
          defaultOptions={{ styles: [  {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#d6e2e6"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#cfd4d5"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#7492a8"
              }
            ]
          },
          {
            "featureType": "administrative.neighborhood",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "lightness": 25
              }
            ]
          },
          {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#dde2e3"
              }
            ]
          },
          {
            "featureType": "landscape.man_made",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#cfd4d5"
              }
            ]
          },
          {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#dde2e3"
              }
            ]
          },
          {
            "featureType": "landscape.natural",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#7492a8"
              }
            ]
          },
          {
            "featureType": "landscape.natural.terrain",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#dde2e3"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [
              {
                "saturation": -100
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#588ca4"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#a9de83"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#bae6a1"
              }
            ]
          },
          {
            "featureType": "poi.sports_complex",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#c6e8b3"
              }
            ]
          },
          {
            "featureType": "poi.sports_complex",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#bae6a1"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
              {
                "saturation": -45
              },
              {
                "lightness": 10
              },
              {
                "visibility": "on"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#41626b"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#ffffff"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#c1d1d6"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#a6b5bb"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "on"
              }
            ]
          },
          {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#9fb6bd"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#ffffff"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "labels.icon",
            "stylers": [
              {
                "saturation": -70
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#b4cbd4"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#588ca4"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#008cb5"
              }
            ]
          },
          {
            "featureType": "transit.station.airport",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "saturation": -100
              },
              {
                "lightness": -5
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#a6cbe3"
              }
            ]
          }
        ] }}

        >
          <MarkerClusterer
            onClick={props.onMarkerClustererClick}
            averageCenter
            gridSize={60}
            styles={[
              {
                url: clusterIcons,
                height: 153,
                lineHeight: 153,
                width: 153,
              },
              {
                url: clusterIcons,
                height: 156,
                lineHeight: 156,
                width: 156,
              },
              {
                url: clusterIcons,
                height: 166,
                lineHeight: 166,
                width: 166,
              },
              {
                url: clusterIcons,
                height: 178,
                lineHeight: 178,
                width: 178,
              },
              {
                url: clusterIcons,
                height: 190,
                lineHeight: 190,
                width: 190,
              },
            ]}      
          >
            {props.markers
              && props.markers.length > 0
              && props.markers.map((marker, index) => (
                <Marker
                  key={index}
                  position={{ lat: marker.lat, lng: marker.long }}
                  onClick={() => props.markerClick(marker)}
                  defaultIcon={FeederIcon}
                />
              ))}
          </MarkerClusterer>
        </GoogleMap>
      }
    </div>
  )),
);

/* eslint-disable react/prefer-stateless-function */
class Map extends React.Component {
  render() {
    const { markers, height, markerClick } = this.props;
    return (
      <div>
        <MyNewMapComponent
          isMarkerShown
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBN5C_7k-WG7IFxZwCCe3sSQlS8HQ_9LoE"
          loadingElement={<div style={{ height: '100%' }} />}
          containerElement={<div style={{ height }} />}
          mapElement={<div style={{ height: '100%' }} />}
          markers={markers}
          markerClick={markerClick}
          onMarkerClustererClick={(markerClusterer) => {
            const clickedMarkers = markerClusterer.getMarkers();
            console.log(`Current clicked markers length: ${clickedMarkers.length}`);
            console.log(markerClusterer);
            // console.log(...clickedMarkers.map(x => [x.position.lat(), x.position.lng()]));
          }}
        />
      </div>
    );
  }
}

Map.propTypes = {};

export default Map;
