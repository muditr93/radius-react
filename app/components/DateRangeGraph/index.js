/**
 *
 * DateRangeGraph
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';

import RzCustomDate from 'components/RzCustomDate';
import Grid from '@material-ui/core/Grid';

import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import RzGraphTable from 'components/RzGraphTable';
import messages from './messages';
import dayjs from 'dayjs';
import RzGauge from 'components/RzGauge';
import RzELUTotal from 'components/RzELUTotal';
import RzCustomColumn from 'components/RzCustomColumn';

const Button = styled.div`
  margin-top: 30px;
  padding: 5px 10px 5px 10px;
  box-sizing: border-box;
  font-size: 10px;
  font-weight: light;
  font-family: Montserrat;
  border-bottom: 1px solid #d3d3d361;
  color: #ffffff;
  text-transform: uppercase;
  background: #00a99d;
  border-radius: 30px;
  text-align: center;
  box-shadow: 1px 2px 1px 0px rgba(0, 0, 0, 0.2);
  width: 100px;
  .compareIcon {
    box-sizing: border-box;
    float: right;
    padding-right: 5px;
    cursor: pointer;
    &:hover {
      //box-shadow: 1px 2px 1px 0px rgba(186,179,186,1)
    }
  }
`;

const style1 = {
  grid: {
    boxSizing: 'border-box',
  },
  gridMinor: {
    paddingLeft: 0,
    paddingRight: 0,
  },
};
const styles = {
  root: {
    color: green[600],
    '&$checked': {
      color: 'green[500]',
    },
  },
  checked: {
    color: green[600],
  },
};

const daysInMonth = (month, year) => {
  var d = new Date(year, month, 0);
  return d.getDate();
};

/* eslint-disable react/prefer-stateless-function */
class DateRangeGraph extends React.Component {
  constructor() {
    super();
    this.state = {
      startDate: '',
      endDate: '',
      dateRangeData: {},
      lastTimeOrder: '',
    };
  }

  updateState = (key, value) => this.setState({ key, value });

  render() {
    const { submit, pane, graphData, timeOrder, type, border } = this.props;
    console.log('graphData: ', graphData);

    const defaultStartDate = dayjs(
      `${timeOrder === 'daily' || timeOrder === 'weekly' ? dayjs().date() : 1}-${
        timeOrder === 'yearly' ? 1 : dayjs().format('MMMM')
      }-${dayjs().year()}`,
    ).format('YYYY-MM-DD');

    const defaultEndDate = dayjs(
      `${
        timeOrder === 'daily' || timeOrder === 'weekly'
          ? dayjs().date()
          : daysInMonth(dayjs().format('MM'), dayjs().year())
      }-${timeOrder === 'yearly' ? 'December' : dayjs().format('MMMM')}-${dayjs().year()}`,
    ).format('YYYY-MM-DD');
    let { dateRangeData, lastTimeOrder } = this.state;

    if (timeOrder !== lastTimeOrder) {
      this.setState({
        lastTimeOrder: timeOrder,
        startDate: '',
        endDate: '',
      });
    }

    if (graphData.pane === pane) {
      this.state.dateRangeData = graphData;
      dateRangeData = graphData;
    }
    return (
      <Grid item style={{ ...style1.grid, ...border }} xs={6}>
        <Grid container spacing={24}>
          <Grid item style={style1.grid} xs={6}>
            <div
              style={{
                fontFamily: 'Montserrat',
                color: '#999999',
                fontWeight: 'bold',
              }}
            >
              Start Date
            </div>
            <RzCustomDate
              onSelect={date => this.setState({ startDate: date })}
              active={timeOrder}
              auto="start"
            />
          </Grid>
          <Grid item style={style1.grid} xs={6}>
            {timeOrder !== 'daily' && (
              <div>
                <div
                  style={{
                    fontFamily: 'Montserrat',
                    color: '#999999',
                    fontWeight: 'bold',
                  }}
                >
                  End Date
                </div>
                <RzCustomDate
                  onSelect={date => this.setState({ endDate: date })}
                  active={timeOrder}
                />
              </div>
            )}
            <Button
              style={{ alignSelf: 'right' }}
              onClick={() => {
                if (timeOrder === 'daily')
                  this.state.endDate = this.state.startDate
                    ? this.state.startDate
                    : defaultStartDate;
                submit(
                  this.state.startDate ? this.state.startDate : defaultStartDate,
                  this.state.endDate ? this.state.endDate : defaultEndDate,
                );
              }}
            >
              Submit{' '}
            </Button>
          </Grid>
        </Grid>
        <div>
          {type === 'interruption' &&
            dateRangeData &&
            dateRangeData.body &&
            dateRangeData.body.resources && (
              <RzGraphTable
                title="Interruption"
                series={(dateRangeData.body && dateRangeData.body.resources) || []}
              />
            )}
          {type === 'feederEnergy' &&
            dateRangeData &&
            dateRangeData.body &&
            dateRangeData.body.resources && (
              <RzGauge
                data={
                  (dateRangeData.body &&
                    dateRangeData.body.resources.map(x => {
                      return { name: x.discomName, y: x['kvah'] };
                    })) || [{ name: 'No Data Available', y: 100 }]
                }
                compare={this.openModal}
              />
            )}
          {type === 'elu' &&
            dateRangeData &&
            dateRangeData.body &&
            dateRangeData.body.resources && (
              <RzELUTotal series={dateRangeData.body.resources} title="Total Amount" />
            )}
          {type === 'bestWorst' &&
            dateRangeData &&
            dateRangeData.body &&
            dateRangeData.body.resources && (
              <RzCustomColumn
                best={
                  dateRangeData.body.resources && dateRangeData.body.resources.length > 0
                    ? dateRangeData.body.resources.filter(x => x.status === 'BEST').map(x => {
                        return `${x.discomName} - ${x.substationName}`;
                      })
                    : []
                }
                worst={
                  dateRangeData.body.resources && dateRangeData.body.resources.length > 0
                    ? dateRangeData.body.resources.filter(x => x.status === 'WORST').map(x => {
                        return `${x.discomName} - ${x.substationName}`;
                      })
                    : []
                }
              />
            )}
        </div>
      </Grid>
    );
  }
}

DateRangeGraph.propTypes = {};

export default DateRangeGraph;
