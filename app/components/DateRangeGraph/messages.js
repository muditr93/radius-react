/*
 * DateRangeGraph Messages
 *
 * This contains all the text for the DateRangeGraph component.
 */

import { defineMessages } from "react-intl";

export default defineMessages({
  header: {
    id: "app.components.DateRangeGraph.header",
    defaultMessage: "This is the DateRangeGraph component !"
  }
});
