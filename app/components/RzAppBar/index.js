/**
*
* RzAppBar
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import muiThemeable from 'material-ui/styles/muiThemeable';
import AppBar from 'material-ui/AppBar';


function RzAppBar(props) {
  const { muiTheme, ...otherProps } = props;

  otherProps.style.background = muiTheme.appBar.background;

  return <AppBar {...otherProps} />;
}

RzAppBar.propTypes = {
  muiTheme: PropTypes.object.isRequired,
};

export default muiThemeable()(RzAppBar);
