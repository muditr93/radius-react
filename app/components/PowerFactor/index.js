/**
 *
 * PowerFactor
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import ReactHighcharts from 'react-highcharts';
import GraphTitle from 'components/GraphTitle';

/* eslint-disable react/prefer-stateless-function */
class PowerFactor extends React.Component {
  render() {
    const { categories, pf } = this.props;
    const config = {
      colors: ['#3A4D6C', '#B4384D', '#00A99D', '#8CC63F'],
      chart: {
        type: 'bar',
      },
      title: {
        text: false,
      },
      subtitle: {
        text: '< 0.90',
        align: 'right',
        style: {
          color: '#999999',
          fontFamily: 'Montserrat',
          fontSize: '14px',
        },
      },
      xAxis: {
        categories:
          categories && categories.length > 0
            ? categories
            : ['EODB', 'IPDS', '16 Nigam Nagar', 'REC'],
        crosshair: true,
        title: {
          text: null,
        },
      },
      yAxis: {
        min: 0,
        gridLineWidth: 0,
        minorGridLineWidth: 0,
        title: {
          text: '',
          align: 'high',
        },
        labels: {
          overflow: 'justify',
        },
      },
      tooltip: {
        valueSuffix: '',
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
        shadow: true,
      },
      credits: {
        enabled: false,
      },
      series: [
        {
          name: 'PF',
          data: pf && pf.length > 0 ? pf : [5, 2, 16, 19],
        },
      ],
    };
    return (
      <div style={{ background: 'white' }}>
        <GraphTitle title="Power Factor" />
        <ReactHighcharts config={config} />
      </div>
    );
  }
}

PowerFactor.propTypes = {};

export default PowerFactor;
