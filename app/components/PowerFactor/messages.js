/*
 * PowerFactor Messages
 *
 * This contains all the text for the PowerFactor component.
 */

import { defineMessages } from "react-intl";

export default defineMessages({
  header: {
    id: "app.components.PowerFactor.header",
    defaultMessage: "This is the PowerFactor component !"
  }
});
