/**
*
* RzMoreFilter
*
*/

import React from 'react';
import styled from 'styled-components';

import Checkbox from 'material-ui/Checkbox';
import ImageTune from 'material-ui/svg-icons/image/tune';
import RzAutoComplete from 'components/RzAutoComplete';

import { FormattedMessage } from 'react-intl';

import messages from './messages';

const Wrapper = styled.div`
  display: flex;
  position: relative;

  button {
    outline: none;
  }

  .popup {
    position: absolute;
    top: 65px;
    right: 0;
    padding: 20px;
    min-width: 300px;
    min-height: 300px;
    background: #fff;
    border-radius: 10px;
    box-shadow: 0 5px 20px 0 rgba(0, 0, 0, .5);
    text-align: left;
    z-index: 2;

    strong {
      display: block;
      margin-top: 15px;
    }

    &:after {
      content: '';
      position: absolute;
      top: -14px;
      right: 14px;
      width: 0;
      height: 0;
      border-left: 10px solid transparent;
      border-right: 10px solid transparent;
      border-bottom: 14px solid #fff;
    }
  }
`;

class RzMoreFilter extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      popupFocus: false,
      moreFitlerOpen: false,
    };
  }

  blurHandler(event) {
    const { relatedTarget, currentTarget } = event;

    setTimeout(() => {
      if (this.state.popupFocus) {
        this.setState({
          moreFitlerOpen: true,
        });

        return false;
      }

      if (!relatedTarget) {
        this.setState({
          moreFitlerOpen: false,
          popupFocus: false,
        });

        return false;
      }

      if (!currentTarget.contains(relatedTarget)) {
        this.setState({
          moreFitlerOpen: false,
          popupFocus: false,
        });
      }
    }, 100);

    return false;
  }

  moreFilterHandler() {
    this.setState({ moreFitlerOpen: !this.state.moreFitlerOpen });
  }

  render() {
    const filter = {
      sort: 0,
      name: 'event_id',
      label: 'Events',
      data: [
        'Birthday Party',
        'Big Promotion',
        'Holiday Coupon',
      ],
    };

    const checkboxStyle = {
      marginTop: '14px',
    };

    return (
      <Wrapper
        onBlur={(e) => this.blurHandler(e)}
      >
        <button
          onClick={() => this.moreFilterHandler()}
          className="button"
        >
          <ImageTune
            color="#ffcf3f"
            style={{
              width: '33px',
              height: '33px',
            }}
          />
        </button>
        <button
          className="popup"
          style={{ display: this.state.moreFitlerOpen ? 'inline-block' : 'none' }}
          onFocus={() => this.setState({ popupFocus: true, moreFitlerOpen: true })}
          onBlur={() => this.setState({ popupFocus: false })}
        >
          <div>
            <FormattedMessage {...messages.tagInput}>
              {
                (message) => (
                  <RzAutoComplete
                    key={filter.sort}
                    dataSource={filter.data}
                    floatingLabelText={message}
                    // enterHandler={input.onFocus}
                  />
                )
              }
            </FormattedMessage>

            <strong>CASE STATUS</strong>

            <Checkbox
              style={checkboxStyle}
              label="Case Status A"
            />
            <Checkbox
              style={checkboxStyle}
              label="Case Status B"
            />
            <Checkbox
              style={checkboxStyle}
              label="Case Status C"
            />
            <Checkbox
              style={checkboxStyle}
              label="Case Status D"
            />
            <Checkbox
              style={checkboxStyle}
              label="Case Status E"
            />
          </div>
        </button>
      </Wrapper>
    );
  }
}

RzMoreFilter.propTypes = {

};

export default RzMoreFilter;
