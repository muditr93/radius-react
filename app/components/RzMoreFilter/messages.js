/*
 * RzMoreFilter Messages
 *
 * This contains all the text for the RzMoreFilter component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  tagInput: {
    id: 'app.components.RzMoreFilter.tagInput',
    defaultMessage: 'Filter by policy providers...',
  },
});
