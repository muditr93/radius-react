/**
 *
 * RzBulletGraph
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import ReactHighcharts from 'react-highcharts';

const config = {
  chart: {
    inverted: true,
    type: 'bullet',
  },
  title: {
    text: null,
  },
  legend: {
    enabled: false,
  },
  yAxis: {
    gridLineWidth: 0,
  },
  plotOptions: {
    series: {
      pointPadding: 0.25,
      borderWidth: 0,
      color: '#3a4d6c',
      targetOptions: {
        width: '200%',
      },
    },
  },
  credits: {
    enabled: false,
  },
  exporting: {
    enabled: false,
  },
};

class RzBulletGraph extends React.Component {
  render() {
    return <ReactHighcharts config={config} />;
  }
}

RzBulletGraph.propTypes = {};

export default RzBulletGraph;
