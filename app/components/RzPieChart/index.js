/**
 *
 * RzPieChart
 *
 */

import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { toASCII } from 'punycode';
import GraphTitle from 'components/GraphTitle';

// import PropTypes from 'prop-types';
// import styled from 'styled-components';

class RzPieChart extends React.Component {
  render() {
    const { data, compare } = this.props;
    let total = 0;
    data.forEach((element) => {
      total += Number(element.y);
    });

    const config = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
      },
      plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
      },
      chartOptions: {
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'bottom',
        },
      },
      colors: ['#3A4D6C', '#8CC63F',  '#B4384D', '#00A99D'],
      title: {
        text: false,
      },
      tooltip: {
        headerFormat: '<span style="font-size:8px">{point.key}</span><table>',
        pointFormat: '<td style="padding:-10px"><b>{point.y:.1f} %</b></td></tr>',
        footerFormat: '</table>',
        shared: false,
        useHTML: true,
      },
      series: [
        {
          name: 'Share',
          data: data.map(x => ({
            name: x.name,
            y: Number(((x.y / total) * 100).toFixed(2)),
          })),
        },
      ],
    };

    return (
      <div style={{ background: 'white' }}>
        <GraphTitle title="Energy Supplied" compare={compare} />
        <ReactHighcharts config={config} />
      </div>
    );
  }
}

RzPieChart.propTypes = {};

export default RzPieChart;
