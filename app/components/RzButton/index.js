/**
*
* RzButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { lighten, darken } from 'polished';

const Button = styled.div`
  ${(props) => props.block && 'display: block;'}
  margin: 2px;
  width: 100%;
  font-size: 12px;
  font-weight: bold;
  position: absolute;
  bottom: 20px;
  padding: 10px;
  text-align: center;
  box-shadow: 2px 2px 5px 0px rgba(0,0,0,0.75);
  color: ${(props) => props.color ? props.color : '#111'};
  border-radius: 22px;
  background-color: ${(props) => props.bgColor ? props.bgColor : 'transparent'};
  user-select: none;

  &:hover {
    cursor: pointer;
    transform: translateY(-1px);
    margin-bottom: -1px;
    background-color: ${(props) => props.bgColor ? lighten(0.1, props.bgColor) : 'transparent'};
  }

  &:active {
    transform: translateY(1px);
    margin-bottom: 1px;
    background-color: ${(props) => props.bgColor ? lighten(0.1, props.bgColor) : 'transparent'};
  }
`;


function RzButton({
  children,
  ...otherProps
}) {
  return (
    <Button {...otherProps}>{children}</Button>
  );
}

RzButton.propTypes = {
  color: PropTypes.string,
  bgColor: PropTypes.string,
  children: PropTypes.any,
};

export default RzButton;
