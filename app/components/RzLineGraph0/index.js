/**
 *
 * RzLineGraph0
 *
 */

import React from 'react';
import GraphTitle from 'components/GraphTitle';
import ReactHighcharts from 'react-highcharts';
import RzCustomDate from 'components/RzCustomDate';

class RzLineGraph0 extends React.Component {
  render() {
    const {data} = this.props;
    const config = {
      title: {
        text: false,
      },
      colors: ['#3A4D6C', '#B4384D', '#00A99D', '#8CC63F'],
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
        },
      },
      yAxis:{
        gridLineWidth: 0,
        minorGridLineWidth: 0,
      },
      xAxis: {
              categories: data.date,
              labels: {
                  enabled:true,
                  style: {
                      color: '#BDBDBD',
                      font: '11px Trebuchet MS, Verdana, sans-serif'
                  }
              },
              title: {
                  style: {
                      color: '#BDBDBD',
                      font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
                  },
                  text: 'Days',
                  margin: 0
              }
              },
    series: [
      {name: 'Required Packets', type:'spline', data: data.requiredPacket},
      {name: 'Recieved Packets', type:'spline', data: data.dataPacketCount},
      {name: 'Outage Duration minutes', type:'spline', data: data.duration},
    ],
    };
    return (
      <div style={{ background: 'white' }}>
        <div style={{ padding: '10px' }}>
          <RzCustomDate
            onSelect={date => this.props.handleDateChange(date)}
            active={'monthly'}
            auto="start"
          />
        </div>
        <GraphTitle title="ESTIMATED ENERGY UNIT LOSES (MVAH)" />
        <ReactHighcharts config={config} />
      </div>
    );
  }
}

RzLineGraph0.propTypes = {};

export default RzLineGraph0;
