/**
*
* RzTable
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Field, reduxForm } from 'redux-form/immutable';
// import RzDateRangeFilter from 'components/RzDateRangeFilter';
import RzMoreFilter from 'components/RzMoreFilter';
import RzSearchBar from 'components/RzSearchBar';
import FeederHeaderForm from 'containers/FeederHeaderForm';
import RzAutoComplete from 'components/RzAutoComplete';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import ReactPaginate from 'react-paginate';
import {
  blue300,
  indigo900,
  orange200,
  deepOrange300,
  pink400,
  grey600,
} from 'material-ui/styles/colors';
const styles = {
  buttons: {
    marginRight: '5px',
  },
};
const Wrapper = styled.div`
#react-paginate ul {
    display: inline-block;
    padding-left: 15px;
    padding-right: 15px;
}

#react-paginate li {
    display: inline-block;
}

#react-paginate .break a {
    cursor: default;
}
  padding: 0 30px;

  h1 {
    color: #323844;
    font-size: 24px;
    font-weight: 400;
    min-width: 150px;
  }

  h3 {
    color: #323844;
  }
  .pageNators{
    cursor: pointer;
    transform: perspective(1px) translateZ(0);
    transition-duration: 0.3s;
    transition-property: box-shadow, transform;
    box-shadow: 0 0 1px transparent;

    &:hover {
      border-radius: 10px;
      box-shadow: 0 4px 10px 1px rgba(0, 0, 0, .3);
      transform: scale(1.01);
    }
  }
`;

const Table = styled.div`
overflow-x:scroll;
  table {
    width: 100%;
    border-collapse: separate;
    border-spacing: 0 15px;
    color: #808080;
    font-size: 12px;
    text-align: left;
  }

  thead {
    margin-bottom: 20px;
    background: #2C4F6F;

    th {
      font-size: 13px;
      text-align: center!important;
      height: 66px;
      padding: 10px;
      text-align: left;
      color: #ffffff;
      font-weight: 600;
      border: 0px solid transparent;

      &:first-child {
        border-left-style: solid;
        border-top-left-radius: 8px;
        border-bottom-left-radius: 8px;
      }
      &:last-child {
        border-right-style: solid;
        border-bottom-right-radius: 8px;
        border-top-right-radius: 8px;
      }
    }
  }

  tbody {
    tr {
      transform: perspective(1px) translateZ(0);
      transition-duration: 0.3s;
      transition-property: box-shadow, transform;
      box-shadow: 0 0 1px transparent;

      &:hover {
        cursor: pointer;
        border-radius: 10px;
        box-shadow: 0 4px 10px 1px rgba(0, 0, 0, .3);
        transform: scale(1.01);
      }
    }
  }

  td {
    font-size:12px;
    text-align: center;
    position: relative;
    padding: 10px;
    border: 0px solid transparent;
    background: #fff;

    &:first-child {
      border-left-style: solid;
      border-top-left-radius: 8px;
      border-bottom-left-radius: 8px;
    }
    &:last-child {
      border-right-style: solid;
      border-bottom-right-radius: 8px;
      border-top-right-radius: 8px;
    }
  }

`;

const RzTableFilter = styled.div`
  display: flex;
  flex-flow: row nowrap;
`;

const RzTableFilterGroup = styled.div`
  display: flex;
  flex: ${(props) => props.right ? 4 : 6};
  justify-content: ${(props) => props.right ? 'flex-end' : 'flex-start'};
  border-bottom: 1px solid #B3B3B3;
`;

const RzTableFilterItem = styled.div`
  display: flex;
  color:#CF3446;
  h5{
    margin-top:20px;
    font-weight: 600;
  }
  flex: ${(props) => props.full ? '1' : 'none'};
  border-right: 1px solid #323844;

  &:last-child {
    border-right: 1px solid rgba(0, 0 , 0, 0);
  }
  h4{
    margin: 14px;
    margin-bottom:5px !important;
  }
  select {
    outline: none;
  }
`;

const RzTableTextFilter = styled.input`
  width: 240px;
  margin: 0.67em 0;
  padding-left: 12px;
  padding-right: 12px;
  border: 1px solid #323844;
  border-radius: 30px;
  font-size: 16px;
  outline: none;
`;

const RzTablePaginator = styled.div`
  margin-bottom:20px;
  margin-top: 30px;
  color: #808080;
  max-width:800px;
  overflow-x:scroll;
  white-space:nowrap;
`;

class RzTable extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  // paginator = (totalCount) => {
  //   const {
  //     xyz,
  //     handleClick,
  //   } = this.props;
  //   const buttons = [];
  //   const count = Math.floor(totalCount / 10) + 1;
  //   for (let i = 0; i < count; i += 1) {
  //     buttons.push(<Avatar
  //       color={deepOrange300}
  //       backgroundColor={grey600}
  //       style={styles.buttons}
  //       size={30}
  //       key={i}
  //       onClick={() => handleClick(i)}
  //       className="pageNators"
  //     >
  //       {i + 1}
  //     </Avatar>);
  //   }
  //   return buttons
  // }
  handlePageClick = (data) => {
    if (data) {
      const {
        handleClick,
      } = this.props;
      handleClick(data.selected)
    }
  };
  generateSearchResults(searchresults) {
    return searchresults.map((item) => (
      {
        text: item.case_id,
        value: (
          <MenuItem style={{ borderBottom: '1px solid #f0f0f0', padding: '5px' }}>
            <div style={{ lineHeight: '20px' }}> <span style={{ fontSize: '14px', fontWeight: 500 }}> {item.case_code}</span></div>
            <div style={{ lineHeight: '20px', color: '#808285' }}> <span style={{ fontWeight: 500 }} > Company Name </span> <span style={{ fontSize: '14px' }}> {item.company_name} </span></div>
            <div style={{ lineHeight: '20px', color: '#808285' }}> <span style={{ fontWeight: 500 }} > Vehicle Reg </span> <span style={{ fontSize: '14px' }}> {item.vehicle_registration_number} </span> </div>
            <Divider />
          </MenuItem>
        ),
      }
    ));
  }
  handleSelectSearch(value) {
    if (value.text === '') {
      return
    }
    if (typeof value === 'object') {
      this.props.handleSearchSelect(value.text)
    } else {
      console.log('Select a field')
    }
    // this.props.handleSearchSelect(value.text)
    // this.setState({ fieldText: value.text });
  }
  defaultFilter = (row) => {
    const {
      handleClick,
      caseLogs = false,
    } = this.props;
    // if (caseLogs) {
    //   if (row.status === 'case_completed') {
    //     return true;
    //   }
    //   return false;
    // }
    return true;
  };

  render() {
    const {
      data,
      columns,
      title,
      renderCell,
      filter,
      loaded=false,
      caseLogs = false,
      searchResults,
      handleSearchSelect,
      handleSearchText,
      handleSelect,
      currentPage,
      totalCount,
      showSub=false,
    } = this.props;

    const defaultSorter = (a, b) => a.sort - b.sort;

    return (
      <Wrapper>
        <RzTableFilter>
          <RzTableFilterGroup>
            <RzTableFilterItem>
              <h5>{title}</h5>
            </RzTableFilterItem>
            {
              filter && filter.status ? null
               : null
            }
            {
              filter && filter.assignee ? null
               : null
            }
          </RzTableFilterGroup>

          <RzTableFilterGroup right>
            {
              filter && filter.search && caseLogs ? (
                <AutoComplete
                  name="company_id"
                  fullWidth
                  menuStyle={{ maxHeight: '400px', overflowY: 'auto' }}
                  filter={AutoComplete.noFilter}
                  hintText="Search Cases"
                  dataSource={this.generateSearchResults(searchResults)}
                  onNewRequest={this.handleSelectSearch.bind(this)}
                  onUpdateInput={(value) => handleSearchText(value)}
                />
              ) : null
            }
            {/*
              filter && filter.dateRange ? (
                <RzTableFilterItem>
                  <RzDateRangeFilter />
                </RzTableFilterItem>
              ) : null
            }
            {
              filter && filter.more ? (
                <RzTableFilterItem>
                  <RzMoreFilter />
                </RzTableFilterItem>
              ) : null
            */}
          </RzTableFilterGroup>
        </RzTableFilter>
        <FeederHeaderForm
          showSub={showSub}
          handleSubmit={this.props.handleSubmit}
        />
        {loaded ? (<Table>
          <table>
            <thead>
              <tr>
                {
                  columns.sort(defaultSorter).map((column) => (
                    <th
                      key={column.sort}
                      colSpan={column.size > 1 ? column.size : null}
                    >
                      {column.name}
                    </th>
                  ))
                }
              </tr>
            </thead>
            <tbody>
              {
                data.filter(this.defaultFilter).map((row) => (
                  <tr
                    key={row.sort}
                    onClick={() => handleSelect(row)}
                  >
                    {
                      columns.sort(defaultSorter).map((column) => (
                        <td
                          key={column.sort}
                          colSpan={column.size > 1 ? column.size : null}
                        >
                          {renderCell(column.type, row)}
                        </td>
                      ))
                    }
                  </tr>
                ))
              }
            </tbody>
          </table>
        </Table>): null}

        {loaded?(<RzTablePaginator>
          <ReactPaginate
            previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={<a href="">...</a>}
            breakClassName={'break-me'}
            pageCount={Math.ceil(totalCount / 10)}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            initialPage={currentPage}
            containerClassName={'pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'active'}
          />
          {/* <div> &lt;&nbsp;1&nbsp;-&nbsp;10&nbsp;of&nbsp;20&nbsp;&gt; </div> */}
        </RzTablePaginator>):(null)}
      </Wrapper>
    );
  }
}

RzTable.propTypes = {
  title: PropTypes.string.isRequired,
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  renderCell: PropTypes.func.isRequired,
  filter: PropTypes.object,
};

export default RzTable;
