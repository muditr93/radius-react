/**
 *
 * HeaderButton0
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
import styled from 'styled-components';



/* eslint-disable react/prefer-stateless-function */
class HeaderButton0 extends React.PureComponent {
  render(props) {
    const {
      active=false,
      down,
      id,
      name,
      up
    } = this.props


    const HeaderTile = styled.div`
      padding: 20px 15px;
      cursor: pointer;
      position: relative;
      &:active{
        transform: translateY(2px);
      }
      img{
        height: 25px;
      }
      .MainText{
        font-size: 13px;
        font-weight: 700;
      }
      color: ${() => active ? "white": "#2C4F6F"};
      background: ${() => active ? "#2C4F6F" : "white"};
      display: flex;
      &:hover{
        color: white;
        background: #2C4F6F;
        box-shadow: 4px 7px 4px 0px rgba(186,179,186,1)
      }
      .activeBox{

      }
      .leftBlock{
        flex: 1;
        text-align: center;
        position: relative;
        .leftTextBlock{
          border-right: 1px solid #c0c0c0;
        }
        .subtext{
          font-size: 11px;
        }
        .mainText{

        }
      }
      .rightBlock{
        .leftTextBlock{
          display: flex;
          .right-left-Block{
            flex: 4;
          }
          .right-right-Block{
            flex: 6;
          }
        }
        flex: 1;
        text-align: center;
        .subtext{
          font-size: 13px;
          font-weight: 700;
        }
        .mainText{
          font-size: 11px;
        }
      }
    `;
    return (
      <HeaderTile>
        <div className="leftBlock">
          <div className="leftTextBlock">
            <div className="MainText">
              {name}
            </div>
            <div className="subtext">
              {down} Down
            </div>
          </div>
        </div>
        <div className="rightBlock">
          <div className="leftTextBlock">
            <div className="right-left-Block">
              <img src="https://image.ibb.co/cR1Ksq/Bar-graph.png" alt="Bar-graph" border="0" />
            </div>
            <div className="right-right-Block">
              <div className="mainText">
                Total Up
              </div>
              <div className="subtext">
                {up}
              </div>
            </div>
          </div>
        </div>
      </HeaderTile>
    );
  }
}

HeaderButton0.propTypes = {};

export default HeaderButton0;
