/**
 *
 * RzLineGraph1
 *
 */

import React from 'react';
import GraphTitle from 'components/GraphTitle';
import ReactHighcharts from 'react-highcharts';
import DateRangeGraph from 'components/DateRangeGraph';
import RzCustomDate from 'components/RzCustomDate';
import dayjs from 'dayjs'
class RzLineGraph1 extends React.Component {
  dateSubmit = (date) => {
    console.log('startDate', date);
    // this.setState({ startDate: date })
  };
  getDate = (data) => {
    let utcSeconds = data;
    let today = new Date(0);
    today.setUTCSeconds(utcSeconds);
    return dayjs(today).format('DD-MM-18')
    // debugger;
    // let dd = today.getDate();
    // let mm = today.getMonth() + 1; //January is 0!
    // let yyyy = today.getFullYear();
    // if (dd < 10) {
    //   dd = '0' + dd;
    // }
    // if (mm < 10) {
    //   mm = '0' + mm;
    // }
    // today = dd + '/' + mm + '/'  + yyyy;
    // return today;
  }

  render() {
    const {data, logError} = this.props;
    const config = {
      title: {
        text: false,
      },
      colors: ['#3A4D6C', '#B4384D', '#00A99D', '#8CC63F'],
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
        },
      },
      yAxis:[{
            min:0,
            labels: {
                format: '{value}Amp',
                style: {
                    color: '#BDBDBD'
                }
            },
            title: {
                text: '',
                style: {
                    color:'#BDBDBD'
                }
            }
        }, {
            min:0,
            opposite: true,
            labels: {
                format: '{value}kVA',
                style: {
                    color:'#BDBDBD'
                }
            },
            title: {
                text: '',
                style: {
                    color: '#BDBDBD'
                }
            }
        }],
      xAxis: {
              categories: data.time.map(item => this.getDate(item)),
              labels: {
                  enabled:true,
                  style: {
                      color: '#BDBDBD',
                      font: '11px Trebuchet MS, Verdana, sans-serif'
                  }
              },
              title: {
                  style: {
                      color: '#BDBDBD',
                      font: 'bold 12px Trebuchet MS, Verdana, sans-serif'
                  },
                  text: 'Time',
                  margin: 0
              }
              },
    series: [ {
                name: 'R Phase Current (Amp)',
                type:'spline',
                data: data.rvoltage,
                },
                {
                    name: 'Y Phase Current (Amp)',
                    type:'spline',
                    data: data.yvoltage,
                },
                {
                    name: 'B Phase Current (Amp)',
                    type:'spline',
                    data: data.bvoltage,
                },{
                    name: 'Load',
                    type:'spline',
                    yAxis: 1,
                    data: data.load,
                }],
    };
    return (
      <div style={{ background: 'white' }}>
        <div style={{ padding: '10px' }}>
          {
            !logError ? (
              <div>
                <RzCustomDate
                  onSelect={date => this.props.handleDateChange(date)}
                  active={'daily'}
                  auto="start"
                />
                <ReactHighcharts config={config} />
              </div>
            ): (<div style={{textAlign: 'center'}}>{logError}</div>)
          }
        </div>
      </div>
    );
  }
}

RzLineGraph1.propTypes = {};

export default RzLineGraph1;
