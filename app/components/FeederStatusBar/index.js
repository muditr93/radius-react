/**
 *
 * FeederStatusBar
 *
 */

import React from 'react';
import GraphTitle from 'components/GraphTitle';
import ReactHighcharts from 'react-highcharts';
let chartReflow = undefined;

/* eslint-disable react/prefer-stateless-function */
class FeederStatusBar extends React.Component {
  componentDidUpdate() {

  }

  componentDidMount() {
    const chart = this.refs.chart ? this.refs.chart.getChart() : {}
    chartReflow = chartReflow || chart.reflow
    chart.reflow = () => {}
    setTimeout(() => (chart.reflow = chartReflow))
  }

  render() {
    const {
      categories, notConnected, down, up, nonParticipating, data
    } = this.props;

    const config = {
      chart: {
        type: 'column',
      },
      animation: {
            duration: 8000
        },
      showLegend: 'true',
      colors:['#3A4D6C','#B4384D','#00A99D','#8CC63F'],
      title: {
        text: false,
      },
      xAxis: {
        categories: categories && categories.length > 0 ? categories : [],
        crosshair: true,
      },
      yAxis: {
        min: 0,
        gridLineWidth: 0,
        minorGridLineWidth: 0,
        title: {
          text: 'No. of feeders (Percentage)',
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
          + '<td style="padding:0"><b>{point.y} {point.percentage}%</b></td></tr>',
        footerFormat: '</table>',
        shared: false,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: 'Not Connected',
          data: notConnected && notConnected.length > 0 ? notConnected : [],
        },
        {
          name: 'Down',
          data: down && down.length > 0 ? down : [],
        },
        {
          name: 'Not Participating',
          data: nonParticipating && nonParticipating.length > 0 ? nonParticipating : [],
        },
        {
          name: 'Up',
          data: up && up.length > 0 ? up : [],
        },
      ],
    };

    return (
      <div style={{background: 'white'}}>
        <GraphTitle title={this.props.title} />
        {up ? <ReactHighcharts config={config} ref="chart" /> : null}

      </div>
    );
  }
}

FeederStatusBar.propTypes = {};

export default FeederStatusBar;
