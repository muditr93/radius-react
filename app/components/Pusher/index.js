/**
*
* Pusher
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Paper from 'material-ui/Paper';

const style = {
  minHeight: '400px',
  margin: '40px 0',
  padding: '30px',
};

const Wrapper = styled.div`
  flex: 1;
  margin: 0 300px 20px 0;
  height: 97%;

  .subWrapper{
    height:96%;
  }
`;
function Pusher({
  needPaper = true,
  children,
}) {
  return (
    <Wrapper>
      {
        needPaper ? (
          <Paper style={style} zDepth={2}>
            {children}
          </Paper>
        ) : (
          <div className="subWrapper">
            {children}
          </div>
        )
      }
    </Wrapper>
  );
}

Pusher.propTypes = {
  needPaper: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default Pusher;
