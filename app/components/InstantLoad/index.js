/**
 *
 * InstantLoad
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import GraphTitle from 'components/GraphTitle';

import ReactHighcharts from 'react-highcharts';

/* eslint-disable react/prefer-stateless-function */
class InstantLoad extends React.Component {
  render() {
    const { categories, mw, mva } = this.props;
    const config = {
      chart: {
        type: 'column',
      },
      colors:['#00A99D','#8CC63F'],
      title: {
        text: '',
      },
      yAxis: {
        min: 0,
        gridLineWidth: 0,
        minorGridLineWidth: 0,
      },
      xAxis: {
        categories:
          categories && categories.length > 0
            ? categories
            : ['EODB', 'IPDS', '16 Nigam Nagar', 'REC'],
        crosshair: true,
      },
      // yAxis: {
      //   min: 0,
      //   title: {
      //     text: '',
      //   },
      // },
      // tooltip: {
      //   headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      //   pointFormat:
      //     '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
      //     + '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
      //   footerFormat: '</table>',
      //   shared: true,
      //   useHTML: true,
      // },
      // plotOptions: {
      //   column: {
      //     pointPadding: 0.2,
      //     borderWidth: 0,
      //   },
      // },
      credits: {
        enabled: false,
      },
      series: [
        {
          name: 'MVA',
          data: mva && mva.length > 0 ? mva : [5, 2, 16, 19],
        },
        {
          name: 'MW',
          data: mw && mw.length > 0 ? mw : [20, 10, 40, 50],
        },
      ],
    };
    return (
      <div style={{background: 'white'}}>
        <GraphTitle title="Instant Load"/>
        <ReactHighcharts config={config} />
      </div>
    );
  }
}

InstantLoad.propTypes = {};

export default InstantLoad;
