/**
 *
 * RzCustomColumn
 *
 */

import React from 'react';
import styled from 'styled-components';
import GraphTitle from 'components/GraphTitle';

const HeaderTile = styled.div`
  padding: 10px 0 0 10px;
  font-size: 12px;
  border-bottom: 1px solid lightgray;
  color: #cf3446;
  text-transform: uppercase;
`;

const Wrapper = styled.div`
  background: white;
  height: 100%;
  .grid {
    width: 100%;
    padding: 15px 2px 5px 2px;
    height: inherit;
    display: flex;
    flex-direction: column;
    .a {
      flex: 1;
      .aa {
        flex: 1;
      }
      .ab {
        font-size: 13px;
        color: #544d4dd6;
        display: block;
        padding: 10px;
        border: 0px solid transparent;
        text-align: left;
        div {
          border-left: 3px solid Green;
          margin-top: 2px;
          padding: 2px;
          border-radius: 5px;
          border: 0px solid transparent;
          background: #56d25a5e;
          line-height: 20px;
        }
      }
    }
    .b {
      flex: 1;
      .ba {
      }
      .bb {
        display:block;
        font-size: 13px;
        color: #544d4dd6;
        text-align: left;
        div {
          border-left: 3px solid red;
          margin-top: 2px;
          padding: 2px;
          border-radius: 5px;
          background: #ca55555e;
          line-height: 20px;
        }
      }
    }
  }
`;

class RzCustomColumn extends React.Component {
  render() {
    const {
      best,
      worst,
      bWCountUpdate,
      bWCount,
      selectType=false,
      performanceType = false,
    } = this.props;

    return (
      <Wrapper>
        <GraphTitle
          title="Performance"
          bWCountUpdate={bWCountUpdate}
          bWCount={bWCount}
          selectType={selectType}
          performanceType={performanceType}
        />
        <div className="grid">
          <div className="a">
            <div className="aa">Best</div>
            <div className="ab">{best && best.length > 0 && best.map(x => {
                return (
                  <div>
                    {x.discomName} - {x.substationName} - {' '}
                    <span style={{fontWeight: 800}}>
                      (
                      {performanceType == "interruption" ? x.outageCount:''}
                      {performanceType == "average_duration" ? x.averageOutageDuration:''}
                      {performanceType == "duration" ? x.total_Outage_Duration:''})
                    </span>
                  </div>);
              })}</div>
          </div>
          <div className="b">
            <div className="ba">Worst</div>
            <div className="bb">{worst && worst.length > 0 && worst.map(x => <div>
              {x.discomName} - {x.substationName} - {' '}
              <span style={{fontWeight: 800}}>
                (
                  {performanceType == "interruption" ? x.outageCount:''}
                  {performanceType == "average_duration" ? x.averageOutageDuration:''}
                  {performanceType == "duration" ? x.total_Outage_Duration:''}
                )
              </span>
              </div>)}</div>
          </div>
        </div>
      </Wrapper>
    );
  }
}

RzCustomColumn.propTypes = {};

export default RzCustomColumn;
